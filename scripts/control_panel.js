// JavaScript Document

var globalVar = "";
var globalSemester = "";

function SelectSemester(title, id) {
	
	var login = '<TABLE align="center" border="0" width="130">';
		login += '<TR>';
	login += '<TD height="26" id="fnt_8_typ_1" align="center" style="font-size:14pt"><strong>'+title+'</strong></TD>';
	login += '</TR>';
	login += '<TR>';
	login += '<TD height="15" id="fnt_8_typ_1" align="center"><strong>Select Semester:</strong></TD>';
	login += '</TR>';
	login += '<TR>';
	login += '<TD align="center"><select id="fnt_8_typ_1" class="semester" style="background-color:#DEFEB4; border:1px solid #7AD202; width:170pt;padding:3pt"><option value=""></option><option value="first_registration">1st Semester</option><option value="second_registration">2nd Semester</option></select></TD>';
	login += '</TR>';
	login += '<TR>';
	login += '<TD align="center"><input type="button" onclick="SetGlobalSemester(\''+id+'\')" id="fnt_8_typ_1" style="padding:3pt; border:1px solid black;" value=" Continue " /></TD>';
	login += '</TR>';
	login += '</TABLE><br>';
	
	DisposeDialog();
	COEDialog(login,310);
}


function SetGlobalSemester(id) {
	
	var semester = $(".semester").val();
	globalSemester = semester;
	
	if(semester != "") {
		
		if(id == "EXT_REG_DATA" || id == "PROCESS_SUMMARY") {
			HandleDatabaseOperation("","","","",id,semester);
		}
	}
	else {
		alert("Select Semester (1st or 2nd)");
	}
}


function ExtractRegistration(title) {
	SelectSemester(title,"EXT_REG_DATA");
}

function ProcessSummary(title) {
	SelectSemester(title,"PROCESS_SUMMARY");
}



function SemesterResultProcessor(processType, dbName, semester) {
	HandleDatabaseOperation("","","","",processType,semester);
}




function ConfigureDialogBox(processType, dbName, semester) {

	var html = "";
	if(processType != "" && dbName != "") {
		
		if(globalVar == "") {
			html = $("#DialogBox").html();
			globalVar = html;
			$("#DialogBox").html("");
		}
		else {
			html = globalVar;
		}
		html += '<input type="hidden" id="hiddenParam" value="'+dbName+'" /><input type="hidden" id="_semester" value="'+semester+'" /><input type="hidden" id="_opCode" value="'+processType+'" />';
		
		DisposeDialog();
		COEDialog(html,420);
	}
	else {
		$("#errorMsg").html("<span style='color:red'>Select Operation to perform on the Database</span>");
	}
}

function DialogResponse() {
	
	var Course = $("#_Course").val();
	var Programme = $("#_Programme").val();
	var Mode = $("#_Mode").val();
	var CurrentClass = $("#_CurrentClass").val();
	var semester = $("#_semester").val();
	var opCode = $("#_opCode").val();
	var hiddenParam = $("#hiddenParam").val();
	var _error = "";

	if(Course == "")
	_error += "Course, ";
	
	if(Programme == "")
	_error += "Programme, ";
	
	if(Mode == "")
	_error += "Mode, ";
	
	if(CurrentClass == "")
	_error += "Level, ";

	if(_error == "") {
	
		if(confirm("Are you sure you want to proceed?")) {
			HandleDatabaseOperation(Course,Programme,Mode,CurrentClass,opCode,semester);
		}
		
	}
	else {
		alert("The following field(s) are missing:\n" + _error + "and be sure you input valid entries.",400);	
		return;
	}
}


function HandleDatabaseOperation(Course,Programme,Mode,CurrentClass,opCode,semester) {
	DisposeDialog();
	COEDialog("<center><br>Processing your request... Please wait.<p><img src='../images/loading.gif'></center>",400);
$.post("../util/controlAPI.php",{ opCode: opCode, Course: Course, Programme: Programme, Mode: Mode, CurrentClass: CurrentClass, semester: semester }, function(response) {
	DisposeDialog();
	COEDialog("<center><br>"+response+"</center>",300);
});
	
}

