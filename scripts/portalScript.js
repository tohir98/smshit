// JavaScript Document


function COEDialog(arg, width) {
$(function() {
	$("#dialog-modal").html(arg);
	$("#dialog").dialog("destroy");
	$("#dialog-modal").dialog({
		modal: true,
		width: width
	});
});
}

function DisposeDialog() {
$("#dialog-modal").dialog("destroy");
}

function isChecked() {
	if(document.forms.Reg.agreeBtn.checked == true)  {
		document.forms.Reg.verifier.disabled = false;
	}
	else {
		document.forms.Reg.verifier.disabled = true;
	}
}

function RestoreText() {
	$("#errorMsg").css("color","#333").html("The Registration Number and Bank Pin Code are on your Bank Orchids eTransaction Printout.");
}


function RefreshLGA() {
	
	$(".LGA").css("background-color", "pink");
	var state = $(".State").val();
	$.post("./util/api.php",{ opCode: "LOCAL_GOVT", state:state }, function(response) {
		if(response != "")
			$("#lgaLoader").html(response);
		$(".LGA").css("background-color", "white");
	});
}

function InputLoginForm(title, OpType) {
	
var login = '<DIV style="padding-left:10pt; padding-top:4pt"><TABLE border="0" width="310" align="center">';
login += '<TR>';
login += '<TD height="30" class="FontB" align="center" colspan="2"><strong>'+title+'</strong></TD>';
login += '</TR>';
login += '<TR>';
login += '<TD height="20" width="100" class="FontB">Exam No:</TD>';
login += '<TD><input type="text" id="RegNo" class="InputBoxB" onclick="RestoreText()" style="width:130pt; background-color:#eeeeee" /></TD>';
login += '</TR>';
login += '<TR>';
login += '<TD class="FontB">Bank Pin Code:</TD>';
login += '<TD><input type="text" id="PinCode" class="InputBoxB" onclick="RestoreText()" style="width:130pt; background-color:#eeeeee" /></TD>';
login += '</TR>';
login += '<TR>';
login += '<TD colspan="2" align="center"><input type="button" class="Button" style="padding:3pt; width:70pt; background-color:gray"  onclick="VerifyLoginForm()" value=" Login " /></TD>';
login += '</TR>';
login += '<TR>';
login += '<TD colspan="2" height="14"><input type="hidden" id="OpType" value="'+OpType+'"></TD>';
login += '</TR>';
login += '<TR>';
login += '<TD class="FontB" align="center" colspan="2"><div id="errorMsg">The Examination Number and Bank Pin Code are on your Bank Orchids eTransaction Printout.</div></TD>';
login += '</TR>';
login += '</TABLE><BR></DIV>';

DisposeDialog();
COEDialog(login, 380);

}


function StaffDialog() {
	
var login = '<DIV style="padding-left:10pt; padding-top:4pt"><TABLE border="0" width="310" align="center">';
login += '<TR>';
login += '<TD height="30" class="FontB" align="center" colspan="2"><strong>STAFF MONTHLY PAYSLIP</strong></TD>';
login += '</TR>';
login += '<TR>';
login += '<TD height="20" width="100" class="FontB">Employee ID:</TD>';
login += '<TD><input type="text" id="EmpID" class="InputBoxB" style="width:130pt; background-color:#eeeeee" /></TD>';
login += '</TR>';
login += '<TR>';
login += '<TD class="FontB">Phone No:</TD>';
login += '<TD><input type="text" id="PhoneNo" class="InputBoxB" style="width:130pt; background-color:#eeeeee" /></TD>';
login += '</TR>';
login += '<TR>';
login += '<TD colspan="2" align="center"><input type="button" class="Button" style="padding:3pt; width:70pt; background-color:gray" onclick="VerifyStaffInfo()" value=" Continue " /></TD>';
login += '</TR>';
login += '<TR>';
login += '<TD class="FontB" align="center" colspan="2"><div id="errorMsg"></div></TD>';
login += '</TR>';
login += '</TABLE><BR></DIV>';

DisposeDialog();
COEDialog(login, 380);

}


function VerifyStaffInfo() {
	
	var EmpID = $("#EmpID").val();
	var PhoneNo = $("#PhoneNo").val();

	if(EmpID != "" && PhoneNo != "") {
		
		$("#errorMsg").html("<blink><span style='color:green'><b>Please Wait...</b></span></blink>");
		$.post("./util/api.php",{ opCode: "STAFF_PAYMENT", EmpID: EmpID, PhoneNo: PhoneNo }, function(response) {

			if(response.indexOf("VALID") != -1) {
				$("#errorMsg").html("<blink><span style='color:green'><b>Generating Slip...</b></span></blink>");
				window.location = "PaymentSlip.php";
			}
			else if(response.indexOf("ERROR") != -1) {
				$("#errorMsg").html("<blink><span style='color:red'><b>Invalid Employee Information</b></span></blink>");
			}
		});
	}
	else {
		$("#errorMsg").html("<blink><span style='color:red'><b>Incomplete login details</b></span></blink>");
	}
}

function VerifyLoginForm() {
	
	var RegNo = $("#RegNo").val();
	var PinCode = $("#PinCode").val();
	var OpType = $("#OpType").val();
	var errorMessage = "";
	var validMessage = "";
	var pageUrl = "";

	if(RegNo != "" && PinCode != "") {
		$("#errorMsg").html("<blink><span style='color:green'><b>Please Wait...</b></span></blink>");
		$.post("./util/api.php",{ opCode: OpType, RegNo: RegNo, PinCode: PinCode }, function(response) {
		
			//RESULT CHECKING
			if(OpType == "RESULT") {
				errorMessage = "Cannot Locate Qualifying Result.";
				validMessage = "Loading Qualifying Result...";
				pageUrl = "ResultCheckSlip.php";
			}

			if(response.indexOf("VALID") != -1) {
				$("#errorMsg").html("<blink><span style='color:green'><b>"+validMessage+"</b></span></blink>");
				window.location = pageUrl;
			}
			else if(response.indexOf("ERROR") != -1) {
				$("#errorMsg").html("<blink><span style='color:red'><b>"+errorMessage+"</b></span></blink>");
			}
			
		});
	}
	else {
		$("#errorMsg").html("<blink><span style='color:red'><b>Incomplete login details</b></span></blink>");
	}
}


function ShowLoginDialog(title, opcode, caption)  {
	
var login = '<DIV style="padding-left:10pt; padding-top:4pt"><TABLE align="center">';
login += '<TR>';
login += '<TD width="216" height="20" class="FontB"><strong>'+ title +':</strong></TD>';
login += '</TR>';
login += '<TR>';
login += '<TD height="20" class="FontB">' + caption + ':</TD>';
login += '</TR>';
login += '<TR>';
login += '<TD><input type="text" id="LoginID" class="InputBoxB" style="width:130pt; background-color:#eeeeee" /></TD>';
login += '</TR>';
login += '<TR>';
login += '<TD class="FontB">Password:</TD>';
login += '</TR>';
login += '<TR>';
login += '<TD><input type="password" id="Password" class="InputBoxB" style="width:130pt; background-color:#eeeeee" /></TD>';
login += '</TR>';
login += '<TR>';
login += '<TD><input type="button" class="Button" onclick="VerifyLogin(\''+ opcode +'\')" value="  Login  " /></TD>';
login += '</TR>';
login += '<TR>';
login += '<TD height="14"<input type="hidden" name="opCode" value="'+ opcode +'"</TD>';
login += '</TR>';
login += '<TR>';
login += '<TD class="FontB" align="center"><div style="font-size:8pt; padding-right:10pt; color:#333333" id="errorMsg"> </div></TD>';
login += '</TR>';
login += '</TABLE><BR></DIV>';

DisposeDialog();
COEDialog(login, 235);

}

function VerifyLogin(LoginType) {
	var LoginID = $("#LoginID").val();
	var Password = $("#Password").val();
	
	if(LoginID != "" && Password != "") {	
		$("#errorMsg").html("<blink><span style='color:green'><b>Verifying...</b></span></blink>");
		$.post("./util/api.php",{ opCode: "VERIFY", LoginType: LoginType, LoginID: LoginID, Password: Password }, function(response) {
			if(response.indexOf("VALID") != -1) {
				$("#errorMsg").html("<blink><span style='color:green'><b>Welcome, Redirecting...</b></span></blink>");
				
				if(LoginType == "STAFF_LOGIN") 
					window.location = "Lecturer.php?auth=1";
				else if(LoginType == "STUD_LOGIN") 
					window.location = "StudentPortal.php?auth=1";
			}
			else if(response.indexOf("ERROR") != -1) {
				$("#errorMsg").html("<blink><span style='color:green'><b>Invalid Login.</b></span></blink>");
			}
		});
	}
	else {
		$("#errorMsg").html("<blink><span style='color:red'><b>Incomplete login details</b></span></blink>");
	}
}


function AdmissionList() {

	var login = '<TABLE align="center" border="0" width="230">';
	login += '<TR>';
	login += '<TD id="fnt_8_typ_1" align="center" style="padding:2pt; font-size:14pt"><strong>Admission List</strong></TD>';
	login += '</TR>';
	login += '<TR>';
	login += '<TD height="15" id="fnt_8_typ_1">Select Department:</TD>';
	login += '</TR>';
	login += '<TR>';
	login += '<TD><div class="dept_loader"><select id="fnt_8_typ_1" class="dept" style="background-color:#DEFEB4; border:1px solid #7AD202; width:170pt;padding:3pt"><option value=""></option></select></div></TD>';
	login += '</TR>';
	login += '<TR>';
	login += '<TD align="center" style="padding-top:3pt"><input type="button" onclick="ViewList()" id="fnt_8_typ_1" style="padding:3pt; border:1px solid black;" value=" View Admission List " /></TD>';
	login += '</TR>';
	login += '</TABLE><br><br>'
	
	DisposeDialog();
	COEDialog(login, 300);	
	//Load Dept
	LoadDeptList();	
}

function LoadDeptList() {
	$.post("./util/api.php",{ opCode: "DEPT"}, function(response) {
			$(".dept_loader").html(response);
	});
}

function ViewList() {
	
	var dept = $(".dept").val();
	if(dept != "") {
		
		DisposeDialog();
		COEDialog("<div class='FontA' id='listviewer' style='overflow:auto;text-align:center; color:black;height:350pt;'><br><br><br><br><br><br><br><b>Loading Admission List...</b></div>", 600);	
	$.post("./util/api.php",{ opCode: "LOAD_ADM_LIST", dept:dept }, function(response) {
			$("#listviewer").html(response);
	});
	
	}
	else {
		alert("Select Department.");	
	}
}


function PrintLetter(examnumber) {
	window.open("AdmissionLetters.php?ex="+examnumber);
}

function ReprintCourseRegSlip(type) {

	var login = '<TABLE align="center" border="0" width="230">';
	login += '<TR>';
	login += '<TD id="fnt_8_typ_1" align="center" style="padding:2pt; font-size:14pt"><strong>Reprint Slip</strong></TD>';
	login += '</TR>';
	login += '<TR>';
	login += '<TD height="15" id="fnt_8_typ_1">Select Semester:</TD>';
	login += '</TR>';
	login += '<TR>';
	login += '<TD><select id="fnt_8_typ_1" class="semester" style="background-color:#DEFEB4; border:1px solid #7AD202; width:170pt;padding:3pt"><option value=""></option><option value="first">1st Semester</option><option value="second">2nd Semester</option></select></TD>';
	login += '</TR>';
	login += '<TR>';
	login += '<TD align="center" style="padding-top:3pt"><input type="button" onclick="PopUpCourseSlip(\''+type+'\')" id="fnt_8_typ_1" style="padding:3pt; border:1px solid black;" value=" Reprint Course Registration Slip " /></TD>';
	login += '</TR>';
	login += '</TABLE><br><br>'
	
	DisposeDialog();
	COEDialog(login, 300);		
}


function SemesterResult() {

	var login = '<TABLE align="center" border="0" width="230">';
	login += '<TR>';
	login += '<TD id="fnt_8_typ_1" align="center" style="padding:2pt; font-size:14pt"><strong>Semester Result</strong></TD>';
	login += '</TR>';
	login += '<TR>';
	login += '<TD height="15" id="fnt_8_typ_1">Select Semester:</TD>';
	login += '</TR>';
	login += '<TR>';
	login += '<TD><select id="fnt_8_typ_1" class="semester" style="background-color:#DEFEB4; border:1px solid #7AD202; width:170pt;padding:3pt"><option value=""></option><option value="first">1st Semester</option><option value="second">2nd Semester</option></select></TD>';
	login += '</TR>';
	login += '<TR>';
	login += '<TD align="center" style="padding-top:3pt"><input type="button" onclick="PrintResult()" id="fnt_8_typ_1" style="padding:3pt; border:1px solid black;" value=" SHOW RESULT " /></TD>';
	login += '</TR>';
	login += '</TABLE><br><br>'
	
	DisposeDialog();
	COEDialog(login, 300);		
}

function PrintResult() {
	var semester = $(".semester").val();
	if(semester != "") {
		window.open("resultslip.php?sem="+semester);
	}
	else {
		alert("Select result's semester to preview.");
	}
}



function PopupIDCardBox() {
	
	var login = '<br><TABLE align="center" border="0" width="200">';
	login += '<TR>';
	login += '<TD height="15" align="center" id="fnt_8_typ_1"><b>ID Card Confirmation Slip</b></TD>';
	login += '</TR>';
	login += '<TR>';
	login += '<TD align="center" style="padding-top:3pt"><input type="button" onclick="window.open(\'idcardslip.php\')" id="fnt_8_typ_1" style="padding:3pt; border:1px solid black;" value=" Print ID Card Slip " /></TD>';
	login += '</TR>';
	login += '</TABLE><br>'
	
	DisposeDialog();
	COEDialog(login, 250);		
}
