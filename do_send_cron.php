<?php

require("pubs/virtualphp.php");
opendatabase();

do_send_schedule();
do_send_cron();
processMessageStatus();

function do_send_cron() {
    echo $current_date = date("Y-m-d : H:i:s");

    $new_rs = ExecuteSQLQuery("SELECT * FROM smsoutbox WHERE status = 'Pending' AND date <= '$current_date' and mid is null  limit 1000 ");

    $rs_count = mysqli_num_rows($new_rs);

    $j = 0;
    if ($rs_count > 0) {
        $t = "1";
        $u = "tundeaminu@gmail.com";
        $p = "olatunde2711";


        for ($j = 1; $j <= $rs_count; $j++) {
            $new_row = mysqli_fetch_array($new_rs);
            $smsid = $new_row['id'];
            $m = $new_row['message'];
            $numr = $new_row['pnumber'];
            $s = $new_row['sname'];

            $url = "http://83.138.190.168:8080/pls/vas2nets.inbox_pkg.schedule_sms?"
                    . "username=" . UrlEncode($u)
                    . "&password=" . UrlEncode($p)
                    . "&message=" . UrlEncode($m)
                    . "&receiver=" . UrlEncode($numr)
                    . "&sender=" . UrlEncode($s)
                    . "&message_type=" . UrlEncode($t);


            $statusCode = do_response($url);

            $split = explode(".", $statusCode);
            $messageId = trim($split[1]);

            if (ExecuteSQLQuery("UPDATE smsoutbox SET status = '$statusCode', message_id = '$messageId' WHERE id = '$smsid' ")) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
    }
}

/**
 * Update sent message status
 */
function processMessageStatus() {
    echo $current_date = date("Y-m-d : H:i:s");
    $new_rs = ExecuteSQLQuery("SELECT * FROM smsoutbox WHERE processed = '0' AND date <= '$current_date' and message_id is not null  limit 100 ");

    $rs_count = mysqli_num_rows($new_rs);

    $j = 0;
    if ($rs_count > 0) {
        for ($j = 1; $j <= $rs_count; $j++) {
            $new_row = mysqli_fetch_array($new_rs);
            $message_id = $new_row['message_id'];
            
            if (strlen($message_id) < 1) {
                continue;
            }
            $smsid = $new_row['id'];

            $responseurl = "http://83.138.190.168:8080/pls/vas2nets.inbox_pkg.delivery_status?message_id=" . UrlEncode($message_id);

            $status = json_decode(do_response($responseurl));
            $starep = $status->status;
            
            ExecuteSQLQuery("UPDATE smsoutbox SET status = '$starep', processed = 1 WHERE id = '$smsid' ");
            
        }
    }
}

function do_send_schedule() {
    $current_date = date("Y-m-d");
    $current_time = date("H:i:s");

    $new_rs = ExecuteSQLQuery("SELECT * FROM schedul WHERE status = 0 and Send_date <= '$current_date' and send_time <= '$current_time' ");

    while ($row = mysqli_fetch_row($new_rs)) {
        $mid = $row[5];
        $scheduleid = $row[0];

        $smsQuery = ExecuteSQLQuery("SELECT * FROM smsoutbox WHERE status = 'Pending' AND mid = '" . $mid . "' ");

        $t = "1";
        $u = "tundeaminu@gmail.com";
        $p = "olatunde2711";

        while ($smsResult = mysqli_fetch_array($smsQuery)) {
            $smsid = $smsResult['id'];
            $m = $smsResult['message'];
            $numr = $smsResult['pnumber'];
            $s = $smsResult['sname'];
            $url = "http://83.138.190.168:8080/pls/vas2nets.inbox_pkg.schedule_sms?"
                    . "username=" . UrlEncode($u)
                    . "&password=" . UrlEncode($p)
                    . "&message=" . UrlEncode($m)
                    . "&receiver=" . UrlEncode($numr)
                    . "&sender=" . UrlEncode($s)
                    . "&message_type=" . UrlEncode($t);


            $statusCode = do_response($url);

            $split = explode(".", $statusCode);
            $messageId = trim($split[1]);
           
            if (ExecuteSQLQuery("UPDATE smsoutbox SET status = '$statusCode', message_id = '$messageId' WHERE id = '$smsid' ")) {
                echo 'success';
            } else {
                echo 'failed';
            }
        }
        //update schedule table
        ExecuteSQLQuery("UPDATE schedul SET status = 1 WHERE id = '$scheduleid' ");
    }
}

function getStatusMessage($sta) {
    if (trim($sta) == '00') {
        $starep = "Delivered";
    } elseif (trim($sta) == 11) {
        $starep = "MISSING USERNAME";
    } elseif (trim($sta) == 12) {
        $starep = "MISSING PASSWORD";
    } elseif (trim($sta) == 13) {
        $starep = "TMISSING RECIPEINT";
    } elseif (trim($sta) == 14) {
        $starep = "MISSING SENDER";
    } elseif (trim($sta) == 15) {
        $starep = "MISSING MESSAGE";
    } elseif (trim($sta) == 21) {
        $starep = "SENDER ID TOO LONG";
    } elseif (trim($sta) == 22) {
        $starep = "INVALID RECIPIENT";
    } elseif (trim($sta) == 23) {
        $starep = "INVALID MESSAGE";
    } elseif (trim($sta) == 31) {
        $starep = "INVALID USERNAME";
    } elseif (trim($sta) == 32) {
        $starep = "INVALID PASSWORD";
    } elseif (trim($sta) == 33) {
        $starep = "INVALID LOGIN";
    } elseif (trim($sta) == 34) {
        $starep = "ACCOUNT DISABLED";
    } elseif (trim($sta) == 41) {
        $starep = "INSUFFICIENT CREDIT";
    } elseif (trim($sta) == 51) {
        $starep = "GATEWAY UNREACHABLE";
    } elseif (trim($sta) == 52) {
        $starep = "SYSTEM ERROR";
    } else {
        $starep = "Pending";
    }

    return $starep;
}

function do_response($url) {
    $headers = array("User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.8) Gecko/20061025 Firefox/2.0.0.1");
    try {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPGET, 1); // Make sure GET method it used
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Return the result
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIEJAR, '/tmp/cookies.txt');
        curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/cookies.txt');
        $res = curl_exec($ch); // Run the request
    } catch (Exception $ex) {

        $res = 'NOK';
    }
    return $res;
}
?>


