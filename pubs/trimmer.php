<?php

ini_set(display_errors,'Off');

function img_resize($tmpname,$size,$save_dir,$save_name,$maxisheight=1) {

$save_dir .= (substr($save_dir,-1) != "/") ? "/" : "";
$gis = getimagesize($tmpname);
$type = $gis[2];
$imorig = "";


$imorig = imagecreatefromjpeg($tmpname);
if($imorig == null) {
$imorig = imagecreatefrompng($tmpname);
if($imorig == null) {
$imorig = imagecreatefromgif($tmpname);
}
}

$x = imagesx($imorig);
$y = imagesy($imorig);

$woh = (!maxisheight) ? $gis[0] : $gis[1];
if($woh <= $size) {
$aw = $x;
$ah = $y;
}
else {
if(!maxisheight) {
$aw = $size;
$ah = $size * $y / $x;

}
else {
$aw = $size * $x / $y;
$ah = $size;
}
}

$im = imagecreatetruecolor($aw,$ah);
if(imagecopyresampled($im,$imorig,0,0,0,0,$aw,$ah,$x,$y)) 
if(imagejpeg($im,$save_dir.$save_name))
return true;
else 
return false;

}


?>