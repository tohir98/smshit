<?php
ini_set('display_errors', 1);
error_reporting(0);
session_start();
require("virtualphp.php");
$opCode = $_POST['opCode'];
$page1 = $_POST['page1'] !== '' ? $_POST['page1'] : '';

/* * ************* reCAPTCHA KEYS*************** */
$publickey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";
$privatekey = "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx";

function sendEmail($subject, $message, $recepient) {
    $post = [
        'subject' => $subject,
        'message' => $message,
        'recepient' => $recepient,
    ];
    
    $url = "http://crm.oaastudy.com/api/v1/Mail/sendmail";
    
    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_HEADER, true);
    curl_setopt($ch,CURLOPT_POST, 1); 
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
    curl_setopt($ch,CURLOPT_TIMEOUT, 20);
    $resp = curl_exec($ch);
    curl_close($ch);
    return true;
}

// Password and salt generation
function PwdHash($pwd, $salt = null) {
    if ($salt === null) {
        $salt = substr(md5(uniqid(rand(), true)), 0, SALT_LENGTH);
    } else {
        $salt = substr($salt, 0, SALT_LENGTH);
    }
    return $salt . sha1($pwd . $salt);
}

function Pcode() {
    $activation = substr(md5(microtime()), 0, 5);
    return $activation;
}

function do_response($url) {
    $headers = array("User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.8) Gecko/20061025 Firefox/1.5.0.8");
    try {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPGET, 1); // Make sure GET method it used
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Return the result
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIEJAR, '/tmp/cookies.txt');
        curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/cookies.txt');
        $res = curl_exec($ch); // Run the request
    } catch (Exception $ex) {

        $res = 'NOK';
    }
    return $res;
}

if ($opCode == "") {
    echo "empty";
} else if ($opCode == "REPORT") {

    $butclick = "";
    if (isset($_POST['butto'])) {
        $butclick = filter($_POST['butto']);
    };
    if (isset($_POST['count'])) {
        $sear = filter($_POST['count']);
    };
    if ($butclick == "Next") {
        if ($_SESSION['keysear'] == 20) {
            $display = $sear;
            $start = 20;
            $_SESSION['keysear'] = 20 + $sear;
        } else {
            $display = $sear;
            $start = $_SESSION['keysear'];
            $_SESSION['keysear'] = $start + $sear;
        }
    } elseif ($butclick == "Prev") {
        if ($_SESSION['keysear'] > 20) {
            $start = $_SESSION['keysear'] - $sear;
            $display = $sear;
            $_SESSION['keysear'] = $start;
        } else {
            $start = 0;
            $display = $_SESSION['keysear'];
        }
    } else {
        $_SESSION['keysear'] = 20;
        $start = 0;
        $display = $_SESSION['keysear'];
    }
    ?>

    <div class="report hideover">
        <div class="part2 hideover" style="width:90px">Messageid</div>
        <div class="part2 hideover" style="width:80px">Sent date</div>
        <div class="part2 hideover" style="width:70px">Time</div>
        <div class="part2 hideover" style="width:85px">Sender</div>
        <div class="part2 hideover" style="width:100px">Recipients</div>
        <div class="part2 hideover" style="width:60px">Country</div>
        <div class="part2 hideover" style="width:40px">SMS count</div>
        <div class="part2 hideover" style="width:77px">Status</div>
        <div class="part2 hideover" style="width:180px">Message</div>
    </div>

    <?php
    $user = $_SESSION['SMShitgenid'];

    $targetpage = "home.php?pageload=sms&load=Report";  //your file name  (the name of this file)
    $limit = 10;
    $adjacents = 3;
    if ($page1) {
        $start = ($page1 - 1) * $limit;    //first item to display on this page
    } else {
        $start = 0;
    }
    $qCount = ExecuteSQLQuery("SELECT * FROM smsoutbox WHERE sid='$user' ");
    $numRows = mysqli_num_rows($qCount);

    $sqlq = "select * from smsoutbox where sid='$user' ORDER BY id DESC LIMIT $start, $limit ";
    $viewq = ExecuteSQLQuery($sqlq);
    if ($viewq) {

        $numr = mysqli_num_rows($viewq);

        /* Setup page vars for display. */
        if ($page1 == 0)
            $page1 = 1;     //if no page var is given, default to 1.
        $prev = $page1 - 1;       //previous page is page - 1
        $next = $page1 + 1;       //next page is page + 1
        $lastpage = ceil($numRows / $limit);  //lastpage is = total pages / items per page, rounded up.
        $lpm1 = $lastpage - 1;

        /*
          Now we apply our rules and draw the pagination object.
          We're actually saving the code to a variable in case we want to draw it more than once.
         */
        $pagination = "";
        if ($lastpage > 1) {
            $pagination .= "<div class=\"pagination\">";
            //previous button
            if ($page1 > 1)
                $pagination.= " <a href=\"$targetpage&page1=$prev\">previous</a>";
            else
                $pagination.= "<span class=\"disabled\"> << previous</span>";

            //pages	
            if ($lastpage < 7 + ($adjacents * 2)) { //not enough pages to bother breaking it up
                for ($counter = 1; $counter <= $lastpage; $counter++) {
                    if ($counter == $page1)
                        $pagination.= "<span class=\"current\">$counter</span>";
                    else
                        $pagination.= "<a href=\"#\">$counter</a>";
                }
            }
            elseif ($lastpage > 5 + ($adjacents * 2)) { //enough pages to hide some
                //close to beginning; only hide later pages
                if ($page1 < 1 + ($adjacents * 2)) {
                    for ($counter = 1; $counter < 4 + ($adjacents * 2); $counter++) {
                        if ($counter == $page1)
                            $pagination.= "<span class=\"current\">$counter</span>";
                        else
                            $pagination.= "<a href=\"#\">$counter</a>";
                    }
                    $pagination.= "...";
                    $pagination.= "<a href=\"$targetpage&page1=$lpm1\">$lpm1</a>";
                    $pagination.= "<a href=\"$targetpage&page1=$lastpage\">$lastpage</a>";
                }
                //in middle; hide some front and some back
                elseif ($lastpage - ($adjacents * 2) > $page1 && $page1 > ($adjacents * 2)) {
                    $pagination.= "<a href=\"$targetpage&page1=1\">1</a>";
                    $pagination.= "<a href=\"$targetpage&page1=2\">2</a>";
                    $pagination.= "...";
                    for ($counter = $page1 - $adjacents; $counter <= $page1 + $adjacents; $counter++) {
                        if ($counter == $page1)
                            $pagination.= "<span class=\"current\">$counter</span>";
                        else
                            $pagination.= "<a href=\"#\">$counter</a>";
                    }
                    $pagination.= "...";
                    $pagination.= "<a href=\"$targetpage&page1=$lpm1\">$lpm1</a>";
                    $pagination.= "<a href=\"$targetpage&page1=$lastpage\">$lastpage</a>";
                }
                //close to end; only hide early pages
                else {
                    $pagination.= "<a href=\"$targetpage&page1=1\">1</a>";
                    $pagination.= "<a href=\"$targetpage&page1=2\">2</a>";
                    $pagination.= "...";
                    for ($counter = $lastpage - (2 + ($adjacents * 2)); $counter <= $lastpage; $counter++) {
                        if ($counter == $page1)
                            $pagination.= "<span class=\"current\">$counter</span>";
                        else
                            $pagination.= "<a href=\"#\">$counter</a>";
                    }
                }
            }

            //next button
            if ($page1 < $counter - 1) {
                $pagination.= " <a href=\"$targetpage&page1=$next\">Next </a>";
            } else
                $pagination.= "<span class=\"disabled\">next </span>";
            $pagination.= "</div>\n";
        }

        if ($numr > 0) {
            while ($row = mysqli_fetch_assoc($viewq)) {
                $tim = $row['time'] . $row['ampm'];
                ?>

                <div class="report2 hideover">
                    <div class="part2 hideover" style="width:90px"><?= $row['mid'] ? : '-'; ?></div>
                    <div class="part2 hideover" style="width:80px"><?= $row['date'] ?></div>
                    <div class="part2 hideover" style="width:70px"><?= $tim ?></div>
                    <div class="part2 hideover" style="width:85px"><?= $row['sname'] ?></div>
                    <div class="part2 hideover" style="width:100px"><?= $row['pnumber'] ?></div>
                    <div class="part2 hideover" style="width:60px; text-align:center"><?= $row['country'] ?></div>
                    <div class="part2 hideover" style="width:40px"><?= $row['smscount'] ?></div>
                    <div class="part2 hideover" style="width:77px;font-size:8pt"><?= strtolower($row['status']) ?></div>
                    <div class="part2 hideover" style="width:180px;font-size:8pt"><?= $row['message'] ?></div>

                </div>
                <?php
            }

            echo "<div style='padding-left:200px; margin-top:40px'>$pagination</div>";
        }
    }
} else if ($opCode == "TRANSFERSENDERUNIT") {
    $email = filter($_POST['uemail']);
    $unitt = filter($_POST['unit']);
    if (isset($_SESSION['SMShitgenid'])) {
        $user = $_SESSION['SMShitgenid'];
        $sql69 = "SELECT firstname,surname,email,credit FROM users WHERE id='$user'";
        $res69 = ExecuteSQLQuery($sql69);
        if ($res69) {
            $num69 = mysqli_num_rows($res69);
            if ($num69 == 1) {
                $fet69 = mysqli_fetch_assoc($res69);
                $nam = $fet69['surname'] . ' ' . $fet69['firstname'];
                $senEm = $fet69['email'];
                $senCrd = $fet69['credit'];
                if ($unitt > $senCrd) {
                    echo "Sorry, you can't tranfer more than you have";
                    exit();
                } else {
                    $newsenCrd = $senCrd - $unitt;
                    $_SESSION['SMShitcredit'] = $newsenCrd;
                }
            }
        }
    }
    $sql68 = "SELECT * FROM users WHERE email='$email'";
    $res68 = ExecuteSQLQuery($sql68);
    if ($res68) {
        $num68 = mysqli_num_rows($res68);
        if ($num68 == 1) {
            $fet68 = mysqli_fetch_assoc($res68);
            $uisd = $fet68['id'];
            $recCrd = $fet68['credit'] + $unitt;
            $sql40 = "INSERT INTO unitlog(sender,reciever,unit,operation,datecreated) VALUES('$user','$uisd','$unitt','Transfer',NOW())";
            $res40 = ExecuteSQLQuery($sql40);
            if ($res40) {

                $sqlUp = "UPDATE users SET credit='$newsenCrd' WHERE email='$senEm'";
                $resUp = ExecuteSQLQuery($sqlUp);
                if ($resUp) {
                    $sqlUp2 = "UPDATE users SET credit='$recCrd' WHERE email='$email'";
                    $resUp2 = ExecuteSQLQuery($sqlUp2);
                    if (!$resUp) {
                        echo "An error encounter. Please inform SMSHit of this transaction";
                        exit();
                    }
                } else {
                    echo "An error encounter. Please inform SMSHit of this transaction";
                    exit();
                }


                $request = "$nam you transfered sms unit of $unitt to $email account <br> Thanks <br> SMSHit Team";
                $request2 = "$nam transfered sms unit of $unitt to your account <br> Thanks <br> SMSHit Team";


                $subject = "SMSHit Unit Notification";
                $from = 'SMSHit <info@smshit.net>';
                $headers = 'From: ' . $from . PHP_EOL
                        . 'Reply-To: ' . $from . PHP_EOL
                        . 'X-Mailer: PHP/' . phpversion();

//                $sucess = mail($email, $subject, $request2, $headers);
                $success = sendEmail($subject, $request2, $email);
//                $sucess2 = mail($senEm, $subject, $request, $headers);
                $success2 = sendEmail($subject, $request, $senEm);
                if ($success) {
                    if ($success2) {
                        echo "Transfer successfull. Please check your email for verification.";
                    } else {
                        echo "Email sending failed to " . $senEm;
                    }
                } else {
                    echo "Email sending failed to " . $email;
                }
            } else {
                echo "Process failed";
            }
        } else {
            echo "Invalid email address";
        }
    }
} else if ($opCode == "ADMINHISUNIT") {
    $opt = filter($_POST['opt']);
    $userid = filter($_POST['userid']);
    $col = 0;
    if ($opt == "SMS Borrow History") {
        $sql42 = "SELECT * FROM unitlog WHERE reciever='$userid' ORDER BY datecreated desc";
        $res42 = ExecuteSQLQuery($sql42);
        if ($res42) {
            $num42 = mysqli_num_rows($res42);
            if ($num42 > 0) {
                echo '

		<div class="tpheader">
			<div class="tptext2" style="width:150px;">Sender</div>
			<div class="tptext2" style="width:150px;">Reciever</div>
			<div class="tptext2" style="width:120px;">Unit</div>
			<div class="tptext2" style="width:150px;">Operation</div>
			<div class="tptext2" style="width:150px;">Date created</div>
		</div>
		';
                $sum = "";
                $sumd = "";
                while ($fet42 = mysqli_fetch_assoc($res42)) {
                    if ($col == '0') {
                        $bgcolr = 'tpheader3';
                        $col++;
                    } else {
                        $bgcolr = 'tpheader2';
                        $col = 0;
                    }
                    echo '
					<div class="' . $bgcolr . '">
					<div class="tptext2" style="width:150px;">' . $fet42['sender'] . '</div>
					<div class="tptext2" style="width:150px;">' . $fet42['reciever'] . '</div>
					<div class="tptext2" style="width:120px;">' . $fet42['unit'] . '</div>
					<div class="tptext2" style="width:150px;">' . $fet42['operation'] . '</div>
					<div class="tptext2" style="width:150px;">' . $fet42['datecreated'] . '</div>
					</div>';

                    if ($fet42['operation'] == "Add") {
                        $sum = $sum + $fet42['unit'];
                    } else {
                        $sumd = $sumd + $fet42['unit'];
                    }
                }//end of while
                echo '
						<div class="tpheader">
							<div class="tptext2" style="width:150px;">The total borrowed unit is: ' . $sum . '</div>
						</div><br>
						<div class="tpheader">
							<div class="tptext2" style="width:150px;">The total deducted unit is: ' . $sumd . '</div>
						</div>
				
				';
            } else {
                echo "No record for this user";
            }
        } else {
            echo "Error encounted";
        }
    } elseif ($opt == "SMS Sent History") {
        echo ' <div class="tpheader">
		<div class="tptext2" style="width:25px;">SN</div>
	<div class="tptext2" style="width:90px">Messageid</div>
    <div class="tptext2" style="width:80px">Sent date</div>
    <div class="tptext2" style="width:80px">Time</div>
    <div class="tptext2" style="width:80px">Sender</div>
    <div class="tptext2" style="width:100px">Recipients</div>
    <div class="tptext2" style="width:60px">Country</div>
    <div class="tptext2" style="width:60px">SMS count</div>
    <div class="tptext2" style="width:70px">Status</div>
    <div class="tptext2" style="width:160px">Message</div>
</div>';
        $sqlq = "select * from smsoutbox where sid='$userid' ORDER BY id DESC";
        $viewq = ExecuteSQLQuery($sqlq);
        if ($viewq) {
            $cou = 1;
            while ($row = mysqli_fetch_assoc($viewq)) {
                if ($col == '0') {
                    $bgcolr = 'tpheader3';
                    $col++;
                } else {
                    $bgcolr = 'tpheader2';
                    $col = 0;
                }
                $tim = $row['time'] . $row['ampm'];
                echo '
	<div class="' . $bgcolr . '">
	<div class="tptext2" style="width:25px;">' . $cou . '</div>
	<div class="tptext2" style="width:90px">' . $row['mid'] . '</div>
    <div class="tptext2" style="width:80px">' . $row['date'] . '</div>
    <div class="tptext2" style="width:80px">' . $tim . '</div>
    <div class="tptext2" style="width:80px">' . $row['sname'] . '</div>
    <div class="tptext2" style="width:100px">' . $row['pnumber'] . '</div>
    <div class="tptext2" style="width:60px">' . $row['country'] . '</div>
    <div class="tptext2" style="width:60px">' . $row['status'] . '</div>
    <div class="tptext2" style="width:70px">' . $row['status'] . '</div>
    <div class="tptext2" style="width:160px">' . $row['message'] . '</div></div>';
                $cou++;
            }
        }
    } elseif ($opt == "Transfer") {

        echo ' <div class="tpheader">
	<div class="tptext2" style="width:120px">Senders name</div>
    <div class="tptext2" style="width:120px">Recievers name</div>
    <div class="tptext2" style="width:80px">Unit</div>
	<div class="tptext2" style="width:80px">Operation</div>
    <div class="tptext2" style="width:120px">Time</div>
</div>';
        $sqlq = "select * from unitlog where sender='$userid' or reciever='$userid' ORDER BY id DESC";
        $viewq = ExecuteSQLQuery($sqlq);
        if ($viewq) {
            while ($row = mysqli_fetch_assoc($viewq)) {
                if ($col == '0') {
                    $bgcolr = 'tpheader3';
                    $col++;
                } else {
                    $bgcolr = 'tpheader2';
                    $col = 0;
                }
                $tim = $row['datecreated'];
                echo '
	<div class="' . $bgcolr . '">
	<div class="tptext2" style="width:120px">' . $row['sender'] . '</div>
    <div class="tptext2" style="width:120px">' . $row['reciever'] . '</div>
    <div class="tptext2" style="width:80px">' . $row['unit'] . '</div>
	<div class="tptext2" style="width:80px">' . $row['operation'] . '</div>
    <div class="tptext2" style="width:120px">' . $row['datecreated'] . '</div></div>';
            }
        }
    } else {
        $sql43 = "SELECT * FROM users WHERE id='$userid'";
        $res43 = ExecuteSQLQuery($sql43);
        if ($res43) {
            $fet43 = mysqli_fetch_assoc($res43);
            echo '
				<table width="95%" cellpadding="1" cellspacing="1" id="view">
				<tr><td>Name:</td><td>' . $fet43['surname'] . ' ' . $fet43['firstname'] . '</td></tr>
				<tr><td>Email:</td><td>' . $fet43['email'] . '</td></tr>
				<tr><td>Phone Number:</td><td>' . $fet43['phone'] . '</td></tr>
				<tr><td>Date of Birth:</td><td>' . $fet43['day'] . '-' . $fet43['month'] . '</td></tr>
				<tr><td>Country:</td><td>' . $fet43['country'] . '</td></tr>
				<tr><td>Status:</td><td>' . $fet43['status'] . '</td></tr>
				<tr><td>Current Credit:</td><td>' . $fet43['credit'] . '</td></tr>
				<tr><td>Lastlogin:</td><td>' . $fet43['lastlogin'] . '</td></tr>
				<tr><td>Resellercode:</td><td>' . $fet43['resellercode'] . '</td></tr>
				<tr><td>Date Created:</td><td>' . $fet43['datecreated'] . '</td></tr>
				</table>
			';
        } else {
            echo "Error encountered";
        }
    }
} else if ($opCode == "ADMINADDUNIT") {
    $unit = filter($_POST['unit']);
    $userid = filter($_POST['userid']);
    $opt = filter($_POST['opt']);
    $sql1 = "SELECT credit,email FROM users WHERE id='$userid'";
    $res1 = ExecuteSQLQuery($sql1);
    if ($res1) {
        $fet1 = "";
        $bcd = "";
        $borowcredit = "";
        $fet1 = mysqli_fetch_assoc($res1);
        $red = $fet1['credit'];
        //$bcd = $fet1['borowcredit'];
        $email = $fet1['email'];
        $ad = $_SESSION['SMShitAdmingenid'];

        if (trim($opt) == trim('Add')) {

            $crd = $red + $unit;
            $borcrd = $bcd + $unit;
            $sql13 = "UPDATE users SET credit='$crd' WHERE id='$userid'";
            $res13 = ExecuteSQLQuery($sql13);
            if ($res13) {
                $sql40 = "INSERT INTO unitlog(sender,reciever,unit,operation,datecreated) VALUES('$ad','$userid','$unit','$opt',NOW())";
                $res40 = ExecuteSQLQuery($sql40);
                if ($res40) {
                    $headers = 'From: SMSHit <info@SMSHit.com>' . "<br>";

                    $request = "SMSHit added a unit of $unit to your account <br> Thanks <br> SMSHit Team";


                    $subject = "SMSHit Unit Notification";
                    $from = 'SMSHit <info@smshit.net>';
                    $headers = 'From: ' . $from . PHP_EOL
                            . 'Reply-To: ' . $from . PHP_EOL
                            . 'X-Mailer: PHP/' . phpversion();
//                    $sucess = mail($email, $subject, $request, $headers);
                    $success = sendEmail($subject, $request, $email);
                    if ($success) {

                        echo $unit . " Unit " . $opt . 'ed to user';
                    } else {
                        echo "Error sending mail to user";
                    }
                } else {
                    echo "Process failed";
                }
            } else {
                echo "Error in processing reqest";
            }
        } else {
            if ($unit > $red) {
                echo "Users unit " . $fet['credit'] . " is lesser to deducted unit";
            } else {
                $crd = $red - $unit;
                $borcrd = $bcd - $unit;
                $sql13 = "UPDATE users SET credit='$crd', borowcredit='$borcrd' WHERE id='$userid'";
                
                $res13 = ExecuteSQLQuery($sql13);
                if ($res13) {
                    $sql40 = "INSERT INTO unitlog(sender,reciever,unit,operation,datecreated) VALUES('$ad','$userid','$unit','$opt',NOW())";
                    $res40 = ExecuteSQLQuery($sql40);
                    if ($res40) {
                        $headers = 'From: SMSHit <info@SMSHit.com>' . "<br>";

                        $request = "SMSHit deducted unit of $unit from your account <br> Thanks <br> SMSHit Team";


                        $subject = "SMSHit Unit Notification";

                        $from = 'SMSHit <info@smshit.net>';
                        $headers = 'From: ' . $from . PHP_EOL
                                . 'Reply-To: ' . $from . PHP_EOL
                                . 'X-Mailer: PHP/' . phpversion();
//                        $sucess = mail($email, $subject, $request, $headers);
                        $sucess = sendEmail($subject, $request, $email);
                        
                        if ($sucess) {

                            echo $unit . " Unit " . $opt . 'ed to user';
                        } else {
                            echo "Error sending mail to user";
                        }
                    } else {
                        echo "Process failed";
                    }
                } else {
                    echo "Error in processing reqest";
                }
            }
        }
    } else {
        echo "An error encountered ";
    }
} elseif ($opCode == "SUBSCRIBE") {
    $subs = $_POST['subs'];
    if (!filter_var($subs, FILTER_VALIDATE_EMAIL)) {
        echo "This is not a valid email address";
        exit();
    }
    $sql33 = "SELECT * FROM newsletter WHERE email='$subs'";
    $res33 = ExecuteSQLQuery($sql33);
    if ($res33) {
        $num33 = mysqli_num_rows($res33);
        if ($num33 == 1) {
            echo "This email is in use";
        } else {
            $sql34 = "INSERT INTO newsletter(email) VALUES('$subs')";
            $res34 = ExecuteSQLQuery($sql34);
            if ($res34) {
                $headers = 'From: SMSHit <info@SMSHit.com>' . "<br>";

                $request = "Thank you for subcribing to our updates. <br> Thanks <br> SMSHit Team";


                $subject = "SMSHit Email Subscription Notification";

                $from = 'SMSHit <info@smshit.net>';
                $headers = 'From: ' . $from . PHP_EOL
                        . 'Reply-To: ' . $from . PHP_EOL
                        . 'X-Mailer: PHP/' . phpversion();
//                $sucess = mail($subs, $subject, $request, $headers);
                $success = sendEmail($subject, $request, $subs);
                if ($success) {

                    echo "Thank you for subcribing to us";
                } else {
                    echo "Error sending mail to user";
                }
            } else {
                echo "Error! sorry for any inconvinency";
            }
        }
    } else {
        echo "Error in processing";
    }
} elseif ($opCode == "EditPro") {
    $email = $_POST['email'];
    $fn = $_POST['fnn'];
    $sur = $_POST['sur'];
    $fon = $_POST['fon'];
    $coun = $_POST['coun'];
    if (isset($_SESSION['SMShitgenid'])) {
        $idf = $_SESSION['SMShitgenid'];
    } else {
        echo "Please login again";
        exit();
    }

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo "This is not a valid email address";
        exit();
    }
    $sql46 = "SELECT * FROM users WHERE id='$idf'";
    $res46 = ExecuteSQLQuery($sql46);
    if ($res46) {
        $fet46 = mysqli_fetch_assoc($res46);
        $em = $fet46['email'];
        $fnn = $fet46['firstname'];
        $sn = $fet46['surname'];
        $pno = $fet46['phone'];
        $cotry = $fet46['country'];
        $der = "";
        if ($cotry != $coun) {
            $der = "User changed Country,";
        }if ($email != $em) {
            $der = $der . " User changed Email,";
        }if ($fn != $fnn) {
            $der = $der . " User changed Firstname,";
        }
        if ($sn != $sur) {
            $der = $der . " User changed Surname,";
        }if ($fon != $pno) {
            $der = $der . " User changed Phone Number,";
        }
        $der = "No changes made to profile";
        $sql38 = "UPDATE users SET email='$email',firstname='$fn',surname='$sur',phone='$fon',country='$coun' WHERE id='$idf'";
        $res38 = ExecuteSQLQuery($sql38);
        if ($res38) {
            $_SESSION['SMShitname'] = $sur . ' ' . $fn;
            $sql45 = "INSERT INTO usersoperation(user_id,operation,datecreated) VALUES('$idf','$der', NOW())";
            $res45 = ExecuteSQLQuery($sql45) or die("Error in updating file");
            $dtt = date('Y M D H:i:s');


            $request = "Change of Profile Successfully made on $dtt, <br> You made the following changes,<br> $der <br> Thanks <br> SMSHit Team";


            $subject = "CHANGE OF PROFILE SMSHIT";
            $from = 'SMSHit <info@smshit.net>';
            $headers = 'From: ' . $from . PHP_EOL
                    . 'Reply-To: ' . $from . PHP_EOL
                    . 'X-Mailer: PHP/' . phpversion();
//            $sucess = mail($email, $subject, $request, $headers);
            $sucess = sendEmail($subject, $request, $email);
            if ($sucess) {

                echo("<p>Update successful</p>");
            } else {
                echo "Error sending mail to user";
            }
        } else {
            echo "Process failed";
        }
    } else {
        echo "Error ";
    }
} elseif ($opCode == "CHANGEUSERPASS") {
    $email = "";
    $sid = "";
    $email = $_POST['em'];
    $act = $_POST['act'];
    $np = $_POST['newp'];

    if ($act == 1) {
        $sql38 = "UPDATE users SET password='$np' WHERE email='$email'";
        $res38 = ExecuteSQLQuery($sql38);
        if ($res38) {



            $request = "Change of Password Successful, <br> Your new password is: $np  <br> Thanks <br> SMSHit Team";


            $subject = "CHANGE OF PASSWORD SMSHIT";
            $from = 'SMSHit <info@smshit.net>';
            $headers = 'From: ' . $from . PHP_EOL
                    . 'Reply-To: ' . $from . PHP_EOL
                    . 'X-Mailer: PHP/' . phpversion();
            //$sucess = mail($email, $subject, $request, $headers);
            $success = sendEmail($subject, $request, $email);
            if ($success) {

                echo '<p>Password change successfully.</p>';
            } else {
                echo "Error sending mail to user";
            }
        } else {
            echo "Process failed";
        }
    } else {
        echo "Process failed! Please contact SMShit customer care.";
    }
} elseif ($opCode == "SENDNOW") {
    $credit = $_POST['credit'];
    $pnumber = $_POST['pnumber'];
    $pnumba = $_POST['pnumber'];
    $sname = $_POST['sname'];
    //$message=addslashes($_POST['message']);
    $message = str_replace("", " ", $_POST['message']);
    $sendmess = stripslashes($_POST['message']);
    $sendpage = $_POST['state_count'];
    $country = $_POST['country'];
    //echo $country;
    //exit;
    $send_date = $_POST['dob'];
    $hr = $_POST['hr'];
    $mi = $_POST['mi'];
    $send_time = $hr . ":" . $mi . ":" . '00';
    $schedule = $_POST['schedule'];
    $date = date("dS F Y");
    $time = date("h:i:s");
    $ampm = date("A");
    $mid = substr(md5(microtime()), 0, 5);

    $order = array("\r\n", "\n", "\r");
    $pnumber = trim($pnumber);
    $lpnumber = strlen($pnumber);
    $pnumber = str_replace($order, ",", $pnumber);
    $pnumber4 = substr_replace($order, ",", $pnumber, $lpnumber);
    $pnumber = str_replace($order, ",", $pnumber);
    $nummo = $pnumber;
    $meslen = strlen($sendmess);
    $ids = $_SESSION['SMShitgenid'];
    $mtn_nig = 0;
    $cdma = 0;
    $subcdma = 0;
    $a = strlen($pnumber);
    if (substr($pnumber, $a - 1, 1) == ',') {
        $pnumber = substr($pnumber, 0, $a - 1);
    } else if (substr($pnumber, $a - 2, 1) == ',') {
        $pnumber = substr($pnumber, 0, $a - 2);
    }

    $j = 1;
    if ($meslen > 160 && $meslen < 306) {
        $j = 2;
    } else if ($meslen > 305 && $meslen < 458) {
        $j = 3;
    } elseif ($meslen > 457 && $meslen < 611) {
        $j = 4;
    } elseif ($meslen > 610 && $meslen < 751) {
        $j = 5;
    } elseif ($meslen > 750 && $meslen < 891) {
        $j = 6;
    }

    $nig = 0;
    $gsmarray = array('234905', '234811', '234805', '234807', '234705', '234815', '234903', '234810', '234814', '234803', '234806', '234703', '234706', '234813', '234816', '234802', '234808', '234809', '234818', '234909', '234817', '234809', '2347025', '2347027', '234708', '234812', '234902');
    $pnumber = explode(",", $pnumber);
    if (trim($country) == "NG") {
        $iii = count($pnumber) * $j;
    } else {
        $iii = count($pnumber) * 5 * $j;
    }




    if ($pb_mnumber == '' && $totalnum == '' && $_POST[pnumber] == '') {
        echo "<div id=mes>You have not added a phone number, Kindly Check this </div>";
    } elseif ($j > 6) {
        echo "Message too long";
    } elseif ($credit < $iii) {
        echo "<div id=mes>You have insufficient sms credit for the attempted transaction </div><br><br>";
        echo '<form name="relsm" action="http://smshit.net/home.php?pageload=profile&load=buysms" method="post"><input type="submit" name="relsms" id="relsms" class="pstbutton forpst" value="Buy sms" /></form>';
    } elseif ($country == "select country") {
        echo "<div id=mes>You have not selected a destination </div>";
    } elseif ($sname == '') {
        echo "<div id=mes>You have not added a Sender's name, Kindly Check this </div>";
    } elseif ($message == '') {
        echo "<div id=mes>You have not added a message, Kindly Check this </div>";
    } elseif ($schedule == 'sendlater' && $send_date == '') {
        echo "<div id=mes>You have not added a date, Kindly Check this </div>";
    } elseif ($schedule == 'sendlater' && $send_time == '') {
        echo "<div id=mes>You have not added a time, Kindly Check this </div>";
    } else {
        $pnumber1 = explode(",", $nummo);
        for ($ii = 0; $ii < count($pnumber1); $ii++) {

            $pn = $pnumber1[$ii];
            if (substr($pn, 0, 3) != '234' && (substr($pn, 0, 2) == trim('08') || substr($pn, 0, 2) == trim('07') || substr($pn, 0, 2) == trim('09'))) {
                if ($ii == 0) {
                    if (substr($pn, 1, 11) != "") {
                        $pnum = '234' . substr($pn, 1, 11);
                    }
                } else {
                    if (substr($pn, 1, 11) != "") {
                        $pnum = $pnum . ',' . '234' . substr($pn, 1, 11);
                    }
                }
            } elseif (substr($pn, 0, 3) == '234' && (substr($pn, 3, 1) == trim('8') || substr($pn, 3, 1) == trim('7') || substr($pn, 3, 1) == trim('9'))) {
                if ($ii == 0) {
                    $pnum = $pn;
                } else {
                    $pnum = $pnum . ',' . $pn;
                }
            } else {
                if ($ii == 0) {
                    $pnum1 = $pn;
                } else {
                    $pnum1 = $pnum1 . ',' . $pn;
                }
            }
        }



        $ncredit = $credit - $iii;
        $_SESSION['SMShitcredit'] = $ncredit;
        $sql77 = "UPDATE users SET credit='$ncredit' WHERE id='$ids'";
        $res77 = ExecuteSQLQuery($sql77);
        if (!$res77) {
            echo '<div>Error updating credit</div>';
            exit();
        }



        if ($schedule == 'sendnow') {


            if (trim($country) == "NG") {
                if (count($pnum1) > 0) {
                    echo 'The following numbers are invalid for nigeria network ' . $pnum1 . '<form name="relbut" action="http://smshit.net/home.php" method="post"><input type="submit" name="relbut" id="relbut" class="pstbutton forpst" value="Go back" /></form>';
                } else {
                    //$url = urlencode("www.v2nmobile.co.uk/api/httpsms.php?u=tundeaminu@gmail.com&p=olatunde2711&m=$message&r=$nummo&s=$sname&t=1");
                    $u = "tundeaminu@gmail.com";
                    $p = "olatunde2711";
                    $m = $message;
                    $r = $pnum;
                    $s = $sname;
                    $t = "1";
                    /* $url = "http://www.v2nmobile.co.uk/api/httpsms.php?"
                      ."u=" . UrlEncode($u)
                      ."&p=" . UrlEncode($p)
                      ."&m="	. UrlEncode($m)
                      ."&r=" . UrlEncode($pnum)
                      ."&s=" . UrlEncode($s)
                      ."&t=" . UrlEncode($t);
                     */

                    try {
                        $cr = explode(",", $pnum);
                        for ($cou = 0; $cou < count($cr); $cou++) {
                            $numr = $cr[$cou];
                            $url = "http://www.v2nmobile.co.uk/api/httpsms.php?"
                                    . "u=" . UrlEncode($u)
                                    . "&p=" . UrlEncode($p)
                                    . "&m=" . UrlEncode($m)
                                    . "&r=" . UrlEncode($numr)
                                    . "&s=" . UrlEncode($s)
                                    . "&t=" . UrlEncode($t);


                            $sta = do_response($url);


                            if (trim($sta) == '00') {
                                $starep = "Delivered";
                            } elseif (trim($sta) == 11) {
                                $starep = "MISSING USERNAME";
                            } elseif (trim($sta) == 12) {
                                $starep = "MISSING PASSWORD";
                            } elseif (trim($sta) == 13) {
                                $starep = "TMISSING RECIPEINT";
                            } elseif (trim($sta) == 14) {
                                $starep = "MISSING SENDER";
                            } elseif (trim($sta) == 15) {
                                $starep = "MISSING MESSAGE";
                            } elseif (trim($sta) == 21) {
                                $starep = "SENDER ID TOO LONG";
                            } elseif (trim($sta) == 22) {
                                $starep = "INVALID RECIPIENT";
                            } elseif (trim($sta) == 23) {
                                $starep = "INVALID MESSAGE";
                            } elseif (trim($sta) == 31) {
                                $starep = "INVALID USERNAME";
                            } elseif (trim($sta) == 32) {
                                $starep = "INVALID PASSWORD";
                            } elseif (trim($sta) == 33) {
                                $starep = "INVALID LOGIN";
                            } elseif (trim($sta) == 34) {
                                $starep = "ACCOUNT DISABLED";
                            } elseif (trim($sta) == 41) {
                                $starep = "INSUFFICIENT CREDIT";
                            } elseif (trim($sta) == 51) {
                                $starep = "GATEWAY UNREACHABLE";
                            } elseif (trim($sta) == 52) {
                                $starep = "SYSTEM ERROR";
                            } else {
                                $starep = "SYSTEM ERROR";
                            }




                            $sql = "INSERT INTO smsoutbox(pnumber,sname,date,message,status,sid,smscount,country,time,ampm,mid) VALUES('$numr','$sname','$date','$message','$starep','$ids','$j','$country','$time','$ampm','$mid')";
                            $resultc = ExecuteSQLQuery($sql);
                        }




                        echo '<div class="alert alert-success">           
                <strong>Well done!</strong> Your Message has been successfully sent.</div>
				You have used <strong style="color:#F00">' . $iii . ' sms credits </strong> <br>

	
		<span  id="small2">You have <strong style="color:#F00">' . $ncredit . '</strong> sms credits left  </span>';
                    } catch (Exception $ex) {
                        echo $res = 'NOK';
                    }
                }
            } elseif ($country != "NG") {
                $user = "tunde11";
                $password = "OLA^27*11";

                //require_once('sendSMSclass.php');




                $username = $user;     //your username
                $password = $password;     //your password
                $sender = $sname;
                $isflash = $_POST['isflash'];       //Is flash message (1 or 0)
                $type = "longSMS";
                $bookmark = $_POST['bookmark'];  //wap url (example: www.google.com)
                $message = $sendmess;


                $sender = str_replace("+", "%2b", $sender);
                $message = str_replace("+", "%2b", $message);

                //$SENDSMS = new SendSMSclass();
                //$response = $SENDSMS->SendSMS($username,$password,$sender,$message,$isflash, $nummo, $type, $bookmark); //IsFlash must be 0 or 1
//$url = 'http://api.infobip.com/api/v3/sendsms/plain?user=$user&password=$password&sender=$sname&SMSText=$message&GSM=$nummo';
//$urll = 'http://api.infobip.com/api/v3/sendsms/plain?user=tunde11&password=OLA^27*11&sender=Friend&SMSText=messagetext&GSM=2348037002523';
                $url = "http://api.infobip.com/api/v3/sendsms/plain?"
                        . "user=" . UrlEncode($user)
                        . "&password=" . UrlEncode($password)
                        . "&sender=" . UrlEncode($sname)
                        . "&SMSText=" . UrlEncode($message)
                        . "&GSM=" . UrlEncode($nummo);

                try {
                    $xml = simplexml_load_file($url);
                    for ($i = 0; $i < sizeof($xml); $i++) {
                        $sta = $xml->result[$i]->status;

                        $msgid = $xml->result[$i]->messageid;

                        $des = $xml->result[$i]->destination;


                        if ($sta == 0) {
                            $starep = "Delivered";
                        } elseif ($sta == -1) {
                            $starep = "Error in processing the request";
                        } elseif ($sta == -2) {
                            $starep = "Internal error";
                        } elseif ($sta == -3) {
                            $starep = "Targeted network is not covered on specific account";
                        } elseif ($sta == -5) {
                            $starep = "Username or password is invalid";
                        } elseif ($sta == -6) {
                            $starep = "Destination address is missing in the request";
                        } elseif ($sta == -10) {
                            $starep = "Username is missing in the request";
                        } elseif ($sta == -11) {
                            $starep = "Password is missing in the request";
                        } elseif ($sta == -13) {
                            $starep = "Number is not recognized by smshit platform";
                        } elseif ($sta == -22) {
                            $starep = "Incorrect XML format, caused by syntax error";
                        } elseif ($sta == -23) {
                            $starep = "General error, reasons may vary";
                        } elseif ($sta == -26) {
                            $starep = "General API error, reasons may vary";
                        } elseif ($sta == -27) {
                            $starep = "Invalid scheduling parametar";
                        } elseif ($sta == -28) {
                            $starep = "Invalid PushURL in the request";
                        } elseif ($sta == -30) {
                            $starep = "Invalid APPID in the request";
                        } elseif ($sta == -33) {
                            $starep = "Duplicated MessageID in the request";
                        } elseif ($sta == -34) {
                            $starep = "Sender name is not allowed";
                        } elseif ($sta == -99) {
                            $starep = "Error in processing request, reasons may vary";
                        }

                        $sql = "insert into smsoutbox(pnumber,sname,date,message,status,sid,smscount,country,time,ampm,mid) VALUES('$des','$sname','$date','$message','$starep','$ids','$j','$country','$time','$ampm','$msgid')";
                        $resultc = ExecuteSQLQuery($sql);
                    }



                    echo '<div class="alert alert-success">           
                <strong>Well done!</strong> Your Message has been successfully sent.</div>
				You have used <strong style="color:#F00">' . $iii . ' sms credits </strong> <br>

	
		<span  id="small2">You have <strong style="color:#F00">' . $ncredit . '</strong> sms credits left  </span>';
                } catch (Exception $ex) {

                    echo $res = 'NOK';
                }
            }


            /*
              if (ceil(strlen($message)>160)){
              $urltouse = "http://www.infobip.com/Addon/SMSService/SendSMS.aspx?user=$user&password=$password&sender=$sname&SMSText=$message&type=LongSMS&IsFlash=0&GSM=$pn";}
              else{ $urltouse = "http://www.infobip.com/Addon/SMSService/SendSMS.aspx?user=$user&password=$password&sender=$sname&SMSText=$message&IsFlash=0&GSM=$pn";} */

            ///$url='http://www.google.com/index.php?go=1';
            ///include($urltouse);
            ///$key = file_get_contents($urltouse);
            ///  return $key;
            //require_once('sendSMS.php');
            //$gsm = array(); //gsm numbers must be in the array
            //echo 'Hello num: '.$nummo;
            //$gsmm = '2348037002523,2348155620477';
            //$host = "121.241.242.114";
            //$port = 8080;
            //$username 		= $user;    	//your username
            //$password 		= $password;    	//your password
            //$sender 		= $sname;
            //$isflash 		= $_POST['isflash'];      	//Is flash message (1 or 0)
            //$msgtype		= 0;
            //$dlr 			= 1;		
            //$message	 	= $sendmess;
            // Note: replace sign "+" with "%2b" for sender and message text or it will be replaced with empty space
            //$sender = str_replace("+","%2b",$sender);
            //$message = str_replace("+","%2b",$message);
            //$obj = new Sender ($host,$port,$username,$password,$sender,$message,$nummo,$msgtype,$dlr);
            //$obj->Submit ();
            //Call The Constructor.
//$host,$port,$username,$password,$sender, $message,$mobile,$msgtype,$dlr
//$obj = new Sender("121.241.242.114","8080","tund-femi11","ola2711","Abcde","Love you jesus","2348037002523" ,"0","1");
//$obj->Submit ();
            //$live_url = "http://121.241.242.114:8080/bulksms/bulksms?username=$username&password=$password&type=0&dlr=1&destination=$gsm&source=$sender&message=$messagetext";
            //$parse_url = file($live_url);
            //echo $parse_url[0];
        }//end of schedule now






        if ($schedule == 'sendlater') {
            $status = 'pending';
            $sqlsm = "insert into smsoutbox(pnumber,sname,date,message,status,sid,smscount,country,time,ampm,mid) VALUES('$nummo','$sname','$date','$message','$status','$ids','$j','$country','$time','$ampm','$mid')";
            $resultcsm = ExecuteSQLQuery($sqlsm);


            $sqly = "insert into schedul(sid,send_date,send_time,status,mid,message,number,sender) VALUES('$ids','$send_date','$send_time','$status','$mid' 
					,'$message','$nummo','$sname')";
            $resulty = ExecuteSQLQuery($sqly);

            echo '<div class="alert alert-success">           
                <strong>Well done!</strong> Your Message has been successfully Schedule.</div>';
        }
    }//end of else








    if ($resultc) {
        if ($nig != 0) {
            echo "<div id=mes>  You have been charged $intbill sms credits for $intpage page(s) sent to $nig international number </div>";
        }

        if ($intbill != 0) {
            //echo"
            //Number of international sms(s) sent: <strong style='color:#F00'>$nig</strong> <br /></div>
            //Number of sms credit(s) charged for international sms: <strong style='color:#F00'>$intbill sms credits</strong> <br /></div>";
        }
        if ($localpage != 0) {
            //echo"
            //Number of local sms(s) sent: <strong style='color:#F00'>$local</strong> <br /></div>
            //Number of sms credit(s) charged for local sms: <strong style='color:#F00'>$localpage sms credit(s) to other networks + $localmtn sms credit(s) to MTN 			
            //Nigeria </strong> <br /></div>";
        }
        if ($cdmabill != 0) {
            //echo"
            //Number of CDMA sms(s) sent: <strong style='color:#F00'>$cdma</strong> <br /></div>
            //Number of sms credit(s) charged for CDMA sms: <strong style='color:#F00'>$cdmabill sms credits</strong> <br /></div>";
        }
    } else {



        if (isset($_POST['sender'])) {

            $rid = $_POST['rid'];
        }


        if (isset($_GET['send'])) {
            echo "<b>Sending Successful </b><br><br>";
        }
    }
} else if ($opCode == "USERMAIL") {
    $msg = $_POST['msg'];
    $sql33 = "SELECT * FROM users";
    $res33 = ExecuteSQLQuery($sql33);
    if ($res33) {
        while ($fet33 = mysqli_fetch_assoc($res33)) {
            $email = $fet33['email'];
            $name = $fet33['firstname'] . ' ' . $fet33['surname'];
            $headers = 'From: SMSHit <info@SMSHit.com>' . "<br>";

            $request = "Dear $name, <br> $msg <br> Best regards <br> SMSHit Team";


            $subject = "SMSHIT UPDATES";
            $from = 'SMSHit <info@smshit.net>';
            $headers = 'From: ' . $from . PHP_EOL
                    . 'Reply-To: ' . $from . PHP_EOL
                    . 'X-Mailer: PHP/' . phpversion();
//            $sucess = mail($email, $subject, $request, $headers);
            $sucess = sendEmail($subject, $request, $email);
            if ($sucess) {
                $sql12 = "UPDATE admin SET password='$activation' WHERE id='$id'";
                $res12 = ExecuteSQLQuery($sql12);
                if ($res12) {
                    $fr = "<p>Message successfully sent to users!</p>";
                } else {
                    echo "Error processing request";
                }
            } else {
                echo "Error sending mail to user";
            }
        }
        echo("<p>Message successfully sent to users!</p>");
    }
} else if ($opCode == "UPDATECUR") {
    $ccur = $_POST['ccur'];
    $cprice = $_POST['curprice'];
    $sql31 = "SELECT * FROM currency WHERE curren='$ccur'";
    $res31 = ExecuteSQLQuery($sql31);
    if ($res31) {
        $num31 = mysqli_num_rows($res31);
        if ($num31 == 1) {
            $sql32 = "UPDATE currency SET price='$cprice' WHERE curren='$ccur'";
            $res32 = ExecuteSQLQuery($sql32);
            if ($res32) {
                echo "Currency updated";
            } else {
                echo "Error in updating currency";
            }
        } else {
            echo "Problem in inserting data " . $ccur;
        }
    }
} else if ($opCode == "rePAYMENT") {
    $code = $_POST['rcode'];
    $sql18 = "SELECT * FROM upgrade WHERE code='$code'";
    $res18 = ExecuteSQLQuery($sql18);
    if ($res18) {
        $num18 = mysqli_num_rows($res18);
        if ($num18 == 1) {
            $fet18 = mysqli_fetch_assoc($res18);
            if (isset($_SESSION['SMShitgenid'])) {
                $user = $_SESSION['SMShitgenid'];
                $sellerid = $fet18['user_id'];
                $sql19 = "INSERT INTO resellerusers(resellerid,user_id,datecreated) VALUES('$sellerid','$user',NOW())";
                $res19 = ExecuteSQLQuery($sql19);
                if ($res19) {
                    echo "success";
                } else {
                    echo "Error";
                }
            } else {
                echo "Please login properly";
            }
        } else {
            echo "Incorrect code";
        }
    } else {
        echo "Invalid code";
    }
} else if ($opCode == "GETPRICE") {
    $cur = $_POST['curr'];
    $qu = $_POST['qu'];

    if ($cur != 'Naira') {

        $sql17 = "SELECT * FROM currency WHERE curren='$cur'";
        $res17 = ExecuteSQLQuery($sql17);
        if ($res17) {
            $num17 = mysqli_num_rows($res17);
            if ($num17 == 1) {
                $fet17 = mysqli_fetch_assoc($res17);
                $curprice = $fet17['price'];
                if ($qu > 0 && $qu < 5000) {
                    $toprice = $qu * 2.0 * $curprice;
                } else if ($qu >= 5000) {
                    $toprice = $qu * 1.80 * $curprice;
                }
            }
        }
    } else {
        if ($qu > 0 && $qu < 5000) {
            $toprice = $qu * 2.0;
        } else if ($qu >= 5000) {
            $toprice = $qu * 1.80;
        }
    }

    $_SESSION['unitget'] = $qu;
    $_SESSION['toprice'] = $toprice;
    $_SESSION['pikcur'] = $cur;
    echo $toprice;
} else if ($opCode == "UPGRADE") {
    if (isset($_SESSION['SMShitgenid'])) {
        $chk = $_POST['agree'];
        if (isset($chk) == 'agree') {
            $id = $_SESSION['SMShitgenid'];
            $sql15 = "SELECT * FROM upgrade WHERE user_id='$id'";
            $res15 = ExecuteSQLQuery($sql15);
            if ($res15) {
                $num15 = mysqli_num_rows($res15);
                if ($num15 > 0) {
                    echo "This account has been upgraded to reseller's account";
                } else {
                    $cod = GenKey();
                    if (!ctype_alpha($cod)) {
                        $cod = $cod;
                    } else {
                        GenKey();
                    }
                    $sql15 = "SELECT * FROM upgrade WHERE code='$cod'";
                    $res15 = ExecuteSQLQuery($sql15);
                    if ($res15) {
                        $num15 = mysqli_num_rows($res15);
                        if ($num15 > 0) {
                            echo "Unable to generate!please try again";
                        } else {
                            $sql16 = "INSERT INTO upgrade(user_id,code,datecreated) VALUES('$id', '$cod',NOW())";
                            $res16 = ExecuteSQLQuery($sql16);
                            if ($res16) {
                                $qry2 = "Select * from users Where id='$id'";
                                $re2 = ExecuteSQLQuery($qry2);
                                if ($re2) {
                                    $num2 = mysqli_num_rows($re2);
                                    if ($num2 == 1) {
                                        $fet = mysqli_fetch_assoc($re2);
                                        $em = $fet['email'];
                                    }
                                }
                                echo 'Your code is ' . $cod . '. Your code has been forwarded to ' . $em;
                                $headers = 'From: SMSHit <info@SMSHit.com>' . "<br>";

                                $request = "This is your reseller's code, <br> $cod  <br> Thanks <br> SMSHit Team";


                                $subject = "SMSHit RESELLER's CODE";
                                $from = 'SMSHit <info@smshit.net>';
                                $headers = 'From: ' . $from . PHP_EOL
                                        . 'Reply-To: ' . $from . PHP_EOL
                                        . 'X-Mailer: PHP/' . phpversion();
//                                $sucess = mail($em, $subject, $request, $headers);
                                $success = sendEmail($subject, $request, $em);
                                
                                if ($success) {

                                    echo "<p>Message successfully sent to your mail!</p>";
                                } else {
                                    echo "Error sending mail to user";
                                }
                            } else {
                                echo "Error in processing";
                            }
                        }
                    } else {
                        echo "Error in processing";
                    }
                }
            } else {
                echo "Error in processing";
            }
        } else {
            echo "You have to agree to the terms and condition.Thanks";
        }
    }
} else if ($opCode == "FORGOT") {
    $username = $_POST['username'];

    $sql10 = "SELECT * FROM admin WHERE username = '$username'";
    $res10 = ExecuteSQLQuery($sql10);
    if ($res10) {
        $num10 = mysqli_num_rows($res10);
        if ($num10 == 1) {
            $fet10 = mysqli_fetch_assoc($res10);
            $id = $fet10['id'];
            $ids = sha1($id);
            $em = $fet10['email'];
            $activation = substr(md5(microtime()), 0, 8);
            $activations = sha1(substr(md5(microtime()), 0, 6));
            $verifyurl = 'http://smshit.net/Admin/passverifystring.php?id=' . $ids . '&email=' . $em . '&sys=' . $activation;
            $time = date("h:i:s") . date("A");

            $sql11 = "INSERT INTO changepass(userid,mdstring,active,timech) VALUES('$id','$activation','0','$time')";
            $res11 = ExecuteSQLQuery($sql11);
            if ($res11) {


                $request = "Request for password change, <br> Please follow this link to change your password <br> $verifyurl  <br> Thanks <br> SMSHit Team";


                $subject = "FORGOT PASSWORD SMSHIT";
                $from = 'SMSHit <info@smshit.net>';
                $headers = 'From: ' . $from . PHP_EOL
                        . 'Reply-To: ' . $from . PHP_EOL
                        . 'X-Mailer: PHP/' . phpversion();
//                $sucess = mail($em, $subject, $request, $headers);
                $sucess = sendEmail($subject, $request, $em);
                
                if ($sucess) {

                    echo "<p>Message successfully sent to your mail!</p>";
                } else {
                    echo "Error sending mail to user";
                }
            } else {
                echo "Error in processing";
            }
        } else {
            echo "Incorrect username";
        }
    } else {
        echo "Error processing request";
    }
} else if ($opCode == "FORG") {
    $email = $_POST['email'];

    $sql10 = "SELECT * FROM users WHERE email = '$email'";
    $res10 = ExecuteSQLQuery($sql10);
    if ($res10) {
        $num10 = mysqli_num_rows($res10);
        if ($num10 == 1) {
            $fet10 = mysqli_fetch_assoc($res10);
            $id = $fet10['id'];
            $ids = sha1($id);
            $em = $fet10['email'];
            $activation = substr(md5(microtime()), 0, 8);
            $activations = sha1(substr(md5(microtime()), 0, 6));
            $verifyurl = 'http://smshit.net/passverifystring.php?id=' . $ids . '&email=' . $em . '&sys=' . $activation;
            $time = date("h:i:s") . date("A");

            $sql11 = "INSERT INTO changepass(userid,mdstring,active,timech) VALUES('$id','$activation','0','$time')";
            $res11 = ExecuteSQLQuery($sql11);
            if ($res11) {
                $from = 'SMSHit <info@smshit.net>';
                $headers = 'From: ' . $from . PHP_EOL
                        . 'Reply-To: ' . $from . PHP_EOL
                        . 'X-Mailer: PHP/' . phpversion();

                $request = "Request for password change, <br> Please follow this link to change your password <br> $verifyurl  <br> Thanks <br> SMSHit Team";


                $subject = "FORGOT PASSWORD SMSHIT";

//                $sucess = mail($em, $subject, $request, $headers);
                $sucess = sendEmail($subject, $request, $em);
                if ($sucess) {
                    echo '<p>New password link successful sent to</p> ' . $em;
                } else {
                    echo("<p>Could not deliver an email to your emailID...</p>");
                }
            } else {
                echo "Error in processing";
            }
        } else {
            echo "Incorrect username";
        }
    } else {
        echo "Error processing request";
    }
} else if ($opCode == "VERIFY") {

    $email = filter($_POST['email']);
    $pass = filter($_POST['pass']);
    if (isset($_POST['chk']) && $_POST['chk'] != "") {
        $post_autologin = 1;
    } else {
        $post_autologin = "";
    }

    $Query = "";

    $qry2 = "Select * from users Where email = '$email' AND password = '$pass'";
    $re2 = ExecuteSQLQuery($qry2);
    if ($re2) {
        if ($post_autologin == 1) {
            $cookie_name = 'SMSHitsiteAuth';
            $cookie_time = (3600 * 24 * 30); // 30 days
            setcookie("cookpa", $pass, time() + 60 * 60 * 24 * 100, "/");
            setcookie("cookem", $email, time() + 60 * 60 * 24 * 100, "/");
        } else {
            setcookie("cookem", "", time() - 60 * 60 * 24 * 100, "/");
            setcookie("cookpa", "", time() - 60 * 60 * 24 * 100, "/");
        }
        $num2 = mysqli_num_rows($re2);
        if ($num2 == 1) {
            $bcrd = "";
            $intega = "";
            $fet2 = mysqli_fetch_assoc($re2);
            $fullname = $fet2['surname'] . " " . $fet2['firstname'];
            $bcrd = $fet2['borowcredit'];
            $regid = $fet2['id'];
            $cred = $fet2['credit'];
            for ($t = 0; $t < 3; $t++) {
                $intega .= mt_rand(100, 300);
            }
            $_SESSION['SMShitcrdown'] = $bcrd;
            $_SESSION['SMShitname'] = $fullname;
            $_SESSION['SMShitusid'] = $intega;
            $_SESSION['SMShitgenid'] = $regid;
            $_SESSION['SMShitcredit'] = $cred;
            $_SESSION['SMShitsecagt'] = md5($_SERVER['HTTP_USER_AGENT']);
            $secagt = $_SESSION['SMShitsecagt'];
            $usid = $_SESSION['SMShitusid'];
            $qry3 = "Select * from currentsess Where reg_id = '$regid'";
            $re3 = ExecuteSQLQuery($qry3);
            if ($re3) {
                $num3 = mysqli_num_rows($re3);
                if ($num3 > 0) {
                    $fet3 = mysqli_fetch_assoc($re3);
                    if (!empty($fet3['sess']) AND ! empty($fet3['gen_id'])) {
                        if ($_SESSION['SMShitsecagt'] == $fet3['sess']) {
                            $qry4 = "UPDATE currentsess SET sess = '$secagt',gen_id = '$usid' Where reg_id = '$regid'";
                            $re4 = ExecuteSQLQuery($qry4);
                            if ($re4) {
                                echo "sucesslogin";
                            } else {
                                echo "Updating session failed";
                            }
                        } else {
                            $qry4 = "INSERT INTO currentsess(reg_id,sess,gen_id) VALUES('$regid','$secagt','$usid')";
                            $re4 = ExecuteSQLQuery($qry4);
                            if ($re4) {
                                echo "Someone else is using your account";
                            } else {
                                echo "Updating session failed";
                            }
                        }
                    } else {

                        $qry5 = "INSERT INTO currentsess(reg_id,sess,gen_id) VALUES('$regid','$secagt','$usid')";
                        $re5 = ExecuteSQLQuery($qry5);
                        if ($re5) {
                            echo "successtwo";
                        } else {
                            echo "Session not ready";
                        }
                    }
                } else {
                    $qry5 = "INSERT INTO currentsess(reg_id,sess,gen_id) VALUES('$regid','$secagt','$usid')";
                    $re5 = ExecuteSQLQuery($qry5);
                    if ($re5) {
                        echo "successtwo";
                    } else {
                        echo "Program was enable to login";
                    }
                }
            } else {
                echo "Error in starting session";
            }
        } else {
            echo "Invalid username or password";
        }
    }
} else if ($opCode == "AdminLogin") {

    $username = $_POST['username'];
    $pass = $_POST['Password'];

    $qry2 = "Select * from admin Where username = '$username' AND password = '$pass'";
    $re2 = ExecuteSQLQuery($qry2);
    if ($re2) {
        $num2 = mysqli_num_rows($re2);
        if ($num2 == 1) {
            $sql50 = "UPDATE admin SET login_time=NOW()";
            $res50 = ExecuteSQLQuery($sql50) or die("Error found");
            $fet2 = mysqli_fetch_assoc($re2);

            $regid = $fet2['id'];
            $cred = $fet2['credit'];
            $intega = "";
            for ($t = 0; $t < 3; $t++) {
                $intega .= mt_rand(100, 300);
            }
            $_SESSION['SMShitAdminUsername'] = $username;
            $_SESSION['SMShitAdminusid'] = $intega;
            $_SESSION['SMShitAdmingenid'] = $regid;
            $_SESSION['SMShitAdmincredit'] = $cred;
            $_SESSION['SMShitAdminsecagt'] = md5($_SERVER['HTTP_USER_AGENT']);
            $secagt = $_SESSION['SMShitAdminsecagt'];
            $usid = $_SESSION['SMShitAdminusid'];
            $qry3 = "Select * from currentsess Where reg_id = '$regid'";
            $re3 = ExecuteSQLQuery($qry3);
            if ($re3) {
                $num3 = mysqli_num_rows($re3);
                if ($num3 > 0) {
                    $fet3 = mysqli_fetch_assoc($re3);
                    if (!empty($fet3['sess']) AND ! empty($fet3['gen_id'])) {
                        if ($_SESSION['SMShitAdminsecagt'] == $fet3['sess']) {
                            $qry4 = "UPDATE currentsess SET sess = '$secagt',gen_id = '$usid' Where reg_id = '$regid'";
                            $re4 = ExecuteSQLQuery($qry4);
                            if ($re4) {
                                echo "sucesslogin";
                            } else {
                                echo "Updating session failed";
                            }
                        } else {
                            $qry4 = "INSERT INTO currentsess(reg_id,sess,gen_id) VALUES('$regid','$secagt','$usid')";
                            $re4 = ExecuteSQLQuery($qry4);
                            if ($re4) {
                                echo "Sucessone";
                            } else {
                                echo "Updating session failed";
                            }
                        }
                    } else {

                        $qry5 = "INSERT INTO currentsess(reg_id,sess,gen_id) VALUES('$regid','$secagt','$usid')";
                        $re5 = ExecuteSQLQuery($qry5);
                        if ($re5) {
                            echo "successtwo";
                        } else {
                            echo "Session not ready";
                        }
                    }
                } else {
                    $qry5 = "INSERT INTO currentsess(reg_id,sess,gen_id) VALUES('$regid','$secagt','$usid')";
                    $re5 = ExecuteSQLQuery($qry5);
                    if ($re5) {
                        echo "successtwo";
                    } else {
                        echo "Program was enable to login";
                    }
                }
            } else {
                echo "Error in starting session";
            }
        } else {
            echo "Invalid username or password";
        }
    }
} else if ($opCode == "SUBACCT") {
    if (isset($_SESSION['SMShitgenid'])) {
        $user = $_SESSION['SMShitgenid'];
    } else {
        header("Location: " . "../index.php?mess=Your session is over");
        exit();
    }
    $subname = strtoupper(filter($_POST['subname']));
    $pass = filter($_POST['pass']);
    $snd_id = filter($_POST['snd_id']);
    $prefix = filter($_POST['prefix']);
    $sufix = filter($_POST['sufix']);
    $sntem = filter($_POST['sntem']);
    $failem = filter($_POST['failem']);
    $callurl = filter($_POST['callurl']);
    $active = filter($_POST['active']);
    if ($active == "Checked") {
        $active = 1;
    } else {
        $active = 0;
    }
    $po = GenApi();

    $sql4 = "SELECT * FROM subacc Where Accname='$subname'";
    $res4 = ExecuteSQLQuery($sql4);
    if ($res4) {
        $num4 = mysqli_num_rows($res4);
        if ($num4 < 1) {
            $sql5 = "INSERT INTO subacc(user_id,Accname,password,sender_id,prefix,suffix,sentEmail,failEmail,callUrl,active,apiKey)
			VALUES('$user','$subname','$pass','$snd_id','$prefix','$sufix','$sntem','$failem','$callurl','$active','$po')";
            $res5 = ExecuteSQLQuery($sql5);
            if ($res5) {
                echo $po;
            } else {
                echo "An error occured while generating api key";
            }
        } else {
            echo "Account name exist";
        }
    } else {
        echo "An error occured just now!";
    }
} else if ($opCode == "CHANGEPASS") {
    $oldpass = filter($_POST['oldpass']);
    $pass = filter($_POST['pass']);
    $userid = filter($_POST['userid']);
    $email = "";
    $np = "";
    $sid = "";
    $sql9 = "SELECT * FROM users WHERE password='$oldpass' AND id='$userid'";
    $res9 = ExecuteSQLQuery($sql9);
    if ($res9) {
        $num9 = mysqli_num_rows($res9);
        if ($num9 == 1) {
            $sqlup = "UPDATE users SET password='$pass' WHERE id='$userid'";
            $resup = ExecuteSQLQuery($sqlup);
            if ($resup) {

                $fet9 = mysqli_fetch_assoc($res9);
                $email = $fet9['email'];

                $request = "Change of Password Successful, <br> Your new password is: $pass  <br> Thanks <br> SMSHit Team";


                $subject = "CHANGE OF PASSWORD SMSHIT";
                $from = 'SMSHit <info@smshit.net>';
                $headers = 'From: ' . $from . PHP_EOL
                        . 'Reply-To: ' . $from . PHP_EOL
                        . 'X-Mailer: PHP/' . phpversion();
//                $sucess = mail($email, $subject, $request, $headers);
                $sucess = sendEmail($subject, $request, $email);
                if ($sucess) {

                    echo '<p>Password change successfully. Please Check your mail</p>';
                } else {
                    echo "Error sending mail to user";
                }
            } else {
                echo "Please retry! An error encountered";
            }
        } else {
            echo "Incorrect oldpassword";
        }
    } else {
        echo "An error just occured during process.";
    }
} else if ($opCode == "ADMINUNIT") {
    $unit = filter($_POST['unit']);
    $userid = filter($_POST['userid']);
    $adcr = $_SESSION['SMShitAdmincredit'];
    $aid = $_SESSION['SMShitAdmingenid'];
    if ($unit > $adcr) {
        echo "Sorry you have limited credit to transfer";
        exit();
    }
    $newcrd = $adcr - $unit;
    $sql51 = "UPDATE admin SET credit='$newcrd' WHERE id='$aid'";
    $res51 = ExecuteSQLQuery($sql51);
    $_SESSION['SMShitAdmincredit'] = $newcrd;

    $sql54 = "SELECT * FROM admin WHERE id='$userid'";
    $res54 = ExecuteSQLQuery($sql54);
    if ($res54) {
        $fet54 = mysqli_fetch_assoc($res54);
        $Uscrd = $fet54['credit'];

        $newCrd2 = $Uscrd + $unit;
    }
    $sql13 = "UPDATE admin SET credit='$newCrd2' WHERE id='$userid'";
    $res13 = ExecuteSQLQuery($sql13);
    if ($res13) {
        $sql55 = "INSERT INTO unitlog(sender,reciever,unit,operation,datecreated) VALUES('$aid','$userid','$unit','Transfer',NOW())";
        $res55 = ExecuteSQLQuery($sql55);
        echo "Unit added";
    } else {
        echo "Error in processing reqest";
    }
} else if ($opCode == "USERUNIT") {
    $unit = filter($_POST['unit']);
    $userid = filter($_POST['userid']);
    $sql14 = "SELECT * FROM admin WHERE id = '$userid'";
    $res14 = ExecuteSQLQuery($sql14);
    if ($res14) {
        $num14 = mysqli_num_rows($res14);
        if ($num14 == 1) {
            $fet14 = mysqli_fetch_assoc($res14);
            $email = $fet14['email'];
            $tunit = $unit + $_SESSION['SMShitAdmincredit'];
            $_SESSION['SMShitAdmincredit'] = $tunit;
            $sql13 = "UPDATE admin SET credit='$tunit' WHERE id='$userid'";
            $res13 = ExecuteSQLQuery($sql13);
            if ($res13) {
                $sql55 = "INSERT INTO unitlog(sender,reciever,unit,operation,datecreated) VALUES('$userid','$userid','$unit','Transfer',NOW())";
                $res55 = ExecuteSQLQuery($sql55);
                $request = "Unit Added Successful, <br> Unit added is: $unit <br> Thanks <br> SMSHit Team";


                $subject = "UNIT ADDED SMSHIT";
                $from = 'SMSHit <info@smshit.net>';
                $headers = 'From: ' . $from . PHP_EOL
                        . 'Reply-To: ' . $from . PHP_EOL
                        . 'X-Mailer: PHP/' . phpversion();
//                $sucess = mail($email, $subject, $request, $headers);
                $sucess = sendEmail($subject, $request, $email);
                
                if ($sucess) {

                    echo "Unit added Successfully";
                } else {
                    echo "Error sending mail to user";
                }
            } else {
                echo "Error in processing reqest";
            }
        } else {
            echo "Username does not exit";
        }
    } else {
        echo "Error in processing request";
    }
} else if ($opCode == "CREATEUSER") {
    $unit = filter($_POST['unit']);
    $username = filter($_POST['username']);
    $pass = filter($_POST['password']);
    $role = filter($_POST['rol']);
    $email = filter($_POST['email']);
    $sql14 = "SELECT * FROM admin WHERE username = '$username'";
    $res14 = ExecuteSQLQuery($sql14);
    if ($res14) {
        $num14 = mysqli_num_rows($res14);
        if ($num14 == 1) {
            echo "Username is in use";
        } else {
            $sql13 = "INSERT INTO admin(username,password,credit,email,role,datecreated) VALUES('$username','$pass','$unit','$email','$role',NOW())";
            $res13 = ExecuteSQLQuery($sql13);
            if ($res13) {
                $request = "New Admin User Created Successful, <br> Your account detail are: <br> Username is: $username  <br> Your Password is: $pass  <br> Thanks <br> SMSHit Team";


                $subject = "USER CREATION SMSHIT";
                $from = 'SMSHit <info@smshit.net>';
                $headers = 'From: ' . $from . PHP_EOL
                        . 'Reply-To: ' . $from . PHP_EOL
                        . 'X-Mailer: PHP/' . phpversion();
//                $sucess = mail($email, $subject, $request, $headers);
                $sucess = sendEmail($subject, $request, $email);
                if ($sucess) {

                    echo "User created Successfully";
                } else {
                    echo "Error sending mail to user";
                }
            } else {
                echo "Error in processing reqest";
            }
        }
    } else {
        echo "Error in processing request";
    }
} else if ($opCode == "ADMINCHANGEPASS") {
    $oldpass = filter($_POST['oldpass']);
    $pass = filter($_POST['pass']);
    $userid = filter($_POST['userid']);

    $sql9 = "SELECT * FROM admin WHERE password='$oldpass' AND id='$userid'";
    $res9 = ExecuteSQLQuery($sql9);
    if ($res9) {
        $num9 = mysqli_num_rows($res9);
        if ($num9 == 1) {
            $fet9 = mysqli_fetch_assoc($res9);
            $email = $fet9['email'];
            $sqlup = "UPDATE admin SET password='$pass' WHERE id='$userid'";
            $resup = ExecuteSQLQuery($sqlup);
            if ($resup) {

                $request = "Password change detail, <br> Your New Password is: $pass  <br> Thanks <br> SMSHit Team";


                $subject = "CHANGE OF PASSWORD SMSHIT";
                $from = 'SMSHit <info@smshit.net>';
                $headers = 'From: ' . $from . PHP_EOL
                        . 'Reply-To: ' . $from . PHP_EOL
                        . 'X-Mailer: PHP/' . phpversion();
//                $sucess = mail($email, $subject, $request, $headers);
                $sucess = sendEmail($subject, $request, $email);
                if ($sucess) {

                    echo "Password changed successfully";
                } else {
                    echo "Error sending mail to user";
                }
            } else {
                echo "Please retry! An error encountered";
            }
        } else {
            echo "Incorrect oldpassword";
        }
    } else {
        echo "An error just occured during process.";
    }
} else if ($opCode == "REGISTER") {
    $fir = ucfirst(filter($_POST['fir']));
    $sur = ucfirst(filter($_POST['sur']));
    $em = filter($_POST['em']);
    $fon = filter($_POST['fon']);
    $day = filter($_POST['day']);
    $month = filter($_POST['month']);
    $coun = filter($_POST['country']);
    $seller = filter($_POST['reseller']);
    if (!empty($seller)) {
        $AddCrd = 10;
    } else {
        $AddCrd = "";
    }
    $pass = GenKey();
    $activation = substr(md5(microtime()), 0, 10);

    if (!filter_var($em, FILTER_VALIDATE_EMAIL)) {
        echo "This is not a valid email address";
        exit();
    }
    /*
      if (!empty($file))
      {
      if (($_FILES["fileField"]["type"] == "image/jpg") || ($_FILES["fileField"]["type"] == "image/jpeg")
      || ($_FILES["fileField"]["type"] == "image/png") && ($_FILES["fileField"]["size"] < 30000))
      {
      if ($_FILES["fileField"]["error"] > 0)
      {
      echo $rep = "Tmess=Error in uploading file";
      exit();
      }
      else
      {

      if (file_exists("../upload/" . $_FILES["fileField"]["name"]))
      {
      echo $rep = "This file exist in our database! try again.";
      exit();
      }
      else
      {
      $url = "../upload/".rand(11111111,99999999).".jpg";
      }//end of file exist
      }
      }//end of file size
      else
      {
      echo $rep = "Please upload a jpeg,jpg only or image size is big";
      exit();
      }


      }//end of empty image
      else
      {
      $url = "";
      }

     */

    $sql2 = "Select * From users Where email='$em'";
    $res2 = ExecuteSQLQuery($sql2);
    if ($res2) {
        $num2 = mysqli_num_rows($res2);
        if ($num2 == 1) {
            echo "This email is in use";
            exit();
        } else {

            $sql3 = "INSERT INTO users(firstname,surname,email,phone,day,month,password,country,actkey,credit,resellercode,datecreated) 
	VALUES('$fir','$sur','$em','$fon','$day','$month','$pass','$coun','$activation','$AddCrd','',NOW())";

            if ($seller != "") {
                $sql65 = "SELECT * FROM upgrade WHERE code='$seller'";
                $res65 = ExecuteSQLQuery($sql65);
                if ($res65) {
                    $num65 = mysqli_num_rows($res65);
                    if ($num65 == 1) {
                        $fet65 = mysqli_fetch_assoc($res65);
                        $udid = $fet65['user_id'];
                        $sql67 = "SELECT id From users WHERE email='$em'";
                        $res67 = ExecuteSQLQuery($sql67);
                        if ($res67) {
                            $fet67 = mysqli_fetch_assoc($res67);
                            $usddid = $fet67['id'];
                            $sql66 = "INSERT INTO resellerusers(resellerid,user_id,datecreated) VALUES('$udid','$usddid',NOW())";
                            $res66 = ExecuteSQLQuery($sql66);
                            if ($res66) {
                                $res3 = ExecuteSQLQuery($sql3);
                                if ($res3) {





//                                    $request = "Welcome $fir $sur, <br> Thank you for signing up, <br> Your account details are as follows <br> Username : $em  <br> Password: $pass <br>   <br> Thanks <br> SMSHit Team";
                                    $request = "Welcome $fir $sur, <br> Thank you for signing up,<br> Your account details are as follows <br> Username : $em  <br> Password: $pass <br><br> Thanks <br> SMSHit Team";


                                    $subject = "Welcome to SMS";
                                    $from = 'SMSHit <info@smshit.net>';
                                    $headers = 'From: ' . $from . PHP_EOL
                                            . 'Reply-To: ' . $from . PHP_EOL
                                            . 'X-Mailer: PHP/' . phpversion();


                                    $success = sendEmail($subject, $request, $em);

                                    if ($success) {
                                        echo 'Thank you for choosing SMSHit.<p>Login info successfully sent to ' . $em . '</p>';
                                    } else {
                                        echo '<p>Could not deliver an email to your emailID...</p>';
                                    }
                                }
                            }//end of res66
                        }//end of res67
                    }//end of num65
                    else {
                        echo "Sorry reseller code is incorrect";
                    }
                }//end of res65
            }//end of empty seller
            else {

                $res3 = ExecuteSQLQuery($sql3);
                if ($res3) {


                    $request = "Welcome $fir $sur, <br> Thank you for signing up,<br> Your account details are as follows <br> Username : $em  <br> Password: $pass <br><br> Thanks <br> SMSHit Team";


                    $subject = "Welcome to SMS";
                    $from = 'SMSHit <info@smshit.net>';
                    $headers = 'From: ' . $from . PHP_EOL
                            . 'Reply-To: ' . $from . PHP_EOL
                            . 'X-Mailer: PHP/' . phpversion();

                    $success = sendEmail($subject, $request, $em);
                    // $sucess = mail($em, $subject, $request, $headers);
                    if ($success) {
                        echo 'Thank you for choosing SMSHit.<p>Login info successfully sent to ' . $em . '</p>';
                    } else {
                        echo '<p>Could not deliver an email to your emailID...</p>';
                    }
                }
            }
        }
    } else {
        echo "Data could not be verify";
    }
} else if ($opCode == "REGISTER2") {
    $fir = ucfirst(filter($_POST['fir']));
    $sur = ucfirst(filter($_POST['sur']));
    $em = filter($_POST['em']);
    $fon = filter($_POST['fon']);
    $day = filter($_POST['day']);
    $month = filter($_POST['month']);
    $coun = filter($_POST['country']);
    $seller = filter($_POST['reseller']);
    $usid = filter($_POST['userid']);
    $pass = sha1(GenKey());
    $activation = substr(md5(microtime()), 0, 5);

    if (!filter_var($em, FILTER_VALIDATE_EMAIL)) {
        echo "This is not a valid email address";
        exit();
    }

    $sql2 = "Select * From users Where email='$em'";
    $res2 = ExecuteSQLQuery($sql2);
    if ($res2) {
        $num2 = mysqli_num_rows($res2);
        if ($num2 == 1) {
            echo "The email is in use! You currently exist as a user, login.";
            exit();
        } else {

            $sql3 = "INSERT INTO users(firstname,surname,email,phone,day,month,password,country,actkey,credit,resellercode) 
	VALUES('$fir','$sur','$em','$fon','$day','$month','$pass','$coun','$activation','10','$seller')";
            $res3 = ExecuteSQLQuery($sql3);
            if ($res3) {
                $sql67 = "SELECT id From users WHERE email='$em'";
                $res67 = ExecuteSQLQuery($sql67);
                if ($res67) {
                    $fet67 = mysqli_fetch_assoc($res67);
                    $usddid = $fet67['id'];
                    $sql66 = "INSERT INTO resellerusers(resellerid,user_id,datecreated) VALUES('$usid','$usddid',NOW())";
                    $res66 = ExecuteSQLQuery($sql66);
                    if ($res66) {
                        $res3 = ExecuteSQLQuery($sql3);
                        if ($res3) {


                            $request = "Welcome $fir $sur, <br> Thank you for signing up, <br> Your account details are as follows <br> Username : $em  <br> Password: $pass <br>   <br> Thanks <br> SMSHit Team";


                            $subject = "Welcome to SMS";
                            $from = 'SMSHit <info@smshit.net>';
                            $headers = 'From: ' . $from . PHP_EOL
                                    . 'Reply-To: ' . $from . PHP_EOL
                                    . 'X-Mailer: PHP/' . phpversion();


                            //$sucess = mail($em, $subject, $request, $headers);
                            $success = sendEmail($subject, $request, $em);
                            if ($success) {
                                echo 'Thank you for choosing SMSHit.<p>Login info successfully sent to </p>' . $em;
                            } else {
                                echo '<p>Could not deliver an email to your emailID...</p>';
                            }
                        }
                    }//end of res66
                }//end of res67
            } else {
                echo "Invalid input ";
            }
        }
    } else {
        echo "Data could not be verify";
    }
} else if ($opCode == "UPLOADFILL") {
    if (isset($_SESSION['SMShitgenid'])) {
        $user = $_SESSION['SMShitgenid'];
    } else {
        if (isset($_SESSION['SMShitAdmingenid'])) {
            $user = $_SESSION['SMShitAdmingenid'];
        } else {
            header("Location: " . "../index.php?mess=Your session is over");
            exit();
        }
    }
    $filename = filter($_POST['filename']);
    if (($_FILES["fileuplEx"]["size"] < 30000)) {
        if ($_FILES["fileuplEx"]["error"] > 0) {
            echo $rep = "Tmess=Error in uploading file";
            exit();
        } else {

            if (file_exists("../tmp/" . $_FILES["fileuplEx"]["name"])) {
                echo $rep = "This file exist in our database! try again.";
                exit();
            } else {
                $url = "../tmp/" . rand(11111111, 99999999) . ".csv";
            }//end of file exist
        }
    }//end of file size
    else {
        echo $rep = "Please upload a csv only";
        exit();
    }


    copy($_FILES['fileuplEx']['tmp_name'], $url);

    $File = $_FILES['fileuplEx']['tmp_name'];

    // get number of rows
    $lines = file($url);
    $num_lines = count($lines);

    // get number of fields
    $delimiter = ",";
    $handle = fopen($url, "r");
    $data = fgetcsv($handle, 1000);
    $data = array_unique($data);
    $columns = count($data);

    $message = "";

    if ($lines > 0 && $columns == 1) {

        rewind($handle);
        //begin looping through the lines
        while (($data = fgetcsv($handle, 1000)) !== FALSE && !feof($handle)) {
            $co = 0;
            $val .= "," . $data[$co];

            if (is_numeric($data[$co])) {
                if ($co == 0) {
                    $sql6 = "SELECT * FROM phonebookupl Where user_id='$user' AND filename='$filename'";
                    $res6 = ExecuteSQLQuery($sql6);
                    if ($res6) {
                        $num6 = mysqli_num_rows($res6);
                        if ($num6 == 1) {
                            $rep = "Upload successfully";
                        } else {
                            $sql7 = "INSERT INTO phonebookupl(user_id,filelink,filename,datecreated) VALUES('$user','$url','$filename',NOW())";
                            $res7 = ExecuteSQLQuery($sql7);
                            if ($res7) {
                                $rep = ($num_lines - 1) . " Upload successfully! ";
                            } else {
                                $rep = "Could not update phonebook";
                            }
                        }
                    } else {
                        
                    }
                }
            } else {
                $rep .='Error';
            }
            $co += 1;
        }
        $data = null;
        fclose($handle);
    } else {
        $rep = 'Bad CSV format file.';
    }

    echo $rep;
} else if ($opCode == "ADDBK") {
    if (isset($_SESSION['SMShitgenid'])) {
        $user = $_SESSION['SMShitgenid'];
    } else {
        if (isset($_SESSION['SMShitAdmingenid'])) {
            $user = $_SESSION['SMShitAdmingenid'];
        } else {

            header("Location: " . "../index.php?mess=Your session is over");
            exit();
        }
    }

    $name = filter($_POST['name']);
    $numb = filter($_POST['mnumber']);
    $addr = filter($_POST['caddress']);

    /* $chk="select * from phonebook where mnumber like '%$numb%' and user_id='$user'";
      $chkres = ExecuteSQLQuery($chk);
      if($chkres)
      {
      $numchk = mysqli_num_rows($chkres);
      if($numchk == 1)
      {
      echo "There is a duplicate number in this entry";
      }
      else
      { */
    $sql8 = "INSERT INTO phonebook(name,mnumber,caddress,user_id) VALUES('$name','$numb','$addr','$user')";
    $res8 = ExecuteSQLQuery($sql8);
    if ($res8) {
        echo "Entry successfully added";
    } else {
        echo "Error";
    }
    /* }	
      } */
} else if ($opCode == "ADDBK2") {
    if (isset($_SESSION['SMShitgenid'])) {
        $user = $_SESSION['SMShitgenid'];
    } else {
        if (isset($_SESSION['SMShitAdmingenid'])) {
            $user = $_SESSION['SMShitAdmingenid'];
        } else {

            header("Location: " . "../index.php?mess=Your session is over");
            exit();
        }
    }

    $name = filter($_POST['name']);
    $numb = filter($_POST['mnumber']);
    $addr = filter($_POST['caddress']);

    /* $chk="select * from phonebook where mnumber like '%$numb%' and user_id='$user'";
      $chkres = ExecuteSQLQuery($chk);
      if($chkres)
      {
      $numchk = mysqli_num_rows($chkres);
      if($numchk == 1)
      {
      echo "There is a duplicate number in this entry";
      }
      else
      { */
    $sql8 = "INSERT INTO phonebook(name,mnumber,caddress,user_id) VALUES('$name','$numb','$addr','$user')";
    $res8 = ExecuteSQLQuery($sql8);
    if ($res8) {
        echo "Entry successfully added";
    } else {
        echo "Error";
    }
    /* }	
      } */
} else if ($opCode == "SENDNOW") {
    
} elseif ($opCode == "Voucher") {
    if (isset($_SESSION['SMShitgenid'])) {
        $user = $_SESSION['SMShitgenid'];
    } else {
        echo "Your session is over";
        exit();
    }
    $v_num = $_POST[voucher_number];

    $sqlq = "select * from profile where id='$user' ";
    $viewq = mysql_query($sqlq);
    while ($row = mysql_fetch_assoc($viewq)) {
        $mid = $row['0'];
        $credit = $row['credit'];
    }
    $sqld = "select * from vouchers WHERE voucher_value = '$v_num'";
    $chk = mysql_query($sqld);
    while ($row = mysql_fetch_array($chk)) {
        $voucher_num = $row['voucher_value'];
        $sms_value = $row['sms_value'];
        $ncredit = $credit + $sms_value;
    }

    if (!$sms_value) {
        echo"<div id=errmes> The voucher number you entered is Invalid or has already been used</div>";
    } else {
        mysql_query("update profile set credit='$ncredit' where id='$valid_id'");
        mysql_query("delete from vouchers  where voucher_value='$v_num'");
        echo " your account has been credited with $sms_value sms <br>";
        echo "your new balance is : $ncredit sms";
    }
} elseif ($opCode == "GETQUAN") {
    
} else {
    echo 'Error8';
}

function GenKey($length = 5) {
    $password = "";
    $possible = "0123456789abcdefghijkmnopqrstuvwxyz";

    $i = 0;

    while ($i < $length) {


        $char = substr($possible, mt_rand(0, strlen($possible) - 1), 1);


        if (!strstr($password, $char)) {
            $password .= $char;
            $i++;
        }
    }

    return $password;
}

function EncodeURL($url) {
    $new = strtolower(ereg_replace(' ', '_', $url));
    return($new);
}

function isEmail($email) {
    return preg_match('/^\S+@[\w\d.-]{2,}\.[\w]{2,6}$/iU', $email) ? TRUE : FALSE;
}

function isUserID($username) {
    if (preg_match('/^[a-z\d_]{5,20}$/i', $username)) {
        return true;
    } else {
        return false;
    }
}

function isURL($url) {
    if (preg_match('/^(http|https|ftp):\/\/([A-Z0-9][A-Z0-9_-]*(?:\.[A-Z0-9][A-Z0-9_-]*)+):?(\d+)?\/?/i', $url)) {
        return true;
    } else {
        return false;
    }
}

function mailingserver($email, $subj, $req, $header) {


    /*
      DONT FORGET TO DELETE THIS SCRIPT WHEN FINISHED!
     */

    $send = true;

    $from = $header;

    /*
      ini_set( 'SMTP', 'smtp.smshit.net' );
      ini_set( 'SMTP_PORT', 25 );
      ini_set( 'sendmail_from', $from );
     */

    $server = array(
        'HTTP_HOST', 'SERVER_NAME', 'SERVER_ADDR', 'SERVER_PORT',
        'SERVER_ADMIN', 'SERVER_SIGNATURE', 'SERVER_SOFTWARE',
        'REMOTE_ADDR', 'DOCUMENT_ROOT', 'REQUEST_URI',
        'SCRIPT_NAME', 'SCRIPT_FILENAME',
    );

    $to = ( isset($email) ? $email : FALSE );
    $subject = $subj;
    $message = $req;

    if (!$to) {
        $ret = '<strong>Set $email</strong>';
        exit;
    }

    foreach ($server as $s) {
        $message .= sprintf('%s: %s', $s, $_SERVER[$s]) . PHP_EOL;
    }

    $headers = 'From: ' . $from . PHP_EOL
            . 'Reply-To: ' . $from . PHP_EOL
            . 'X-Mailer: PHP/' . phpversion();

    if (isset($send) && $send === 'true') {
        //$success = mail($to, $subject, $message, $from);
        $success = sendEmail($subject, $message, $to);
    } else {
        $ret = 'An error occur forwarding an email to $email';
    }

    if (isset($success)) {
        $ret1 = 'E-mail forwarded to: ' . $to;
    } else {
        $ret = $ret . '<br />';
        $ret = $ret . 'E-mail set as: ' . $to;
    }



    if (isset($success)) {
        return $ret1;
    }
}

function checkPwd($x, $y) {
    if (empty($x) || empty($y)) {
        return false;
    }
    if (strlen($x) < 4 || strlen($y) < 4) {
        return false;
    }

    if (strcmp($x, $y) != 0) {
        return false;
    }
    return true;
}

function GenApi($length = 5) {
    $key = "";
    $key = "0123456789bcdfghjkmnpqrstvwxyz"; //no vowels

    $i = 0;
    $key1 = substr(md5(microtime()), 0, 8);
    while ($i < $length) {

        $key1 = $key1 . "-" . substr(md5(microtime()), 0, 8);

        $i++;
    }

    return $key1;
}

function DecodeURL($url) {
    $new = ucwords(ereg_replace('_', ' ', $url));
    return($new);
}

function filter($data) {
    $db = opendatabase();
    $data = trim(htmlentities(strip_tags($data)));

    if (get_magic_quotes_gpc())
        $data = stripslashes($data);

    $data = mysqli_real_escape_string($db, $data);

    return $data;
    mysqli_close($db);
}

function splitfon($number) {
    $order = array("\r\n", "\n", "\r", ".", "-", "/", "'");
    $pnumber = trim($number);
    $pnumber = str_replace($order, ",", $pnumber);
    $cose = explode(",", $pnumber);

    return $cose;
}
?>