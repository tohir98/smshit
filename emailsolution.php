<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="style/prettyPhoto.css" type="text/css">
<link href="style/SMSHIT.css" rel="stylesheet" type="text/css">
<link type="text/css" href="style/jquery.ui.all.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="style/jDev.css">

 <script src="js/jquery-1.7.1.js" type="text/javascript"></script>
<script type="text/javascript" src="script/portalScript.js"></script>
<script type="text/javascript" src="script/mobile.js"></script>

    <script src="js/cufon-yui.js" type="text/javascript"></script>
    <script src="js/cufon-replace.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/Josefin_Sans_600.font.js"></script>
    <script type="text/javascript" src="js/Lobster_400.font.js"></script>
    <script type="text/javascript" src="js/sprites.js"></script>
    <script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
    <script type="text/javascript" src="js/jquery.jplayer.settings.js"></script>
    <script type="text/javascript" src="js/gSlider.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
   <script type="text/javascript" src="js/jquery.blueberry.js"></script>
   

<title>SMShit Email Marketing</title>

</head>

<body>

<?php require("header.php"); ?>

<?php require("flashplayer.php") ?>
<div class="container hideover">
	<div class="" style="font-size:20px; color:#008ACC">Email Marketing</div>
    <div id="bodycontainer" class="hideover">
    	<div class="divcenter">
      <p>Easily set up an  email marketing campaign in minutes by<strong> c</strong>hoosing from a wide selection of professional  email templates and accessing our database of e-mails of people segmented by  gender, location and age to convey your messages.  <br />
        <strong>Advanced scheduling options give you flexible  message delivery by:</strong></p>
      <ul>
        <li>Scheduling  your blast to go out immediately or on a specific date and time.</li>
        <li>Automatically  sending email reminders to your subscribers on specific dates or time periods,  like every day, week, month, or year.</li>
        <li>Building  customer relationships by sending courtesy emails at specific intervals.</li>
      </ul>
      <p><strong>Optimize your campaign for maximum  deliverability and success. You can do the following.</strong></p>
      <ul>
        <li>Send  your email marketing campaign with high deliverability.</li>
        <li>Spam  filters check content to reduce chance of your emails ending up in junk  folders.</li>
        <li>Detect  invalid email addresses to remove them automatically from your contact database.</li>
      </ul>
      <p><strong>Track email campaign statistics in real-time  by:</strong></p>
      <ul>
        <li>Gauging  audience engagement by tracking click-through rates. Discover what works and  what most interests your readers by seeing how many visitors clicked on each  link.</li>
        <li>Reporting  who opened and clicked your email.</li>
        <li>Monitoring  bounce rates to clean up your email distribution lists of unreachable contacts  to improve deliverability.</li>
      </ul>
        </div>
        
    </div>
</div>


<div id="longdiv" class="hideover">
	<div class="container hideover">
    	<div class="divsocial">
        <ul>
        <li class="textin" style="width:150px; padding:15px 0px 0px 0px; font-size:12pt">Connect to us on:</li>
        <li><img src="img/facebook.png" alt="facebook" /></li>
        <li><img src="img/twitter.png" alt="twitter" /></li>
        <li><img src="img/linkedin.png" alt="linkedin" /></li>
        </ul>
        </div>
    	<div class="divsubscribe">
        <div id="errorMsg"></div>
        <form id="form1" name="form1" method="post" action="">
          <label for="textfield"></label>
          <input type="text" name="subs" id="subs" class="inputsearch" value="Subscribe to our newsletter" />
          <input type="button" name="news" id="news" value="Submit" class="divshit pop pstbutton" style="cursor:pointer" />
        </form>
        </div>
    </div>
</div>

<div id="footer">
  <?php require("footer.php") ?>
</div>
<script type="text/javascript">Cufon.now()
$(function(){
$('nav,.more,.header-more').sprites()

$('.header-slider').gSlider({
prevBu:'.hs-prev',
nextBu:'.hs-next'
})
})
$(window).load(function(){
$('.tumbvr')._fw({tumbvr:{
duration:2000,
easing:'easeOutQuart'
}})
.bind('click',function(){
location="index-3.html"
})

$('a[rel=prettyPhoto]').each(function(){
var th=$(this),
pb
th
.append(pb=$('<span class="playbutt"></span>').css({opacity:.7}))
pb
.bind('mouseenter',function(){
$(this)
.stop()
.animate({opacity:.9})
})
.bind('mouseleave',function(){
$(this)
.stop()
.animate({opacity:.7})
})
})
.prettyPhoto({theme:'dark_square'})
})
$(window).load(function() {
	$('.blueberry').blueberry();
});
</script>
</body>
</html>