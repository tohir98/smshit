<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="style/prettyPhoto.css" type="text/css">
<link href="style/SMSHIT.css" rel="stylesheet" type="text/css">
<link type="text/css" href="style/jquery.ui.all.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="style/jDev.css">

 <script src="js/jquery-1.7.1.js" type="text/javascript"></script>
<script type="text/javascript" src="script/portalScript.js"></script>
<script type="text/javascript" src="script/mobile.js"></script>

    <script src="js/cufon-yui.js" type="text/javascript"></script>
    <script src="js/cufon-replace.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/Josefin_Sans_600.font.js"></script>
    <script type="text/javascript" src="js/Lobster_400.font.js"></script>
    <script type="text/javascript" src="js/sprites.js"></script>
    <script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
    <script type="text/javascript" src="js/jquery.jplayer.settings.js"></script>
    <script type="text/javascript" src="js/gSlider.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
   <script type="text/javascript" src="js/jquery.blueberry.js"></script>
   

<title>SMShit SMS Marketing</title>

</head>

<body>

<?php require("header.php"); ?>

<?php require("flashplayer.php") ?>
<div class="container hideover">
	<div class="" style="font-size:20px; color:#008ACC">Social Marketing</div>
    <div id="bodycontainer" class="hideover">
    	<div class="divcenter">
      <p>Social media  has integrated itself into the busy, everyday lives of consumers. Businesses  now have a unique voice with their audiences and kick-start the strongest form  of marketing: word-of-mouth. <br />
With an  audience of millions, you can build your online presence and strengthen brand  credibility. Exposure is everything. Having great content is important, but  landing on your audience's newsfeed differentiates your business from everyone  else. We give you the tools to get the exposure you deserve, driving audiences  to Like, post, @mention and share your business with their own networks.</p>
      <p><strong>What Social Media Marketing Can Do  For You:</strong><br />
        -Connect  with customers. Social media gives your business a unique face and voice to  communicate and build lasting relationships.<br />
        -Spark  word-of-mouth marketing. Social media creates referrals, which are the most  proven, effective, and trusted form of getting business.<br />
        -Boost your  online presence. Stand out against the noisy online crowd and spotlight  upcoming promotions, announcements, and events.<br />
        -Expand your contact database. Collect contact  information for future marketing campaigns.<br />
        <br />
        Run  effective social campaigns with the right tools:<br />
        </p>
      </div>
        
    </div>
</div>


<div id="longdiv" class="hideover">
	<div class="container hideover">
    	<div class="divsocial">
        <ul>
        <li class="textin" style="width:150px; padding:15px 0px 0px 0px; font-size:12pt">Connect to us on:</li>
        <li><img src="img/facebook.png" alt="facebook" /></li>
        <li><img src="img/twitter.png" alt="twitter" /></li>
        <li><img src="img/linkedin.png" alt="linkedin" /></li>
        </ul>
        </div>
    	<div class="divsubscribe">
        <div id="errorMsg"></div>
        <form id="form1" name="form1" method="post" action="">
          <label for="textfield"></label>
          <input type="text" name="subs" id="subs" class="inputsearch" value="Subscribe to our newsletter" />
          <input type="button" name="news" id="news" value="Submit" class="divshit pop pstbutton" style="cursor:pointer" />
        </form>
        </div>
    </div>
</div>

<div id="footer">
  <?php require("footer.php") ?>
</div>
<script type="text/javascript">Cufon.now()
$(function(){
$('nav,.more,.header-more').sprites()

$('.header-slider').gSlider({
prevBu:'.hs-prev',
nextBu:'.hs-next'
})
})
$(window).load(function(){
$('.tumbvr')._fw({tumbvr:{
duration:2000,
easing:'easeOutQuart'
}})
.bind('click',function(){
location="index-3.html"
})

$('a[rel=prettyPhoto]').each(function(){
var th=$(this),
pb
th
.append(pb=$('<span class="playbutt"></span>').css({opacity:.7}))
pb
.bind('mouseenter',function(){
$(this)
.stop()
.animate({opacity:.9})
})
.bind('mouseleave',function(){
$(this)
.stop()
.animate({opacity:.7})
})
})
.prettyPhoto({theme:'dark_square'})
})
$(window).load(function() {
	$('.blueberry').blueberry();
});
</script>
</body>
</html>