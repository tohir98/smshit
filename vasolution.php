<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="style/prettyPhoto.css" type="text/css">
<link href="style/SMSHIT.css" rel="stylesheet" type="text/css">
<link type="text/css" href="style/jquery.ui.all.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="style/jDev.css">

 <script src="js/jquery-1.7.1.js" type="text/javascript"></script>
<script type="text/javascript" src="script/portalScript.js"></script>
<script type="text/javascript" src="script/mobile.js"></script>

    <script src="js/cufon-yui.js" type="text/javascript"></script>
    <script src="js/cufon-replace.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/Josefin_Sans_600.font.js"></script>
    <script type="text/javascript" src="js/Lobster_400.font.js"></script>
    <script type="text/javascript" src="js/sprites.js"></script>
    <script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
    <script type="text/javascript" src="js/jquery.jplayer.settings.js"></script>
    <script type="text/javascript" src="js/gSlider.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
   <script type="text/javascript" src="js/jquery.blueberry.js"></script>
   

<title>SMShit Vas Solution</title>

</head>

<body>

<?php require("header.php"); ?>

<?php require("flashplayer.php") ?>
<div class="container hideover">
	<div class="" style="font-size:20px; color:#008ACC">VAS Solution</div>
    <div id="bodycontainer" class="hideover">
    	<div class="divcenter">
      <p>We provide value added services  for the Telcos by developing wireless applications for messaging, calls, ring  tones, mobile games etc</p>
      <p><strong>Text-to-Vote/SMS  Polls</strong></p>
      <p>SMS Polls and text to vote tools  are the fastest and easiest way to know what your customers want or how they  feel. With our platform, you can create a poll within minutes and gain instant  and invaluable feedback, helping them feel more connected with your brand.</p>
      <p>Also voting could be done during  contest by texting different keywords to a code to select the winners in the  contest.</p>
      <p><strong>&nbsp;</strong>      </p>
      <p><strong>Two  way messaging</strong></p>
      <p>Offering instant deliverability and  high response rates, text marketing delivers your message with high impact - be  it advertisements, alerts, or information - literally into the hands of your  audience whenever you want.</p>
      <p>Good business is a two-way  street, so get a conversation going with two-way texting. With this open line  of communication, you can hear back from your customers instantly wherever they  are at their convenience. When you respond to their comments, feedback, or  questions, in turn they feel heard and you gain valuable information. </p>
      <p><strong>Maximize  the impact of your marketing through:</strong></p>
      <p>-Instantly collect valuable  customer opinions and data.</p>
      <p>-Invite audience interaction with  one user or a group of people.</p>
      <p>&nbsp;</p>
      <p><strong>Mobile  Keywords and Short Codes</strong></p>
      <p>Mobile Keywords are unique words  that you create for your customers to text to join your SMS marketing program.  They offer a quick and effective way to build your contact list, and allow your  customers to opt-in to receive coupons, special event invites, or any other  form of promotion for your business.</p>
      <p>For example, a movie producer  could create the keyword WATCH for a large number of people and to opt in for a  movie show by texting WATCH to a short code say 121. Subsequently this database  of  responded people could be followed up  with other messages.</p>
      <p>Short codes can either be  dedicated or shared. Our platform gives you instant, up-to-date analytics on  how many opt-in you receive from your keywords, letting you track progress as it  happens.</p>
      <p>SMS short codes are unique and  exclusive to all telecommunications operators at the technological level.  Generally SMS short codes are used for value added services for these Telcos such  as setting ring tones, television voting, mobile services and charity  donations.</p>
      </div>
        
    </div>
</div>

<
<div id="longdiv" class="hideover">
	<div class="container hideover">
    	<div class="divsocial">
        <ul>
        <li class="textin" style="width:150px; padding:15px 0px 0px 0px; font-size:12pt">Connect to us on:</li>
        <li><img src="img/facebook.png" alt="facebook" /></li>
        <li><img src="img/twitter.png" alt="twitter" /></li>
        <li><img src="img/linkedin.png" alt="linkedin" /></li>
        </ul>
        </div>
    	<div class="divsubscribe">
        <div id="errorMsg"></div>
        <form id="form1" name="form1" method="post" action="">
          <label for="textfield"></label>
          <input type="text" name="subs" id="subs" class="inputsearch" value="Subscribe to our newsletter" />
          <input type="button" name="news" id="news" value="Submit" class="divshit pop pstbutton" style="cursor:pointer" />
        </form>
        </div>
    </div>
</div>

<div id="footer">
  <?php require("footer.php") ?>
</div>
<script type="text/javascript">Cufon.now()
$(function(){
$('nav,.more,.header-more').sprites()

$('.header-slider').gSlider({
prevBu:'.hs-prev',
nextBu:'.hs-next'
})
})
$(window).load(function(){
$('.tumbvr')._fw({tumbvr:{
duration:2000,
easing:'easeOutQuart'
}})
.bind('click',function(){
location="index-3.html"
})

$('a[rel=prettyPhoto]').each(function(){
var th=$(this),
pb
th
.append(pb=$('<span class="playbutt"></span>').css({opacity:.7}))
pb
.bind('mouseenter',function(){
$(this)
.stop()
.animate({opacity:.9})
})
.bind('mouseleave',function(){
$(this)
.stop()
.animate({opacity:.7})
})
})
.prettyPhoto({theme:'dark_square'})
})
$(window).load(function() {
	$('.blueberry').blueberry();
});
</script>
</body>
</html>