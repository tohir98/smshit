<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="style/prettyPhoto.css" type="text/css">
<link href="style/SMSHIT.css" rel="stylesheet" type="text/css">
<link type="text/css" href="style/jquery.ui.all.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="style/jDev.css">

 <script src="js/jquery-1.7.1.js" type="text/javascript"></script>
<script type="text/javascript" src="script/portalScript.js"></script>
<script type="text/javascript" src="script/mobile.js"></script>

    <script src="js/cufon-yui.js" type="text/javascript"></script>
    <script src="js/cufon-replace.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/Josefin_Sans_600.font.js"></script>
    <script type="text/javascript" src="js/Lobster_400.font.js"></script>
    <script type="text/javascript" src="js/sprites.js"></script>
    <script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
    <script type="text/javascript" src="js/jquery.jplayer.settings.js"></script>
    <script type="text/javascript" src="js/gSlider.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
   <script type="text/javascript" src="js/jquery.blueberry.js"></script>
   

<title>SMShit Robocall</title>

</head>

<body>

<?php require("header.php"); ?>

<?php require("flashplayer.php") ?>
<div class="container hideover">
	<div class="" style="font-size:20px; color:#008ACC">Robocall</div>
    <div id="bodycontainer" class="hideover">
    	<div class="divcenter">
      <p>Reach out and connect with your  entire audience with a single phone call. As an integral part of marketing for  years, voice messaging delivers your promotional or informational alerts to  your entire audience, whether they rely on mobile or landline phones.<br />
        <strong>Creating  your own voice broadcast campaign right from your desktop:</strong></p>
      <ul>
        <li>Recording a message is simple: Use  your computer’s mic or upload a pre-recorded audio file.</li>
        <li>Send your voice message to one  person or a group of people at one time.</li>
        <li>Schedule your blast to go out  immediately or on a specific date and time.</li>
        <li>Enable your message to be sent again  if there is no answer or a busy signal.</li>
      </ul>
      <p align="center"><br />
        <strong>USEFULNESS</strong><br />
        <strong>Political  Campaign</strong></p>
      <p><strong>Managing Customer Relationships</strong></p>
      <ul>
        <li>Re-engage customers with a friendly  voice message.</li>
        <li>Target specific members or groups of  your audience with exclusive incentives.</li>
        <li>Send a holiday or seasonal greeting  to celebrate the occasion with your customers.</li>
      </ul>
      <p><strong>Announce  Events and Promotions</strong></p>
      <ul>
        <li>Record a personal and memorable  invitation to an event.</li>
        <li>Highlight exciting new promotions.</li>
        <li>Unveil new products or services.</li>
      </ul>
        </div>
        
    </div>
</div>


<div id="longdiv" class="hideover">
	<div class="container hideover">
    	<div class="divsocial">
        <ul>
        <li class="textin" style="width:150px; padding:15px 0px 0px 0px; font-size:12pt">Connect to us on:</li>
        <li><img src="img/facebook.png" alt="facebook" /></li>
        <li><img src="img/twitter.png" alt="twitter" /></li>
        <li><img src="img/linkedin.png" alt="linkedin" /></li>
        </ul>
        </div>
    	<div class="divsubscribe">
        <div id="errorMsg"></div>
        <form id="form1" name="form1" method="post" action="">
          <label for="textfield"></label>
          <input type="text" name="subs" id="subs" class="inputsearch" value="Subscribe to our newsletter" />
          <input type="button" name="news" id="news" value="Submit" class="divshit pop pstbutton" style="cursor:pointer" />
        </form>
        </div>
    </div>
</div>

<div id="footer">
  <?php require("footer.php") ?>
</div>
<script type="text/javascript">Cufon.now()
$(function(){
$('nav,.more,.header-more').sprites()

$('.header-slider').gSlider({
prevBu:'.hs-prev',
nextBu:'.hs-next'
})
})
$(window).load(function(){
$('.tumbvr')._fw({tumbvr:{
duration:2000,
easing:'easeOutQuart'
}})
.bind('click',function(){
location="index-3.html"
})

$('a[rel=prettyPhoto]').each(function(){
var th=$(this),
pb
th
.append(pb=$('<span class="playbutt"></span>').css({opacity:.7}))
pb
.bind('mouseenter',function(){
$(this)
.stop()
.animate({opacity:.9})
})
.bind('mouseleave',function(){
$(this)
.stop()
.animate({opacity:.7})
})
})
.prettyPhoto({theme:'dark_square'})
})
$(window).load(function() {
	$('.blueberry').blueberry();
});
</script>
</body>
</html>