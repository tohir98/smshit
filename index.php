<?php
session_start();
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="stylesheet" href="style/prettyPhoto.css" type="text/css">
<link href="style/SMSHIT.css" rel="stylesheet" type="text/css">
<link type="text/css" href="style/jquery.ui.all.css" rel="stylesheet" />
<link type="text/css" rel="stylesheet" href="style/jDev.css">

 <script src="js/jquery-1.7.1.js" type="text/javascript"></script>
<script type="text/javascript" src="script/portalScript.js"></script>
<script type="text/javascript" src="script/mobile.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.form.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.ui.core.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.ui.widget.js"></script>

    <script src="js/cufon-yui.js" type="text/javascript"></script>
    <script src="js/cufon-replace.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/Josefin_Sans_600.font.js"></script>
    <script type="text/javascript" src="js/Lobster_400.font.js"></script>
    <script type="text/javascript" src="js/sprites.js"></script>
    <script type="text/javascript" src="js/jquery.jplayer.min.js"></script>
    <script type="text/javascript" src="js/jquery.jplayer.settings.js"></script>
    <script type="text/javascript" src="js/gSlider.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.prettyPhoto.js"></script>
   <script type="text/javascript" src="js/jquery.blueberry.js"></script>
   

<title>WELCOME</title>

</head>

<body>

<?php require("header.php"); ?>

<?php require("flashplayer.php") ?>
<div class="container hideover">
	<div class="longtail"></div>
    <div id="bodycontainer" class="hideover">
    	<div class="hideover" style="width:900px; margin:auto">
    		<div class="divcont">
            	<div class="divimg1"></div>
                <div class="divtext">Businesses and organizations all over the world have realized the huge potential of interacting with the clients, suppliers and employees using bulk SMS.</div>
                <div class="divbnt"><a href="Bulksms.php">Read more</a></div>
        	</div>
        	<div class="divcont">
            	<div class="divimg2"></div>
                <div class="divtext">Starting, running your business or a campaign will not be effective until you start tailoring your messages to targeted locations and audiences. Now, you need to start making your campaign and adverts work for you!</div>
                <div class="divbnt"><a href="mobile.php">Read more</a></div>
        	</div>
        	<div class="divcont">
            	<div class="divimg3"></div>
                <div class="divtext">Social media has integrated itself into the busy, everyday lives of consumers. Businesses now have a unique voice with their audiences and kick-start the strongest form of marketing: word-of-mouth</div>
                <div class="divbnt"><a href="socialmarketing.php">Read more</a></div>
        	</div>
        	
        </div>
        <div class="hideover" style="width:900px; margin:auto">
    		<div class="divcont">
            	<div class="divimg5"></div>
                <div class="divtext">Enterprises need to reach their customers periodically and also at special occasions for various reasons. Sending news, credit card statements, bills, Alert sms are just some of the common examples.</div>
                <div class="divbnt"><a href="vasolution.php">Read more</a></div>
        	</div>
        	<div class="divcont">
            	<div class="divimg4"></div>
                <div class="divtext">Reach out and connect with your entire audience with a single phone call. As an integral part of marketing for years, voice messaging delivers your promotional or informational alerts to your entire audience, whether.</div>
                <div class="divbnt"><a href="robocall.php">Read more</a></div>
        	</div>
        	<div class="divcont">
            	<div class="divimg7"></div>
                <div class="divtext">Easily set up an email marketing campaign in minutes by choosing from a wide selection of professional email templates and accessing our database of e-mails of people segmented by gender, location and age.</div>
                <div class="divbnt"><a href="emailsolution.php">Read more</a></div>
        	</div>
        	
        </div>
    </div>
</div>


<div id="longdiv" class="hideover">
	<div class="container hideover">
    	<div class="divsocial">
        
        </div>
    	<div class="divsubscribe">
        <div id="errMsg"></div>
        <form id="form1" name="form1" method="post" action="">
          <label for="textfield"></label>
          <input type="text" name="subs" id="subs" class="inputsearch" value="Subscribe to our newsletter" />
          <input type="button" name="newss" id="newss" value="Submit" class="divshit pop pstbutton" style="cursor:pointer" />
        </form>
        </div>
    </div>
</div>

<div id="footer">
  <?php require("footer.php") ?>
</div>
<script type="text/javascript">Cufon.now()
$(function(){
$('nav,.more,.header-more').sprites()

$('.header-slider').gSlider({
prevBu:'.hs-prev',
nextBu:'.hs-next'
})
})
$(window).load(function(){
$('.tumbvr')._fw({tumbvr:{
duration:2000,
easing:'easeOutQuart'
}})
.bind('click',function(){
location="index-3.html"
})

$('a[rel=prettyPhoto]').each(function(){
var th=$(this),
pb
th
.append(pb=$('<span class="playbutt"></span>').css({opacity:.7}))
pb
.bind('mouseenter',function(){
$(this)
.stop()
.animate({opacity:.9})
})
.bind('mouseleave',function(){
$(this)
.stop()
.animate({opacity:.7})
})
})
.prettyPhoto({theme:'dark_square'})
})
$(window).load(function() {
	$('.blueberry').blueberry();
});
</script>
</body>
</html>