<?php
session_start();
if ($_SESSION['SMShitAdminsecagt'] != md5($_SERVER['HTTP_USER_AGENT']))
{
 header("Location: " . "index.php?mess=Your session is over");
  exit();
}

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Welcome</title>
<link href="../style/SMSHIT.css" rel="stylesheet" type="text/css">
<link href="../style/jquery.ui.all.css" rel="stylesheet" type="text/css">
<link type="text/css" rel="stylesheet" href="../style/jDev.css">

 <script src="../js/jquery-1.7.1.js" type="text/javascript"></script>
<script type="text/javascript" src="../script/portalScript.js"></script>
<script type="text/javascript" src="../script/mobile.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.form.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.ui.core.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.ui.widget.js"></script>

</head>

<body>
<?php require("header.php"); ?>
<div id="bodycontainer" class="hideover">
	<div class="userpage hideover">
    	<div class="container hideover">
    		<?php require("menu.php"); ?>
            
            <div class="hideover" id="pagecontainer">
            	<?php
					if (isset($_SESSION['SMShitAdminsecagt']))
  					{
						$load="apps/admin/index.php";
						if(isset($_GET['load']))
						{
							$load=$_GET['load'];
							$page=$_GET['pageload'];
							if($load == '')
							{
								$load="apps/".$page."/index.php";
							}
							else
							{
								$load="apps/".$page."/".$load.".php";	
							}
							
						}
						include($load);
					}
				
				?>
            </div>
    	</div>
    </div>
</div>
</body>
</html>