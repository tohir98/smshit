<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="../style/SMSHIT.css" rel="stylesheet" type="text/css">
 <script src="../js/jquery-1.7.1.js" type="text/javascript"></script>
<script type="text/javascript" src="../script/portalScript.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.form.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.ui.core.js"></script>
<script type="text/javascript" language="javascript" src="../js/jquery.ui.widget.js"></script>
<title>WELCOME</title>
</head>

<body>
<?php require("header.php"); ?>

<div id="bodycontainer" class="hideover">
	<div class="loginpage hideover">
    	<div class="divlogin hideover">
        	<form method="post" name="form1" action="">
        		<div style="font-size:14pt; padding-right:10pt; color:#F00" id="errorMsg"></div>
        		<ul>
        		<li>&nbsp;</li>
        		<li>Username</li>
                <li>&nbsp;</li>
				<li>
				<input name="username" type="text" class="inputsearch" style="width:96%" id="username" placeholder="username" autocapitalize="off" autocorrect="off" autocomplete="off" /></li>
                <li>&nbsp;</li>
                <li>Password</li>
                <li>&nbsp;</li>
                <li><input name="pass" type="password" class="inputsearch" style="width:96%" id="pass" placeholder="password" autocapitalize="off" autocorrect="off" autocomplete="off" /></li>
                <li>&nbsp;</li>
				<li><input type="button" id="loginAdminB" class="pstbutton forpst" value="ENTER" />
				</li>
                 <li>&nbsp;</li>
                  <li class="forgot"><a href="forgot.php">Forgot Password?</a></li>
				</ul>
	  		</form>
        </div>
    </div>
</div>
</body>
</html>