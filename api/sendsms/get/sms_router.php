<?php
/*
$test = new sms_router("2348067874398,2347083356654,2348052438899");
$test->Start_Filtering();
print_r($test->getErrorCode());
echo "<br/><br/>";
echo "******************Numbers going to RouteSMS >>************************<br/><br/>";
print_r($test->get_RouteSMS_terminal());
echo "<br/><br/>";
echo "******************Numbers going to mtnTerminal >>************************<br/><br/>";
print_r($test->get_mtnTerminal_terminal());
*/
class sms_router{
	//***************************Network Array ***********************************
	//private $allowed_points = new array("234803","234806","234802","234805","234807","234809");
	private $mtn = array("234803","234806","234813","234816","234703","234706","234810","234814","234903");	
	private $airtel =  array("234802","234808","234708","234812","234902","234701");	
	private $glo =  array("234805","234807","234705","234815","234905","234811");
	private $etisalat =  array("234809","234818","234817","234909");
	private $visafone =  array("2347025","2347026","234704");
	private $multilinks =  array("2347027","234709");
	private $starcomm =  array("2347028","2347029","234819");
	
	//***************** End of Array **********************************
	private $dest;
	private $otherTerminals;
	private $mtnTerminal;
	private $error_code;
	private $wrong;

	public function __construct($destinations){
		$this->dest = $destinations;
	}
	public function Start_Filtering(){
		if(strlen($this->dest)){
			$this->check_prefix($this->dest);
		}else{
			$this->setError("Could Not find destinations",0);
		}
	}
	private function check_prefix($numbers){
		try{
		$prefix = array_merge($this->mtn,$this->airtel,$this->glo,$this->etisalat,$this->visafone,$this->multilinks,$this->starcomm);
		$number_array = explode(",",$numbers);
		for($i=0;$i< count($number_array);$i++){
			$pref = $number_array[$i]{0}.$number_array[$i]{1}.$number_array[$i]{2}.$number_array[$i]{3}.$number_array[$i]{4}.$number_array[$i]{5};
				if(in_array($pref,$prefix)){
					if(in_array($pref,$this->mtn)){
						$this->mtnTerminal[] = $number_array[$i];
					}else{
						$this->otherTerminals[] = $number_array[$i];
					}
				}else{
					$this->wrong[]=$number_array[$i];	
				}
		}
		}catch(Exception $e){
			$this->setError("Error occurred during filtering",1);
		}
	}
	public function get_OtherSMS_terminal(){
		return $this->otherTerminals;
	}
	public function get_mtnTerminal_terminal(){
		return $this->mtnTerminal;
	}
	private function setError($error,$code){
		$this->error_code[0] = $code;
		$this->error_code[1] = $error;
	}
	public function getErrorCode(){
		return $this->error_code;
	}
	public function getWrongNumbers(){
		return $this->wrong;	
	}
}
?>