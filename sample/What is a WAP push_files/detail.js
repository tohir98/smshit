
RightNow.Widget.KeywordText2=function(data,instanceID)
{this.data=data;this.instanceID=instanceID;this._eo=new RightNow.Event.EventObject();this._textElement=document.getElementById("rn_"+this.instanceID+"_Text");this.data.js.initialValue=this._decoder(this.data.js.initialValue);if(this._textElement)
{this._searchedOn=this._textElement.value;if(this.data.js.initialValue&&this._textElement.value!==this.data.js.initialValue)
this._textElement.value=this.data.js.initialValue;this._setFilter();var changeEvent=(YAHOO.env.ua.ie)?"focusout":"change";YAHOO.util.Event.addListener(this._textElement,changeEvent,this._onChange,null,this);RightNow.Event.subscribe("evt_keywordChangedResponse",this._onChangedResponse,this);RightNow.Event.subscribe("evt_reportResponse",this._onChangedResponse,this);RightNow.Event.subscribe("evt_getFiltersRequest",this._onGetFiltersRequest,this);RightNow.Event.subscribe("evt_resetFilterRequest",this._onResetRequest,this);if(this.data.attrs.initial_focus)
this._textElement.focus();}};RightNow.Widget.KeywordText2.prototype={_onChange:function(evt)
{this._eo.data=this._textElement.value;this._eo.filters.data=this._textElement.value;RightNow.Event.fire("evt_keywordChangedRequest",this._eo);},_onGetFiltersRequest:function(type,args)
{this._eo.filters.data=YAHOO.lang.trim(this._textElement.value);this._searchedOn=this._eo.filters.data;RightNow.Event.fire("evt_searchFiltersResponse",this._eo);},_setFilter:function()
{this._eo.w_id=this.instanceID;this._eo.filters={"searchName":this.data.js.searchName,"data":this.data.js.initialValue,"rnSearchType":this.data.js.rnSearchType,"report_id":this.data.attrs.report_id};},_onChangedResponse:function(type,args)
{if(RightNow.Event.isSameReportID(args,this.data.attrs.report_id))
{var data=RightNow.Event.getDataFromFiltersEventResponse(args,this.data.js.searchName,this.data.attrs.report_id),newValue=(data===null)?this.data.js.initialValue:data;newValue=this._decoder(newValue);if(this._textElement.value!==newValue)
this._textElement.value=newValue;}},_onResetRequest:function(type,args)
{if(RightNow.Event.isSameReportID(args,this.data.attrs.report_id)&&(args[0].data.name===this.data.js.searchName||args[0].data.name==="all"))
{this._textElement.value=this._searchedOn;}},_decoder:function(value)
{if(value)
return value.replace(/&gt;/g,'>').replace(/&lt;/g,'<').replace(/&#039;/g,"'").replace(/&quot;/g,'"');return value;}};
RightNow.Widget.SearchButton2=function(data,instanceID)
{this.data=data;this.instanceID=instanceID;this._requestInProgress=false;this._searchButton=document.getElementById("rn_"+this.instanceID+"_SubmitButton");this._enableClickListener();RightNow.Event.subscribe("evt_reportResponse",this._onSearchResponse,this);};RightNow.Widget.SearchButton2.prototype={_startSearch:function(evt)
{if(this._requestInProgress)
return false;if(!this.data.attrs.popup_window&&(!this.data.attrs.report_page_url&&(this.data.attrs.target==='_self')))
this._disableClickListener();if(YAHOO.env.ua.ie!==0)
{if(!this._parentForm)
this._parentForm=YAHOO.util.Dom.getAncestorByTagName("rn_"+this.instanceID,"FORM");if(this._parentForm&&window.external&&"AutoCompleteSaveForm"in window.external)
{window.external.AutoCompleteSaveForm(this._parentForm);}}
var eo=new RightNow.Event.EventObject();eo.w_id=this.instanceID;eo.filters={report_id:this.data.attrs.report_id,reportPage:this.data.attrs.report_page_url,target:this.data.attrs.target,popupWindow:this.data.attrs.popup_window,width:this.data.attrs.popup_window_width_percent,height:this.data.attrs.popup_window_height_percent};RightNow.Event.fire("evt_searchRequest",eo);},_onSearchResponse:function(type,args)
{if(args[0].filters.report_id==this.data.attrs.report_id)
this._enableClickListener();},_enableClickListener:function()
{this._requestInProgress=false;YAHOO.util.Event.addListener(this._searchButton,"click",this._startSearch,null,this);},_disableClickListener:function()
{this._requestInProgress=true;YAHOO.util.Event.removeListener(this._searchButton,"click",this._startSearch);}};
RightNow.Widget.AnswerFeedback2=function(data,instanceID)
{this.data=data;this.instanceID=instanceID;this._rate=0;this._dialog=this._keyListener=null;if(!document.getElementById("rn_"+this.instanceID+"_FeedbackTextarea"))
{RightNow.UI.DevelopmentHeader.addJavascriptError(RightNow.Text.sprintf(RightNow.Interface.getMessage("ANSWERFEEDBACK2_DIALOG_MISSING_REQD_MSG"),"rn_"+this.instanceID+"_FeedbackTextarea"));return;}
RightNow.Event.subscribe("evt_answerRatingSubmitResponse",this._onRatingResponseReceived,this);RightNow.Event.subscribe("evt_answerFeedbackSubmitResponse",this._onResponseReceived,this);var Event=YAHOO.util.Event;if(this.data.js.buttonView)
{var noButton=document.getElementById("rn_"+this.instanceID+"_RatingNoButton"),yesButton=document.getElementById("rn_"+this.instanceID+"_RatingYesButton");Event.addListener(noButton,"click",this._onClick,1,this);Event.addListener(yesButton,"click",this._onClick,2,this);Event.addListener([noButton,yesButton],"mouseover",function(){this._wasMouse=true;},null,this);Event.addListener([noButton,yesButton],"mouseout",function(){this._wasMouse=false;},null,this);}
else if(this.data.attrs.use_rank_labels)
{var ratingButton="rn_"+this.instanceID+"_RatingButton_";for(var i=1;i<=this.data.attrs.options_count;++i)
{Event.addListener(ratingButton+i,"click",this._onClick,i,this);Event.addListener(ratingButton+i,"mouseover",function(){this._wasMouse=true;},null,this);Event.addListener(ratingButton+i,"mouseout",function(){this._wasMouse=false;},null,this);}}
else
{var ratingCell="rn_"+this.instanceID+"_RatingCell_";for(var i=1;i<=this.data.attrs.options_count;++i)
{Event.addListener(ratingCell+i,"mouseover",this._onCellOver,i,this);Event.addListener(ratingCell+i,"focus",this._onCellOver,i,this);Event.addListener(ratingCell+i,"mouseout",this._onCellOut,i,this);Event.addListener(ratingCell+i,"blur",this._onCellOut,i,this);Event.addListener(ratingCell+i,"click",this._onClick,i,this);}}};RightNow.Widget.AnswerFeedback2.prototype={_onClick:function(type,args)
{if(this.data.js.buttonView||this.data.attrs.use_rank_labels)
{var ratingButtons=document.getElementById("rn_"+this.instanceID+"_RatingButtons");if(ratingButtons)
YAHOO.util.Event.purgeElement(ratingButtons,true);}
else
{this._onCellOver(0,args);YAHOO.util.Event.preventDefault(type);var rateMeter=document.getElementById("rn_"+this.instanceID+"_RatingMeter");if(rateMeter)
YAHOO.util.Event.purgeElement(rateMeter,true);for(var cell,i=0;i<=this.data.attrs.options_count;++i)
{cell=document.getElementById("rn_"+this.instanceID+"_RatingCell_"+i);if(cell)
{for(var j=0;j<cell.childNodes.length;j++)
{if(cell.childNodes[j].tagName&&cell.childNodes[j].tagName.toLowerCase()==="span"&&YAHOO.util.Dom.hasClass(cell.childNodes[j],"rn_ScreenReaderOnly"))
cell.childNodes[j].innerHTML=RightNow.Text.sprintf(RightNow.Interface.getMessage("PCT_D_OF_PCT_D_SELECTED_LBL"),args,this.data.attrs.options_count);}}}}
this._rate=args;this._submitAnswerRating();if(this._rate<=this.data.attrs.dialog_threshold)
{if(this.data.attrs.feedback_page_url)
{var pageString=this.data.attrs.feedback_page_url;pageString=RightNow.Url.addParameter(pageString,"a_id",this.data.js.answerID);pageString=RightNow.Url.addParameter(pageString,"session",RightNow.Url.getSession());window.open(pageString,'',"resizable, scrollbars, width=630, height=400");}
else
{this._showDialog();}}},_showDialog:function()
{if(!this._dialog)
{var buttons=[{text:this.data.attrs.label_send_button,handler:{fn:this._onSubmit,scope:this},isDefault:true},{text:this.data.attrs.label_cancel_button,handler:{fn:this._onCancel,scope:this},isDefault:false}],dialogForm=document.getElementById("rn_"+this.instanceID+"_AnswerFeedback2Form");this._dialog=RightNow.UI.Dialog.actionDialog(this.data.attrs.label_dialog_title,dialogForm,{"buttons":buttons,"dialogDescription":"rn_"+this.instanceID+"_DialogDescription",width:this.data.attrs.dialog_width});this._keyListener=RightNow.UI.Dialog.addDialogEnterKeyListener(this._dialog,this._onSubmit,this);YAHOO.util.Dom.removeClass(dialogForm,"rn_Hidden");YAHOO.util.Dom.addClass(this._dialog.id,'rn_AnswerFeedback2Dialog');}
this._emailField=this._emailField||document.getElementById("rn_"+this.instanceID+"_EmailInput");this._errorDisplay=this._errorDisplay||document.getElementById("rn_"+this.instanceID+"_ErrorMessage");this._feedbackField=this._feedbackField||document.getElementById("rn_"+this.instanceID+"_FeedbackTextarea");if(this._errorDisplay)
{this._errorDisplay.innerHTML="";YAHOO.util.Dom.removeClass(this._errorDisplay,'rn_MessageBox rn_ErrorMessage');}
this._dialog.show();var focusElement;if(this._emailField&&this._emailField.value==='')
focusElement=this._emailField;else
focusElement=this._feedbackField;focusElement.focus();RightNow.UI.Dialog.enableDialogControls(this._dialog,this._keyListener);},_onSubmit:function(type,args)
{var target=(args&&args[1])?YAHOO.util.Event.getTarget(args[1]):null;if(type==="keyPressed"&&target&&(target.tagName==='A'||target.tagName==='TEXTAREA'||target.innerHTML===this.data.attrs.label_send_button||target.innerHTML===this.data.attrs.label_cancel_button))
return;if(!this._validateDialogData()){return;}
RightNow.UI.Dialog.disableDialogControls(this._dialog,this._keyListener);this._incidentCreateFlag=true;this._submitFeedback();},_onCancel:function()
{RightNow.UI.Dialog.disableDialogControls(this._dialog,this._keyListener);this._closeDialog(true);},_validateDialogData:function()
{YAHOO.util.Dom.removeClass(this._errorDisplay,'rn_MessageBox rn_ErrorMessage');this._errorDisplay.innerHTML="";var returnValue=true;if(this._emailField)
{this._emailField.value=YAHOO.lang.trim(this._emailField.value);if(this._emailField.value==="")
{this._addErrorMessage(RightNow.Text.sprintf(RightNow.Interface.getMessage("PCT_S_IS_REQUIRED_MSG"),this.data.attrs.label_email_address),this._emailField.id);returnValue=false;}
else if(!RightNow.Text.isValidEmailAddress(this._emailField.value))
{this._addErrorMessage(this.data.attrs.label_email_address+' '+RightNow.Interface.getMessage("FIELD_IS_NOT_A_VALID_EMAIL_ADDRESS_MSG"),this._emailField.id);returnValue=false;}}
this._feedbackField.value=YAHOO.lang.trim(this._feedbackField.value);if(this._feedbackField.value==="")
{this._addErrorMessage(RightNow.Text.sprintf(RightNow.Interface.getMessage("PCT_S_IS_REQUIRED_MSG"),this.data.attrs.label_comment_box),this._feedbackField.id);returnValue=false;}
return returnValue;},_closeDialog:function(cancelled)
{if(!cancelled)
{this._feedbackField.value="";}
if(this._errorDisplay)
{this._errorDisplay.innerHTML="";YAHOO.util.Dom.removeClass(this._errorDisplay,'rn_MessageBox rn_ErrorMessage');}
if(this._dialog)
this._dialog.hide();},_submitFeedback:function()
{var eventObject=new RightNow.Event.EventObject();eventObject.w_id=this.instanceID;eventObject.data={"summary":this.data.js.summary,"a_id":this.data.js.answerID,"rate":this._rate,"dialog_threshold":this.data.attrs.dialog_threshold,"options_count":this.data.attrs.options_count,"message":this._feedbackField.value};if(this.data.js.isProfile)
eventObject.data.email=this.data.js.email;else if(this._emailField)
eventObject.data.email=this._emailField.value;RightNow.Event.fire("evt_answerFeedbackRequest",eventObject);return false;},_onResponseReceived:function(type,arg)
{if(this._incidentCreateFlag&&arg[1][0].w_id===this.instanceID)
{this._incidentCreateFlag=false;if(typeof(arg[0])==="string")
{RightNow.UI.Dialog.messageDialog(arg[0],{icon:"WARN",exitCallback:{fn:this._enableDialog,scope:this}});}
else
{RightNow.UI.Dialog.messageDialog(RightNow.Interface.getMessage("THANKS_FOR_YOUR_FEEDBACK_MSG"),{exitCallback:{fn:this._closeDialog,scope:this}});}}
else
{this._closeDialog();}},_submitAnswerRating:function()
{var eventObject=new RightNow.Event.EventObject();eventObject.w_id=this.instanceID;eventObject.data={"a_id":this.data.js.answerID,"rate":this._rate,"options_count":this.data.attrs.options_count,"dialog_threshold":this.data.attrs.dialog_threshold};RightNow.ActionCapture.record('answer','rate',this.data.js.answerID);RightNow.ActionCapture.record('answer','rated',((this._rate-1)/(this.data.attrs.options_count-1))*100);RightNow.Event.fire("evt_answerRatingRequest",eventObject);},_onRatingResponseReceived:function(type,arg)
{var thanksLabel=document.getElementById("rn_"+this.instanceID+"_ThanksLabel");if(thanksLabel)
{if(this._wasMouse)
{thanksLabel.innerHTML=this.data.attrs.label_feedback_submit;}
else if(this._rate>this.data.attrs.dialog_threshold)
{RightNow.UI.Dialog.messageDialog(this.data.attrs.label_feedback_submit);}}},_addErrorMessage:function(message,focusElement)
{if(this._errorDisplay)
{YAHOO.util.Dom.addClass(this._errorDisplay,'rn_MessageBox rn_ErrorMessage');var newMessage='<a href="javascript:void(0);" onclick="document.getElementById(\''+focusElement+'\').focus(); return false;">'+message+'</a>';var oldMessage=this._errorDisplay.innerHTML;if(oldMessage==="")
{this._errorDisplay.innerHTML=newMessage;}
else
{this._errorDisplay.innerHTML=oldMessage+'<br/>'+newMessage;}
this._errorDisplay.firstChild.focus();}},_onCellOver:function(type,chosenRating)
{if(type.type==="mouseover")
this._wasMouse=true;this._updateCellClass(1,chosenRating+1,"rn_RatingCellOver","add");this._updateCellClass(chosenRating+1,this.data.attrs.options_count+1,"rn_RatingCellOver","remove");},_updateCellClass:function(minBound,maxBound,cssClass,removeOrAddClass)
{var cssFunc=(removeOrAddClass==="add")?YAHOO.util.Dom.addClass:YAHOO.util.Dom.removeClass;for(var i=minBound;i<maxBound;i++)
cssFunc("rn_"+this.instanceID+"_RatingCell_"+i,cssClass);},_onCellOut:function(type,args)
{if(type.type==="mouseout")
this._wasMouse=false;this._updateCellClass(1,this.data.attrs.options_count+1,"rn_RatingCellOver","remove");}};