/* JQuery Portal Scripts */


 $(document).ready(function() {


		$('#ajax_answer a[href*=#]').live('click', function() {
			if(this.hash.replace(/#/,'')) {
				var $target = $(this.hash), target = this.hash;
				
				if ($target.length) {
        	var targetOffset = $target.offset().top;
        	$('html, body').animate({scrollTop: targetOffset}, 400);
					return false;
				}
			}
		});

		/*
		*  This controls the retreival (via AJX), expanding and contracting of the selected answer 
		*
		*/
		
		var clicks = 0;
		
		$(".expander").live('click', function (event) {

		if(clicks == 0) {
			clicks = 1;


			var faq_id = $(this).attr( "id" ).replace( /question_/i, '') ;
			var question = this;
			
			if($(this).hasClass("selected")) {
				$(this).toggleClass('selected');
				$(this).next(".answer").animate({height:'toggle', opacity:'toggle'}, "100").toggleClass('selected');
				$('#question_'+faq_id+' span a').css('background-position', '7px 5px');
				clicks = 0;
				$('#answer_'+faq_id).css('display', 'none');
			} 
			else {
				if($('#answer_'+faq_id).hasClass("opened")) {
					$('#answer_'+faq_id).css('display', 'block');
					$(question).next(".answer").animate({height:'toggle', opacity:'toggle'}, "100").toggleClass('selected');
					$('#question_'+faq_id+' span a').css('background-position', '7px -51px');
					$(this).toggleClass('selected');
					clicks = 0;
				}
				else {
						$(question).next("li span a").css("background-poition-y", "20");
						$('#question_'+faq_id+' .loading img').src = $('#question_'+faq_id+' .loading img').src;
						$('#question_'+faq_id+' .loading').css('display', 'inline');
						$('#question_'+faq_id+' span a').css('background-position', '7px -20px');
						$(this).toggleClass('selected');
						$('#answer_'+faq_id).attr('src', '/app/answers/detail_ajax/a_id/'+faq_id);
						$(question).next("li").css("display", "inline");
						$('#answer_'+faq_id).height('0');						
						
						$('#answer_'+faq_id).load(function(){
							
							$('#answer_'+faq_id).css('display', 'block');
							var helpFrame = $('#answer_'+faq_id);
							var innerDoc = (helpFrame.get(0).contentDocument) ? helpFrame.get(0).contentDocument : helpFrame.get(0).contentWindow.document;
							helpFrame.height(innerDoc.body.scrollHeight + 35);
							$('#question_'+faq_id+' span a').css('background-position', '7px -51px');
							$('#question_'+faq_id+' .loading').css('display', 'none');				
							$('#answer_'+faq_id).addClass("opened");
							clicks = 0;


							$('#answer_'+faq_id).contents().find("a").each(function(index){
  							 if((this.href.indexOf('/app/') != -1 || this.href.indexOf('http://') != -1 || this.href.indexOf('https://') != -1) && this.href.indexOf('#') == -1) {
                  if(this.target != '_blank') {
                    this.setAttribute('target', "_parent");
                  }
                }
              });
						});
				}
			}
			}
			return false;	
		});
		
/*
		$(".expander").live('click', function (event) {

		if(clicks == 0) {
			clicks = 1;


			var faq_id = $(this).attr( "id" ).replace( /question_/i, '') ;
			var question = this;
			
			if($(this).hasClass("selected")) {
				$(this).toggleClass('selected');
				$(this).next(".answer").animate({height:'toggle', opacity:'toggle'}, "600").toggleClass('selected');
				$('#question_'+faq_id+' span a').css('background-position', '7px 5px');
				clicks = 0;
			} 
			else {

				if($('#answer_'+faq_id).hasClass("opened")) {		
					$(question).next(".answer").animate({height:'toggle', opacity:'toggle'}, "600").toggleClass('selected');
					$('#question_'+faq_id+' span a').css('background-position', '7px -51px');
					$(this).toggleClass('selected');
					clicks = 0;
				}
				else {
						$(question).next("li span a").css("background-poition-y", "20");
						$('#question_'+faq_id+' .loading img').src = $('#question_'+faq_id+' .loading img').src;
						$('#question_'+faq_id+' .loading').css('display', 'inline');
						$('#question_'+faq_id+' span a').css('background-position', '7px -20px');
						$(this).toggleClass('selected');
						$.ajax({
						  type: "GET",
						  url: "/app/answers/detail_ajax/a_id/"+faq_id+"",
						  dataType: "html",
						  dataFilter: function(data, type) {			  	
						  	//console.log(data);
						  	var new_string = data.replace("\<script type\='text\/javascript' src\='\/rnt\/rnw\/yui\_2\.7\/plugins\/containerariaplugin\-min\.js'\>\<\/script\>", " ");
						  	var final_string = new_string.replace("\<script type\='text\/javascript' src\='\/euf\/rightnow\/js\/9\.11\.0\.1\.0\.128\/min\/RightNow\.js'\>\<\/script\>", " ");
							var final_string = final_string.replace("\<script type\='text\/javascript' src\='\/euf\/rightnow\/debug\-js\/RightNow\.UI\.js'\>\<\/script\>", " ");
							var final_string = final_string.replace("\<script type\='text\/javascript' src\='\/euf\/rightnow\/debug\-js\/RightNow\.Ajax\.js'\>\<\/script\>", " ");
							var final_string = final_string.replace("\<script type\='text\/javascript' src\='\/euf\/rightnow\/debug\-js\/RightNow\.js'\>\<\/script\>", " ");
							var final_string = final_string.replace("\<script type\='text\/javascript' src\='\/euf\/rightnow\/debug\-js\/RightNow\.Text\.js'\>\<\/script\>", " ");
							var final_string = final_string.replace("\<script type\='text\/javascript' src\='\/euf\/rightnow\/debug\-js\/RightNow\.Url\.js'\>\<\/script\>", " ");
							var final_string = final_string.replace("\<script type\='text\/javascript' src\='\/euf\/rightnow\/debug\-js\/RightNow\.Event\.js'\>\<\/script\>", " ");
							var final_string = final_string.replace("\<script type\='text\/javascript' src\='\/euf\/rightnow\/debug\-js\/TreeViewAriaPlugin\.js'\>\<\/script\>", " ");
							var final_string = final_string.replace("\<script type\='text\/javascript' src\='\/euf\/rightnow\/debug\-js\/RightNow\.UI\.AbuseDetection\.js'\>\<\/script\>", " ");
							var final_string = final_string.replace("\<script type\='text\/javascript' src\='\/euf\/rightnow\/debug\-js\/RightNow\.UI\.DevelopmentHeader\.js'\>\<\/script\>", " ");
							
						  	return final_string;
						  },
						  success: function(html){
						    $('#answer_'+faq_id).html(html);
						    $(question).next(".answer").animate({height:'toggle', opacity:'toggle'}, "600").toggleClass('selected');
								$('#question_'+faq_id+' span a').css('background-position', '7px -51px');
								$('#question_'+faq_id+' .loading').css('display', 'none');				
								$('#answer_'+faq_id).addClass("opened");
								clicks = 0;
							}
					});
				}
			}
			}
			return false;	
		});
*/
	/****** CONTACT ASK PAGE SCRIPTS ********/
	
	
	/* 	
	*	Listen for a hover on the top level subjects
	*	and show the appropriate callout information
	*/
	
	$("#subject li").hover(
		function () {
			$(this).children(".callout").show();	
		},
		function () {
			$(this).children(".callout").hide();	
		}
	);


	/* 	
	*	Listen for a click on the top level subjects
	*	and show the appropriate arrow image
	*/

	$("#subject li").click(function () {
 		$(".second-subject").hide();
 		$(".level-arrow-down").hide();
 		$("#subject .callout").hide();
	});
	
	/* Add the highlight to the selected subject */
	
	$("#subject li").click(function () {
 		$("#subject li").removeClass("highlight");
 		$(this).addClass("highlight");
	});

	/* Show the Internet or Home phone sub-level subjects 
	*	 There is a delay of 1 second on this function
	*  so the hidden topic field has time to complete its 
	*  AJAX call. 
	*
	*/
		
/* 	$("#internet-radio").click(function () {
 		if($('#fixed-internet').is(':checked')) {
	 			$("#subject-internet").show();
	 			$("#internet-level-arrow-down").show();
 		}
 		else {
	 		setTimeout(function() {
	 			$("#subject-internet").show();
	 			$("#internet-level-arrow-down").show();
	 		}, 1000);
 		}
	}); */

 	$("#internet-radio").click(function () {
	 		setTimeout(function() {
	 			$("#subject-internet").show();
	 			$("#internet-level-arrow-down").show();
	 			$("#subject-home-phone").hide();
 				$("#home-level-arrow-down").hide();
 				$("#subject-internet ul li").removeClass("highlight");
 				$("#subject-home-phone ul li").removeClass("highlight");
	 		}, 800);
	});
	
	$("#home-phone-radio").click(function () {
 		setTimeout(function() {
 			$("#subject-home-phone").show();
 			$("#home-level-arrow-down").show();
 			$("#subject-internet").hide();
	 		$("#internet-level-arrow-down").hide();
 			$("#subject-internet ul li").removeClass("highlight");
 			$("#subject-home-phone ul li").removeClass("highlight");
 		}, 800);
	});

	/* Add the highlight to the selected subject */

	$("#subject-internet ul li").click(function (){
		$("#subject-internet ul li").removeClass("highlight");
 		$(this).addClass("highlight");
		$(this).children("input[@type=radio]").attr("checked","checked");
		var drop_down = $("select[name='lvl2']")[0];
		for (var i=0; i < drop_down.length; i++) {
			if(drop_down[i].value == $(this).children("input[@type=radio]").val()) {
				drop_down[i].selected = true;
			}
		} 
	});

	
	$("#subject-home-phone ul li").click(function (){
		$("#subject-home-phone ul li").removeClass("highlight");
 		$(this).addClass("highlight");
		$(this).children("input[@type=radio]").attr("checked","checked");
		var drop_down = $("select[name='lvl2']")[0];
		for (var i=0; i < drop_down.length; i++) {
			if(drop_down[i].value == $(this).children("input[@type=radio]").val()) {
				drop_down[i].selected = true;
			}
		}
	});	


	/* 
	* Simple hide/show for the attachment on the contact us page
	* must be a hide/show rather than a toggle because of IE6
	* 
	*/

	$("input#attachment").click(function () {
		$("#attach-panel").toggle();
	});

	/* 
	* hide/show for the incident custom fields for account information
	* 
	*/

	$("input#authenticate").click(function () {
		
		if($("#mobile-dont-know").hasClass("authenticate-opened")) {
			$("#mobile-dont-know").slideToggle("fast");
			$("#mobile-dont-know").toggleClass("authenticate-opened");
		}
		else {			
			if($("input[name = 'subject']:checked").val() == 4396) {
				$("#security").slideToggle("fast");
				$("#security").toggleClass("authenticate-opened");		
			}
			else {
				if($("#security").hasClass("authenticate-opened")) {
					$("#security").slideToggle("fast");
				}
				else {
					$("#mobile-dont-know").slideToggle("fast"); 			
				}
			}
		}
	});

	$("a#dont-know-pin").click(function () {
	 	$("#mobile-dont-know").show();
	 	$("#mobile-dont-know").addClass("authenticate-opened");
	 	$("#security").removeClass("authenticate-opened");
	 	$("#security").hide();
		return(false);
	});


	$("a#dont-know").click(function () {
	 	$("#mobile-dont-know").hide();
	 	$("#security").addClass("authenticate-opened");
	 	$("#mobile-dont-know").removeClass("authenticate-opened");
	 	$("#security").show();
		return(false);
	});


	/* 
	* hide/show for the callouts that are attachted to the
	* incident custom fields
	* 
	*/

	$("#security #number input").click(function () {
 		$("#security .callout").hide();
	});

	$("#security #pin input").click(function () {
 		$("#security #pin .callout").show();
	});

	$("#mobile-dont-know #date-of-birth-item input").click(function () {
 		$("#mobile-dont-know .callout").hide();
 		$("#mobile-dont-know #date-of-birth-item .callout").show();
	});
  	
	$("#mobile-dont-know #time-as-customer-item input").click(function () {
 		$("#mobile-dont-know .callout").hide();
 		$("#mobile-dont-know #time-as-customer-item .callout").show();
	});
  	
	$("#mobile-dont-know #payment-method-item input").click(function () {
	 	$("#mobile-dont-know .callout").hide();
	 	$("#mobile-dont-know #payment-method-item .callout").show();
	});


	/* 
	* Once a question has been submitted, the option to create an account is avaliable
	* for users who are not logged in. This checks (via AJAX) if the user has already created a 
	* profile or not and either displays a form for the user to create a profile or 
	* a message saving the question has been saved to thier profile.
	* 
	*/

	$("#save-profile").click(function () {
				
		if($(this).hasClass("selected")) {
	 		$(this).toggleClass('selected');	
	 		$("#profile").slideUp();
	 		$("#profile-exsits").slideUp();
	 	}
	 	else {			
			$('#save .loading').css('display', 'inline');
			$.post("/ci/ajaxCustom/check_profile_exists", { email_address: $('#email_address').val() },function(data){    		
    		if(data == 'false') {
    			$("#profile").slideToggle();
    			$('#save-profile').toggleClass('selected');
    		}
    		else {
    			$("#profile-exsits").slideToggle();
    			$('#save-profile').toggleClass('selected');
    		}
    		$('#save .loading').css('display', 'none');
  		});
	 	} 	
	});
		
		
	/* 
	* Submits the profile to be created with a redirection to the sucess page when completed.
	*
	*/
		
	$("#create_profile_submit").click(function() {				
		
		var error_message = '';
		
		if($('#profile #first_name').val() == '') {
			error_message += '<span class="ErrorSpan">Please enter your first name</span>';
		}
	
		if($('#profile #last_name').val() == '') {
			error_message += '<span class="ErrorSpan">Please enter your last name</span>';
		}
	
		if($('#profile #password').val() == '') {
			error_message += '<span class="ErrorSpan">Please enter a password</span>';
		}
		
		if($('#profile #password_again').val() == '') {
			error_message += '<span class="ErrorSpan">Please re-enter your password</span>';
		}	
	
		if($('#profile #password').val() != $('#profile #password_again').val()) {
			error_message += '<span class="ErrorSpan">Your My Help passwords do not match. </span>';
		}		
		
		if(error_message != '') {
			$('#rn_ErrorLocation').show();
			$('#rn_ErrorLocation').html(error_message);
		}
		else {
		
			$('#create-profile-button .loading').css('display', 'inline');
			$.post("/ci/ajaxCustom/create_profile", { 
				first_name: $('#first_name').val(),
				last_name: $('#last_name').val(),
				email_address: $('#email_address').val(),
				first_password: $('#password').val(),
				second_password: $('#password_again').val()},function(data){
	  		
		  	if(data == '1') {
		  		window.location.href = "/app/profile_created";
		  	}
		    else {
		    	alert('There was an error creating your profile');
		    }
	  		$('#create-profile-button .loading').css('display', 'none');
			});	
		}		
	});	


	$("#answer_feedback_thumbs_up").click(function() {
		$("#answer_feedback_thumbs_down").css("background-position", "left -18px").css("color","#929292");
		$("#answer_feedback_thumbs_up").css("background-position", "left 0px").css("color","#ffffff");
	});
	
	$("#answer_feedback_thumbs_down").click(function() {
		$("#answer_feedback_thumbs_up").css("background-position", "left -18px").css("color","#929292");
		$("#answer_feedback_thumbs_down").css("background-position", "left 0px").css("color","#ffffff");
	});	


/**** SITE FEEDBACK SCRIPTS ******/

	/* Change the position of the thumbs background image to indicate which option was selected */

	$("#thumbs_up").click(function() {
		$("#feedback_rating_input").val("Good");
		$("#feedback_url_input").val(window.location);
		$("#feedback_content").animate({height: 250}, 500); 
		$("#thumbs_down").css("background-position", "left -18px").css("color","#929292");
		$("#thumbs_up").css("background-position", "left 0px").css("color","#ffffff");
	});
	
	$("#thumbs_down").click(function() {
		$("#feedback_rating_input").val("Bad");
		$("#feedback_url_input").val(window.location);
		$("#feedback_content").animate({height: 250}, 500); 
		$("#thumbs_up").css("background-position", "left -18px").css("color","#929292");
		$("#thumbs_down").css("background-position", "left 0px").css("color","#ffffff");
	});	
	
	
	/* Change the message in the box to tell the user that the feedback has been submitted */
	
	$("#send_comment").click(function() {
		$("#feedback_content").animate({height: 0}, 500);
		$("#feedback_header strong.header").html('Many thanks for your feedback - we\'ll see what we can do');
		return false;
	});		
	
	$("#feedback_content #cancel").click(function() {
		$("#feedback_content").animate({height: 0}, 500);
		$("#feedback_header strong.header").html('Many thanks for your feedback - we\'ll see what we can do');
		return false;
	});		
	
	
	/* A simple script to open the print dialog for the print button on the homepage */
	
	$("#print-home").click(function() {
		window.print();
	});


	/* 	When a top level subject is selected on the contact us form, this changes the hidden topics form field value and
	*		fires the onchange event that YUI has listening on it to update the second level options (via AJAX) 
	*
	*/

	$('.lvl1_subject').click(function() {
			
		//var drop_down = document.getElementById("lvl1MenuFilterForm_0");
		var drop_down = $("select[name='lvl1']")[0];
		var elems_internet = document.getElementsByName('internet-subject');
		
		for(i=0;i<elems_internet.length;i++) {
			elems_internet[i].checked=false;
		}
	
		var elems_phone = document.getElementsByName('home-phone-subject');
		
		for(i=0;i<elems_phone.length;i++) {
			elems_phone[i].checked=false;
		}
	
		for (var i=0; i < drop_down.length; i++) {
			if (drop_down[i].value == $(this).val()) {
				drop_down[i].selected = true;
	
				//On IE
				if(drop_down.fireEvent) {
					drop_down.fireEvent('onchange');
				}
	
				//On Gecko based browsers
				if(document.createEvent) {
	
					var evt = document.createEvent('HTMLEvents');
	
					if(evt.initEvent){
						evt.initEvent('change', true, true);
					}
	
					if(drop_down.dispatchEvent) {
						drop_down.dispatchEvent(evt);
					}
				}
			}
		} 
	});
   
   
   /* Changes the URL when a topic filter is selected */
   
   $('#navigation a').click(function() {
	   	var topic_name = $(this).attr('title');
	   	var category_id = $(this).attr('id');
			var keyword = $('.KeywordField').val();	
			var redirection_url = "";
		
			if(keyword) {
				redirection_url = "/app/topics/search/1/topic_name/"+topic_name+"/c/"+category_id+"/kw/"+keyword+"/";
			}	
			else {
				redirection_url = "/app/topics/search/1/topic_name/"+topic_name+"/c/"+category_id+"/";
			}
			window.location.href = redirection_url;
			return false;
  	});
  	

   $('#footerCrumbs a.breadcrumb-topic').click(function() {
	   	var topic_name = $(this).attr('title');
	   	var category_id = $(this).attr('id');
			var keyword = $('.KeywordField').val();	
			var redirection_url = "";
		
			if(keyword) {
				redirection_url = "/app/topics/search/1/topic_name/"+topic_name+"/c/"+category_id+"/kw/"+keyword+"/";
			}	
			else {
				redirection_url = "/app/topics/search/1/topic_name/"+topic_name+"/c/"+category_id+"/";
			}
			window.location.href = redirection_url;
			return false;
  	});

});