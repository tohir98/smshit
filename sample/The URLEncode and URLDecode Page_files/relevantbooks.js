var w = window;
w.arl_ad_align = '';

if (w.arl_ad_format == '120x600') {
	w.arl_ad_width = 120;
	w.arl_ad_height = 600;
} else if (w.arl_ad_format == '120x600R') {
	w.arl_ad_width = 120;
	w.arl_ad_height = 600;
    w.arl_ad_align = ' ALIGN="RIGHT"';
} else if (w.arl_ad_format == '120x650') {
	w.arl_ad_width = 120;
	w.arl_ad_height = 650;
} else if (w.arl_ad_format == '120x650R') {
	w.arl_ad_width = 120;
	w.arl_ad_height = 650;
    w.arl_ad_align = ' ALIGN="RIGHT"';
} else if (w.arl_ad_format == 'text') {
    if (w.arl_ad_width == null) {
	    w.arl_ad_width = 500;
    }
    if (w.arl_ad_height == null) {
	    w.arl_ad_height = 600;
    }
    w.arl_ad_align = ' ALIGN="CENTER"';
} else {
	w.arl_ad_format = '120x650';
	w.arl_ad_width = 120;
	w.arl_ad_height = 650;
}

if (w.arl_ad_bgcolor == null) {
	w.arl_ad_bgcolor = '#FFFFFF';
}

if (w.arl_ad_like == null) {
	w.arl_ad_like = '0596005423';
}

w.arl_ad_url = 'http://www.albionresearch.com/cgi-bin/relevantbooks.php?fmt=' + escape(w.arl_ad_format);

if (w.arl_ad_bgcolor != null) {
	w.arl_ad_url += '&bgcolor=' + escape(w.arl_ad_bgcolor);
}
if (w.arl_ad_like != null) {
	w.arl_ad_url += '&like=' + escape(w.arl_ad_like);
}

if (w.arl_ad_com != null) {
	w.arl_ad_url += '&com=' + escape(w.arl_ad_com);
}
if (w.arl_ad_ca != null) {
	w.arl_ad_url += '&ca=' + escape(w.arl_ad_ca);
}
if (w.arl_ad_uk != null) {
	w.arl_ad_url += '&uk=' + escape(w.arl_ad_uk);
}
if (w.arl_ad_fr != null) {
	w.arl_ad_url += '&fr=' + escape(w.arl_ad_fr);
}
if (w.arl_ad_de != null) {
	w.arl_ad_url += '&de=' + escape(w.arl_ad_de);
}
if (w.arl_ad_jp != null) {
	w.arl_ad_url += '&jp=' + escape(w.arl_ad_jp);
}

document.write( '<IFR' +'AME BORDER="0"' 
                + w.arl_ad_align
				+ ' WIDTH="' + w.arl_ad_width + '"'
				+ ' HEIGHT="' + w.arl_ad_height + '"'
				+ ' FRAMEBORDER="0"' 
                + ' MARGINHEIGHT="0" MARGINWIDTH="0" VSPACE="0" HSPACE="0"'
                + ' ALLOWTRANSPARENCY="TRUE" + SCROLLING="NO"'
				+ ' SRC="' + w.arl_ad_url + '"> </IFR' + 'AME>' );

