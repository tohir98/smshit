<?php
session_start();
require("pubs/virtualphp.php");

if (isset($_SESSION['SMShitgenid'])) {
    $user = $_SESSION['SMShitgenid'];
    $userSe = $user;
} else {
    header("Location: " . "../index.php?mess=Your session is over");
    exit();
}

$sqlq = "select * from users where id='$user'";
$viewq = ExecuteSQLQuery($sqlq);
if ($viewq) {
    $row = mysqli_fetch_assoc($viewq);
    $credit = $row['credit'];
}

function do_response($url) {
    $headers = array("User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.0.8) Gecko/20061025 Firefox/1.5.0.8");
    try {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_HTTPGET, 1); // Make sure GET method it used
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); // Return the result
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_COOKIEJAR, '/tmp/cookies.txt');
        curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/cookies.txt');
        $res = curl_exec($ch); // Run the request
    } catch (Exception $ex) {

        $res = 'NOK';
    }
    return $res;
}

function filter($data) {
    $db = opendatabase();
    $data = trim(htmlentities(strip_tags($data)));

    if (get_magic_quotes_gpc())
        $data = stripslashes($data);

    $data = mysqli_real_escape_string($db, $data);

    return $data;
    mysqli_close($db);
}

function htmlspecialcharss_decode($string, $style = ENT_COMPAT) {
    $translation = array_flip(get_html_translation_table(HTML_SPECIALCHARS, $style));
    if ($style === ENT_QUOTES) {
        $translation['&#039;'] = '\'';
    }
    return strtr($string, $translation);
}

function filter2($data) {
    $db = opendatabase();
    $data = trim(htmlentities(strip_tags($data)));
    $data = mysqli_real_escape_string($db, $data);
    return $data;
    mysqli_close($db);
}

function splitfon($number) {
    $order = array("\r\n", "\n", "\r", ".", "-", "/", "'");
    $pnumber = trim($number);
    $pnumber = str_replace($order, ",", $pnumber);
    $cose = explode(",", $pnumber);

    return $cose;
}

$opCode = $_POST['opCode'];
if ($opCode == "SENDNOW") {
    $credit = $_POST['credit'];
    $pnumber = $_POST['pnumber'];
    $pnumba = $_POST['pnumber'];
    $sname = $_POST['sname'];
    $message = str_replace(array("&quot;", "'"), "\'", $_POST['message']);
    $sendmess = trim($message);
    $sendpage = $_POST['state_count'];
    $country = $_POST['country'];
    $fil = $_FILES["fileuplEx"]["name"];
    $_SESSION['pnbar'] = $pnumber;
    $_SESSION['psn'] = $sname;
    $_SESSION['pmsg'] = $message;
    $send_date = $_POST['dob'];
    $hr = $_POST['hr'];
    $mi = $_POST['mi'];
    $send_time = $hr . ":" . $mi . ":" . '00';
    $schedule = $_POST['schedule'];
    $date = date('Y-m-d'); //date("dS F Y");
//    $dat = date('H') + 1;
//    $mint = date('i');
//    $sect = date('s');
//    $time = $dat . ':' . $mint . ':' . $sect;
    $time = date('H:i:s');
    $ampm = date("A");
    $mid = substr(md5(microtime()), 0, 5);  // for scheduled messages

    $order = array("\r\n", "\n", "\r");
    $pnumber = trim($pnumber);
    $lpnumber = strlen($pnumber);
    $pnumber = str_replace($order, ",", $pnumber);
    $pnumber4 = substr_replace($order, ",", $pnumber, $lpnumber);
    $pnumber = str_replace($order, ",", $pnumber);
    $nummo = $pnumber;

    $meslen = strlen($sendmess);
    $ids = $_SESSION['SMShitgenid'];
    $mtn_nig = 0;
    $cdma = 0;
    $subcdma = 0;
    $a = strlen($pnumber);
    if (substr($pnumber, $a - 1, 1) == ',') {
        $pnumber = substr($pnumber, 0, $a - 1);
    } else if (substr($pnumber, $a - 2, 1) == ',') {
        $pnumber = substr($pnumber, 0, $a - 2);
    }

    // SMS units
    $j = 1;
    if ($meslen > 160 && $meslen < 306) {
        $j = 2;
    } else if ($meslen > 305 && $meslen < 458) {
        $j = 3;
    } elseif ($meslen > 457 && $meslen < 611) {
        $j = 4;
    } elseif ($meslen > 610 && $meslen < 751) {
        $j = 5;
    } elseif ($meslen > 750 && $meslen < 891) {
        $j = 6;
    }

    $nig = 0;
    $gsmarray = array('234905', '234811', '234805', '234807', '234705', '234815', '234903', '234810', '234814', '234803', '234806', '234703', '234706', '234813', '234816', '234802', '234808', '234809', '234818', '234909', '234817', '234809', '2347025', '2347027', '234708', '234812', '234902');

    // If file is uploaded
    if ($fil != '' && $_POST['pnumber'] == '') {
        $tpm = $_FILES["fileuplEx"]["tmp_name"];
        $file2 = fopen($tpm, "r");
        $nummo = "";
        while (($emapData = fgetcsv($file2, 10000, ",")) !== FALSE) {
            if ($nummo == "") {
                $nummo = $emapData[0];
            } else {
                $nummo = $nummo . "," . $emapData[0];
            }
        }
    }

    // Computes total SMS Units
    $pnumber = explode(",", $nummo);
    if (trim($country) == "NG") {
        $iii = count($pnumber) * $j;
    } else {
        $iii = count($pnumber) * 5 * $j;
    }


    if ($pb_mnumber == '' && $totalnum == '' && $_POST[pnumber] == '' && $fil == '') {
        echo "<div id=mes>You have not added a phone number or upload a file Kindly Check this </div>";
        exit();
    } elseif ($j > 6) {
        echo "Message too long";
    } elseif ($credit < $iii) {
        echo "<div id=mes>You have insufficient sms credit for the attempted transaction </div><br><br>";
        echo '<form name="relsm" action="http://smshit.net/home.php?pageload=profile&load=buysms" method="post"><input type="submit" name="relsms" id="relsms" class="pstbutton forpst" value="Buy sms" /></form>';
        exit();
    } elseif ($country == "select country") {
        echo '<div id=mes>You have not selected a destination </div><form name="relbut" action="http://smshit.net/home.php" method="post"><input type="submit" name="relbut" id="relbut" class="pstbutton forpst" value="Go back" /></form>';
    } elseif ($sname == '') {
        echo "<div id=mes>You have not added a Sender's name, Kindly Check this </div>";
    } elseif ($message == '') {
        echo "<div id=mes>You have not added a message, Kindly Check this </div>";
    } elseif ($schedule == 'sendlater' && $send_date == '') {
        echo '<div id=mes>You have not added a date, Kindly Check this </div><form name="relbut" action="http://smshit.net/home.php" method="post"><input type="submit" name="relbut" id="relbut" class="pstbutton forpst" value="Go back" /></form>';
    } elseif ($schedule == 'sendlater' && $send_time == '') {
        echo '<div id=mes>You have not added a time, Kindly Check this </div><form name="relbut" action="http://smshit.net/home.php" method="post"><input type="submit" name="relbut" id="relbut" class="pstbutton forpst" value="Go back" /></form>';
    } else {

        $pnumber1 = explode(",", $nummo);
        for ($ii = 0; $ii < count($pnumber1); $ii++) {

            $pn = $pnumber1[$ii];
            if (substr($pn, 0, 3) != '234' && (substr($pn, 0, 2) == trim('08') || substr($pn, 0, 2) == trim('07') || substr($pn, 0, 2) == trim('09'))) {
                if ($ii == 0) {
                    if (substr($pn, 1, 11) != "") {
                        $pnum = '234' . substr($pn, 1, 11);
                    }
                } else {
                    if (substr($pn, 1, 11) != "") {
                        $pnum = $pnum . ';' . '234' . substr($pn, 1, 11);
                    }
                }
            } elseif (substr($pn, 0, 3) != '234' && (substr($pn, 0, 2) == trim('80') || substr($pn, 0, 2) == trim('81') || substr($pn, 0, 2) == trim('70') || substr($pn, 0, 2) == trim('90'))) {
                if ($ii == 0) {
                    if (substr($pn, 0, 11) != "" && strlen(substr($pn, 0, 10)) == strlen($pn)) {
                        $pnum = '234' . substr($pn, 0, 10);
                    }
                } else {
                    if (substr($pn, 0, 11) != "" && strlen(substr($pn, 0, 10)) == strlen($pn)) {
                        $pnum = $pnum . ';' . '234' . substr($pn, 0, 10);
                    }
                }
            } elseif (substr($pn, 0, 3) == '234' && (substr($pn, 3, 1) == trim('8') || substr($pn, 3, 1) == trim('7') || substr($pn, 3, 1) == trim('9'))) {
                if ($ii == 0) {
                    $pnum = $pn;
                } else {
                    $pnum = $pnum . ';' . $pn;
                }
            } else {
                if ($pn == "") {
                    if ($ii == 0) {
                        $pnum1 = "There is an empty column space in between your numbers. Please check";
                    } else {
                        $pnum1 = "There is an empty column space in between your numbers. Please check";
                    }
                } else {
                    if ($ii == 0) {
                        $pnum1 = $pn;
                    } else {
                        $pnum1 = $pnum1 . ',' . $pn;
                    }
                }
            }
        }



        $cpnu = explode(";", $pnum);
        if (trim($country) == "NG") {
            $iii2 = count($cpnu) * $j;
        } else {
            $iii2 = count($cpnu) * 5 * $j;
        }
        $ncredit = $credit - $iii2;

        if ($schedule == 'sendnow') {


            if (trim($country) == "NG") {
                $npnum1 = explode(",", $pnum1);
                $npnum2 = count($npnum1);
                $npnum2 = $npnum2 - 1;
                if ($npnum2 > 0) {
                    $ccc2 = count($npnum1);
                    $ccc2 = $ccc2 - 1;
                    echo '<span style="font-size:14pt; color:#999">Total number of bad phone numbers: ' . $ccc2 . '</span><br>';
                    $pnum1 = trim($pnum1);
                    $cuo = 1;
                    $cuo2 = 1;
                    $cpnum = explode(",", trim($pnum1));
                    $inv = "Invalid number";
                    for ($ii2 = 1; $ii2 < count($cpnum); $ii2++) {
                        $pn = trim($cpnum[$ii2]);
                        if ($pn == 0) {
                            if ($cuo == 1) {
                                $colnum1 = $pn;
                                $sql = "INSERT INTO smsoutbox(pnumber,sname,date,message,status,sid,smscount,country,time,ampm) VALUES('0','$sname','$date','$message','$inv','$ids','$j','$country','$time','$ampm')";
                                $resultc = ExecuteSQLQuery($sql);
                            } else {
                                $colnum1 = $colnum1 . ',' . $pn;
                                $sql = "INSERT INTO smsoutbox(pnumber,sname,date,message,status,sid,smscount,country,time,ampm) VALUES('0','$sname','$date','$message','$inv','$ids','$j','$country','$time','$ampm')";
                                $resultc = ExecuteSQLQuery($sql);
                            }
                            $cuo++;
                        } else {
                            if ($cuo2 == 1) {
                                $colnum2 = $pn;
                                $sql = "INSERT INTO smsoutbox(pnumber,sname,date,message,status,sid,smscount,country,time,ampm) VALUES('$pn','$sname','$date','$message','$inv','$ids','$j','$country','$time','$ampm')";
                                $resultc = ExecuteSQLQuery($sql);
                            } else {
                                $colnum2 = $colnum2 . ',' . $pn;

                                $sql = "INSERT INTO smsoutbox(pnumber,sname,date,message,status,sid,smscount,country,time,ampm,mid) VALUES('$pn','$sname','$date','$message','$inv','$ids','$j','$country','$time','$ampm','$mid')";
                                $resultc = ExecuteSQLQuery($sql);
                            }
                            $cuo2++;
                        }
                    }
                    echo '<form name="relbut" action="http://smshit.net/home.php" method="post"><input type="submit" name="relbut" id="relbut" class="pstbutton forpst" value="Go back" /></form>';
                }

                // Valid phone numbers section
                $pnum2 = explode(";", $pnum);
                echo '<span style="font-size:14pt; color:#999">Total number of good phone numbers: ' . $ccc = count($pnum2) . '</span><br>';
                if (count($pnum2) > 0) {

                    $_SESSION['SMShitcredit'] = $ncredit;
                    $res77 = ExecuteSQLQuery("UPDATE users SET credit='$ncredit' WHERE id='$ids'");
                    if (!$res77) {
                        echo '<div>Error updating credit</div>';
                        exit();
                    }

                    try {
                        $cr = explode(";", $pnum);
                        $cou = 0;
                        while ($cou < count($cr)) {
                            $numr = $cr[$cou];

                            $sql = "INSERT INTO smsoutbox(pnumber,sname,date,message,status,sid,smscount,country,time,ampm) VALUES('$numr','$sname','$date','$message','pending','$ids','$j','$country','$time','$ampm')";
                            $resultc = ExecuteSQLQuery($sql);
                            if (!$result) {
                                echo mysql_error();
                            }

                            $cou++;
                        }




                        echo '<div class="alert alert-success">
                <strong style="font-size:14pt; color:#999">Well done!</strong><span style="font-size:14pt; color:#999"> Your Message has been successfully sent.</span></div>
				<span style="font-size:14pt; color:#999">You have used </span><strong style="color:#F00; font-size:12pt">' . $iii2 . ' sms credits </strong> <br>


		<span  id="small2" style="font-size:14pt;">You have <strong style="color:#F00">' . $ncredit . '</strong> sms credits left  </span>';
                    } catch (Exception $ex) {
                        echo $res = 'NOK';
                    }
                }
            } elseif ($country != "NG") {
                $user = "tunde11";
                $password = "OLA^27*11";



                $username = $user;     //your username
                $password = $password;     //your password
                $sender = $sname;
                $isflash = $_POST['isflash'];       //Is flash message (1 or 0)
                $type = "longSMS";
                $bookmark = $_POST['bookmark'];  //wap url (example: www.google.com)
                $message = $sendmess;


                $sender = str_replace("+", "%2b", $sender);
                $message = str_replace("+", "%2b", $message);

                $url = "http://api.infobip.com/api/v3/sendsms/plain?"
                        . "user=" . UrlEncode($user)
                        . "&password=" . UrlEncode($password)
                        . "&sender=" . UrlEncode($sname)
                        . "&SMSText=" . UrlEncode($message)
                        . "&GSM=" . UrlEncode($nummo);
                $_SESSION['SMShitcredit'] = $ncredit;
                $sql77 = "UPDATE users SET credit='$ncredit' WHERE id='$ids'";
                $res77 = ExecuteSQLQuery($sql77);
                if (!$res77) {
                    echo '<div>Error updating credit</div>';
                    exit();
                }
                try {
                    $xml = simplexml_load_file($url);
                    for ($i = 0; $i < sizeof($xml); $i++) {
                        $sta = $xml->result[$i]->status;

                        $msgid = $xml->result[$i]->messageid;

                        $des = $xml->result[$i]->destination;


                        if ($sta == 0) {
                            $starep = "Delivered";
                        } elseif ($sta == -1) {
                            $starep = "Error in processing the request";
                        } elseif ($sta == -2) {
                            $starep = "Internal error";
                        } elseif ($sta == -3) {
                            $starep = "Targeted network is not covered on specific account";
                        } elseif ($sta == -5) {
                            $starep = "Username or password is invalid";
                        } elseif ($sta == -6) {
                            $starep = "Destination address is missing in the request";
                        } elseif ($sta == -10) {
                            $starep = "Username is missing in the request";
                        } elseif ($sta == -11) {
                            $starep = "Password is missing in the request";
                        } elseif ($sta == -13) {
                            $starep = "Number is not recognized by smshit platform";
                        } elseif ($sta == -22) {
                            $starep = "Incorrect XML format, caused by syntax error";
                        } elseif ($sta == -23) {
                            $starep = "General error, reasons may vary";
                        } elseif ($sta == -26) {
                            $starep = "General API error, reasons may vary";
                        } elseif ($sta == -27) {
                            $starep = "Invalid scheduling parametar";
                        } elseif ($sta == -28) {
                            $starep = "Invalid PushURL in the request";
                        } elseif ($sta == -30) {
                            $starep = "Invalid APPID in the request";
                        } elseif ($sta == -33) {
                            $starep = "Duplicated MessageID in the request";
                        } elseif ($sta == -34) {
                            $starep = "Sender name is not allowed";
                        } elseif ($sta == -99) {
                            $starep = "Error in processing request, reasons may vary";
                        }

                        $sql = "insert into smsoutbox(pnumber,sname,date,message,status,sid,smscount,country,time,ampm) VALUES('$des','$sname','$date','$message','$starep','$ids','$j','$country','$time','$ampm')";
                        $resultc = ExecuteSQLQuery($sql);
                    }



                    echo '<div class="alert alert-success">
                <strong style="font-size:14pt; color:#999">Well done!</strong><span style="font-size:14pt; color:#999"> Your Message has been successfully sent.</span></div>
				You have used <strong style="color:#F00">' . $iii . ' sms credits </strong> <br>


		<span  id="small2">You have <strong style="color:#F00">' . $ncredit . '</strong> sms credits left  </span>';
                } catch (Exception $ex) {

                    echo $res = 'NOK';
                }
            }
        }//end of schedule now



        if ($schedule == 'sendlater') {
            $_SESSION['SMShitcredit'] = $ncredit;
            $sql77 = "UPDATE users SET credit='$ncredit' WHERE id='$ids'";
            $res77 = ExecuteSQLQuery($sql77);
            if (!$res77) {
                echo '<div>Error updating credit</div>';
                exit();
            }
            if ($country == "NG") {

                $cr = explode(";", $pnum);
                $cou = 0;
                while ($cou < count($cr)) {
                    $numr = $cr[$cou];

                    $sql = "INSERT INTO smsoutbox(pnumber,sname,date,message,status,sid,smscount,country,time,ampm,mid) VALUES('$numr','$sname','$date','$message','pending','$ids','$j','$country','$time','$ampm','$mid')";
                    $resultc = ExecuteSQLQuery($sql);
                    if (!$result) {
                        echo mysql_error();
                    }

                    $cou++;
                }

            } else {
                // If country is not nigeria
                $cr2 = explode(",", $nummo);
                for ($cou2 = 0; $cou2 < count($cr2); $cou2++) {
                    $numr2 = $cr2[$cou2];
                    $status = 'pending';
                    $sqlsm2 = "insert into smsoutbox(pnumber,sname,date,message,status,sid,smscount,country,time,ampm,mid) VALUES('$numr2','$sname','$date','$message','$status','$ids','$j','$country','$time','$ampm','$mid')";
                    $resultcsm2 = ExecuteSQLQuery($sqlsm2);
                }
            }
            if ($fil != '' && $_POST[pnumber] == '') {
                if (($_FILES["fileuplEx"]["size"] < 300000)) {
                    if ($_FILES["fileuplEx"]["error"] > 0) {
                        echo $rep = "Tmess=Error in uploading file";
                        exit();
                    } else {

                        if (file_exists("tmp/" . $_FILES["fileuplEx"]["name"])) {
                            echo $rep = "This file exist in our database! try again.";
                            exit();
                        } else {
                            $url = "tmp/" . rand(11111111, 99999999) . ".csv";
                        }//end of file exist
                    }
                }//end of file size
            }
            if ($url != "") {
                $nom = "";
                $sqly = "insert into schedul(sid,send_date,send_time,status,mid,country,message,number,filename,sender,datecreated) VALUES('$ids','$send_date','$send_time','$status','$mid','$country'
					,'$message','$nom','$url','$sname',NOW())";
                copy($_FILES['fileuplEx']['tmp_name'], $url);
                $resulty = ExecuteSQLQuery($sqly) or die("Error in scheduling! Process terminated");
            } else {
                $urt = "";
                $sqly = "insert into schedul(sid,send_date,send_time,status,mid,country,message,number,filename,sender,datecreated) VALUES('$ids','$send_date','$send_time','$status','$mid','$country','$message','$nummo','$urt','$sname',NOW())";

                $resulty = ExecuteSQLQuery($sqly) or die("Error in scheduling! Process terminated");
            }

            echo '<div class="alert alert-success">
                <strong style="font-size:14pt; color:#999">Well done!</strong> <span style="font-size:14pt; color:#999">Message successfully scheduled.</span></div>';
        }
    }//end of else








    if ($resultc) {
        if ($nig != 0) {
            echo "<div id=mes>  You have been charged $intbill sms credits for $intpage page(s) sent to $nig international number </div>";
        }

        if ($intbill != 0) {
            //echo"
            //Number of international sms(s) sent: <strong style='color:#F00'>$nig</strong> <br /></div>
            //Number of sms credit(s) charged for international sms: <strong style='color:#F00'>$intbill sms credits</strong> <br /></div>";
        }
        if ($localpage != 0) {
            //echo"
            //Number of local sms(s) sent: <strong style='color:#F00'>$local</strong> <br /></div>
            //Number of sms credit(s) charged for local sms: <strong style='color:#F00'>$localpage sms credit(s) to other networks + $localmtn sms credit(s) to MTN
            //Nigeria </strong> <br /></div>";
        }
        if ($cdmabill != 0) {
            //echo"
            //Number of CDMA sms(s) sent: <strong style='color:#F00'>$cdma</strong> <br /></div>
            //Number of sms credit(s) charged for CDMA sms: <strong style='color:#F00'>$cdmabill sms credits</strong> <br /></div>";
        }
    } else {



        if (isset($_POST['sender'])) {

            $rid = $_POST['rid'];
        }


        if (isset($_GET['send'])) {
            echo "<b>Sending Successful </b><br><br>";
        }
    }
    exit();
}
?>


<script type="text/javascript" src="script/scripts/ui/jquery.ui.widget.js"></script>
<script type="text/javascript" src="script/scripts/ui/jquery.ui.position.js"></script>
<script type="text/javascript" src="script/scripts/jquery.ui.dialog.js"></script>
<script type="text/javascript" src="script/scripts/ui/jquery.ui.datepicker.js"></script>
<script type="text/javascript" src="script/scripts/ui/jquery.ui.core.js"></script>
<script type="text/javascript" src="script/scripts/datefun.js"></script>
<script type="text/javascript" language="javascript" src="js/jquery.form.js"></script>

<script type="text/javascript">
//Edit the counter/limiter value as your wish
    var count = "150";   //Example: var count = "175";
    function limiter() {
        var tex = document.myform.message.value;
        var len = tex.length;
        if (len > count) {
            tex = tex.substring(0, count);
            document.myform.message.value = tex;
            return false;
        }
        document.myform.limit.value = count - len;
    }

</script>
<?php
if (isset($_GET['mess']) && isset($_GET['unit']) && isset($_GET['left'])) {
    $mes = $_GET['mess'];
    $uni = $_GET['unit'];
    $left = $_GET['left'];
}
?>
<div style="font-size:14pt; padding-right:10pt; color:#999" id="errorMsg"></div>
<div id="wilhid">
    <form action="home.php" method="post" name="myform" id="myform" enctype="multipart/form-data">

        <ul>
            <li>&nbsp;</li>
            <li>
                <span style="color:#999;font-size:16px; font-weight:bold">Destination</span> <div class="small"> (This is optional.)</div></li>
            <li><select name="country" onChange=" " class="inputsearch">
                    <option selected="selected" value="select country" > select country </option>
                    <option value="COBN">Combined-numbers</option>
                    <option value="AF">Afghanistan</option>
                    <option value="AL">Albania</option><option value="DZ">Algeria</option><option value="AS">American Samoa</option><option value="AD">Andorra</option><option value="AO">Angola</option><option value="AI">Anguilla</option><option value="AQ">Antarctica</option><option value="AG">Antigua and Barbuda</option><option value="AR">Argentina</option><option value="AM">Armenia</option><option value="AW">Aruba</option><option value="AU">Australia</option><option value="AT">Austria</option><option value="AZ">Azerbaijan</option><option value="BS">Bahamas</option><option value="BH">Bahrain</option><option value="BD">Bangladesh</option><option value="BB">Barbados</option><option value="BY">Belarus</option><option value="BE">Belgium</option><option value="BZ">Belize</option><option value="BJ">Benin</option><option value="BM">Bermuda</option><option value="BT">Bhutan</option><option value="BO">Bolivia</option><option value="BA">Bosnia and Herzegovina</option><option value="BW">Botswana</option><option value="BV">Bouvet Island</option><option value="BR">Brazil</option><option value="IO">British Indian Ocean Territory</option><option value="BN">Brunei Darussalam</option><option value="BG">Bulgaria</option><option value="BF">Burkina Faso</option><option value="BI">Burundi</option><option value="KH">Cambodia</option><option value="CM">Cameroon</option><option value="CA">Canada</option><option value="CV">Cape Verde</option><option value="KY">Cayman Islands</option><option value="CF">Central African Republic</option><option value="TD">Chad</option><option value="CL">Chile</option><option value="CN">China</option><option value="CX">Christmas Island</option><option value="CC">Cocos (Keeling) Islands</option><option value="CO">Colombia</option><option value="KM">Comoros</option><option value="CG">Congo</option><option value="CD">Congo, the Democratic Republic of the</option><option value="CK">Cook Islands</option><option value="CR">Costa Rica</option><option value="CI">Cote D'Ivoire</option><option value="HR">Croatia</option><option value="CU">Cuba</option><option value="CY">Cyprus</option><option value="CZ">Czech Republic</option><option value="DK">Denmark</option><option value="DJ">Djibouti</option><option value="DM">Dominica</option><option value="DO">Dominican Republic</option><option value="EC">Ecuador</option><option value="EG">Egypt</option><option value="SV">El Salvador</option><option value="GQ">Equatorial Guinea</option><option value="ER">Eritrea</option><option value="EE">Estonia</option><option value="ET">Ethiopia</option><option value="FK">Falkland Islands (Malvinas)</option><option value="FO">Faroe Islands</option><option value="FJ">Fiji</option><option value="FI">Finland</option><option value="FR">France</option><option value="GF">French Guiana</option><option value="PF">French Polynesia</option><option value="TF">French Southern Territories</option><option value="GA">Gabon</option><option value="GM">Gambia</option><option value="GE">Georgia</option><option value="DE">Germany</option><option value="GH">Ghana</option><option value="GI">Gibraltar</option><option value="GR">Greece</option><option value="GL">Greenland</option><option value="GD">Grenada</option><option value="GP">Guadeloupe</option><option value="GU">Guam</option><option value="GT">Guatemala</option><option value="GN">Guinea</option><option value="GW">Guinea-Bissau</option><option value="GY">Guyana</option><option value="HT">Haiti</option><option value="HM">Heard Island and Mcdonald Islands</option><option value="VA">Holy See (Vatican City State)</option><option value="HN">Honduras</option><option value="HK">Hong Kong</option><option value="HU">Hungary</option><option value="IS">Iceland</option><option value="IN">India</option><option value="ID">Indonesia</option><option value="IR">Iran, Islamic Republic of</option><option value="IQ">Iraq</option><option value="IE">Ireland</option><option value="IL">Israel</option><option value="IT">Italy</option><option value="JM">Jamaica</option><option value="JP">Japan</option><option value="JO">Jordan</option><option value="KZ">Kazakhstan</option><option value="KE">Kenya</option><option value="KI">Kiribati</option><option value="KP">Korea, Democratic People's Republic of</option><option value="KR">Korea, Republic of</option><option value="KW">Kuwait</option><option value="KG">Kyrgyzstan</option><option value="LA">Lao People's Democratic Republic</option><option value="LV">Latvia</option><option value="LB">Lebanon</option><option value="LS">Lesotho</option><option value="LR">Liberia</option><option value="LY">Libyan Arab Jamahiriya</option><option value="LI">Liechtenstein</option><option value="LT">Lithuania</option><option value="LU">Luxembourg</option><option value="MO">Macao</option><option value="MK">Macedonia, the Former Yugoslav Republic of</option><option value="MG">Madagascar</option><option value="MW">Malawi</option><option value="MY">Malaysia</option><option value="MV">Maldives</option><option value="ML">Mali</option><option value="MT">Malta</option><option value="MH">Marshall Islands</option><option value="MQ">Martinique</option><option value="MR">Mauritania</option><option value="MU">Mauritius</option><option value="YT">Mayotte</option><option value="MX">Mexico</option><option value="FM">Micronesia, Federated States of</option><option value="MD">Moldova, Republic of</option><option value="MC">Monaco</option><option value="ME">Montenegro</option><option value="MN">Mongolia</option><option value="MS">Montserrat</option><option value="MA">Morocco</option><option value="MZ">Mozambique</option><option value="MM">Myanmar</option><option value="NA">Namibia</option><option value="NR">Nauru</option><option value="NP">Nepal</option><option value="NL">Netherlands</option><option value="AN">Netherlands Antilles</option><option value="NC">New Caledonia</option><option value="NZ">New Zealand</option><option value="NI">Nicaragua</option><option value="NE">Niger</option><option value="NG">Nigeria</option><option value="NU">Niue</option><option value="NF">Norfolk Island</option><option value="MP">Northern Mariana Islands</option><option value="NO">Norway</option><option value="OM">Oman</option><option value="PK">Pakistan</option><option value="PW">Palau</option><option value="PS">Palestinian Territory, Occupied</option><option value="PA">Panama</option><option value="PG">Papua New Guinea</option><option value="PY">Paraguay</option><option value="PE">Peru</option><option value="PH">Philippines</option><option value="PN">Pitcairn</option><option value="PL">Poland</option><option value="PT">Portugal</option><option value="PR">Puerto Rico</option><option value="QA">Qatar</option><option value="RE">Reunion</option><option value="RO">Romania</option><option value="RU">Russian Federation</option><option value="RW">Rwanda</option><option value="SH">Saint Helena</option><option value="KN">Saint Kitts and Nevis</option><option value="LC">Saint Lucia</option><option value="PM">Saint Pierre and Miquelon</option><option value="VC">Saint Vincent and the Grenadines</option><option value="WS">Samoa</option><option value="SM">San Marino</option><option value="ST">Sao Tome and Principe</option><option value="SA">Saudi Arabia</option><option value="SN">Senegal</option><option value="RS">Serbia</option><option value="SC">Seychelles</option><option value="SL">Sierra Leone</option><option value="SG">Singapore</option><option value="SK">Slovakia</option><option value="SI">Slovenia</option><option value="SB">Solomon Islands</option><option value="SO">Somalia</option><option value="ZA">South Africa</option><option value="GS">South Georgia and the South Sandwich Islands</option><option value="ES">Spain</option><option value="LK">Sri Lanka</option><option value="SD">Sudan</option><option value="SR">Suriname</option><option value="SJ">Svalbard and Jan Mayen</option><option value="SZ">Swaziland</option><option value="SE">Sweden</option><option value="CH">Switzerland</option><option value="SY">Syrian Arab Republic</option><option value="TW">Taiwan, Province of China</option><option value="TJ">Tajikistan</option><option value="TZ">Tanzania, United Republic of</option><option value="TH">Thailand</option><option value="TL">Timor-Leste</option><option value="TG">Togo</option><option value="TK">Tokelau</option><option value="TO">Tonga</option><option value="TT">Trinidad and Tobago</option><option value="TN">Tunisia</option><option value="TR">Turkey</option><option value="TM">Turkmenistan</option><option value="TC">Turks and Caicos Islands</option><option value="TV">Tuvalu</option><option value="UG">Uganda</option><option value="UA">Ukraine</option><option value="AE">United Arab Emirates</option><option value="GB">United Kingdom</option><option value="US">United States</option><option value="UM">United States Minor Outlying Islands</option><option value="UY">Uruguay</option><option value="UZ">Uzbekistan</option><option value="VU">Vanuatu</option><option value="VE">Venezuela</option><option value="VN">Viet Nam</option><option value="VG">Virgin Islands, British</option><option value="VI">Virgin Islands, U.s.</option><option value="WF">Wallis and Futuna</option><option value="EH">Western Sahara</option><option value="YE">Yemen</option><option value="ZM">Zambia</option><option value="ZW">Zimbabwe</option>

<?php $cum = $_POST[cum]; ?>
                </select></li>
            <li>&nbsp;</li>
            <li>
                <div id='small2'>
                    <span style="color:#999;font-size:16px; font-weight:bold">Phone Number</span> <br />
                    <div class="small">  (Separate numbers with line breaks and write them in international number format if no country is selected for all) e.g<br />
                        234805XXXXXXX <br />
                        447123XXXXXX <br />
                        17123XXXXXXX</div>
                </div></li>
            <li>
                <input type="hidden" name="credit" value="<?php echo $credit; ?>" />

<?php
for ($i = 1; $i <= $cum; $i++) {
    $sindex = "sel" . $i;
    $addnumber = $_POST[$sindex];
    if ($addnumber != "") {
        $totalnum.="$addnumber\n";
    }
}

$pb_num = $_GET[n];

$sqld_pb = "select * from phonebook where id='$pb_num' order by name asc ";
$viewd_pb = ExecuteSQLQuery($sqld_pb);

while ($row = mysqli_fetch_assoc($viewd_pb)) {

    $pb_mnumber = stripslashes($row['mnumber']);
}

$use_num = $pb_mnumber . $totalnum . $_POST[pnumber];
?>



                <textarea cols="16" rows="15" name="pnumber" id="pnumber" /><?php
                if ($use_num != "") {
                    echo $use_num;
                }
?><?php
                if ($_POST['relbut']) {
                    if (isset($_SESSION['pnbar'])) {
                        echo htmlentities($_SESSION['pnbar']);
                    }
                }
                ?></textarea></li>
            <li>&nbsp;</li>
            <li>

                <div  class="pstbutton" style="width:18%; font-size:10pt; cursor:pointer" id="importfon">Import from phonebook</div></li>

            <li>&nbsp;</li>
            <li><input type="file" name="fileuplEx" class="inputsearch" id="fileuplEx" title=".txt, .csv"><span style="font-size:10pt">supported file(.txt, .csv)</span></li>

            <li>&nbsp;</li>
            <li>
                <span id='small2'> <span style="color:#999;font-size:16px; font-weight:bold">Sender's Name</span><span class="alerty"> (Maximum of 11 characters)</span></span></li>
            <li>&nbsp;</li>
            <li>
                <input type="text" name="sname" id="sname" maxlength="11"  value="<?php
                if ($_POST['relbut']) {
                    if (isset($_SESSION['psn'])) {
                        echo htmlentities($_SESSION['psn']);
                    }
                }
                ?>" class="inputsearch"/> <br /></li>
            <li>&nbsp;</li>
            <span id='small2'><span style="color:#999;font-size:16px; font-weight:bold">SMS MESSAGE</span> </span><br /></li>


<!--<textarea style="width:380px;height:100px;" name="message" ><?php echo $message; ?></textarea> -->
            <li>
                <textarea name="message" id="message" wrap=physical rows=5 cols=50 onkeyup="smsupdate()"><?php
                if ($_POST['relbut']) {
                    if (isset($_SESSION['pmsg'])) {
                        echo htmlentities($_SESSION['pmsg']);
                    }
                }
                ?></textarea><br></li>
            <li>
                <input name="state_count" disabled="disabled"  value="0" size="3">
                <input name="state_len" disabled="disabled"  value="160" size="3"></li>

<!--tr><td valign="top" align="left" >

<input type="hidden" name="show" value=""/>
<input readonly type="text" style="width:38px;background-color:#FFF;" class="counter" name="count" size="3" maxlength="3" value="160">

 </td></tr>-->
            <li>&nbsp;</li>
            <li>
                <label><input type="radio" name="schedule"  id="radio1" value="sendnow" checked="checked" /><span id='small2'>Send now</span></label>
                <label><input type="radio" name="schedule" id="radio1" value="sendlater" /><span id='small2'>Send later</span></label></li>
            <li>&nbsp;</li>
            <li>
                <input class="inputsearch" id="dob" name="dob" autocomplete="off" readonly="readonly" type="text" />
            </li>
            <li>&nbsp;</li>
            <li>HR:
                <select name="hr" id="hr" class="inputsearch" style="width:10%">
                    <option selected="selected" value="00">00</option>
                    <SCRIPT language="javascript">
                        for (var dy = 1; dy <= 23; dy++) {
                            if (dy < 10)
                            {
                                dy = "0" + dy;
                            }
                            document.writeln("<OPTION>" + dy + "</OPTION>");
                        }
                    </SCRIPT>
                </select>

                MIN:
                <select name="mi"  id="mi" class="inputsearch" style="width:10%">
                    <option selected="selected" value="00">00</option>
                    <SCRIPT language="javascript">
                        for (var dy = 1; dy <= 59; dy++) {
                            if (dy < 10)
                            {
                                dy = "0" + dy;
                            }
                            document.writeln("<OPTION>" + dy + "</OPTION>");
                        }
                    </SCRIPT>
                </select></li>

            <li>&nbsp;</li>

            <li><input type="button" class="pstbutton forpst" name="sendnow" id="sendnow" value="Send SMS" /><input type="hidden" name="opCode" id="opCode" value="SENDNOW" /><span id="Repotin"></span>
            </li>
    </form>
</div>
