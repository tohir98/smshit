<?php
require("pubs/virtualphp.php");
if (isset($_SESSION['SMShitgenid'])) {
    $user = $_SESSION['SMShitgenid'];
} else {
    header("Location: " . "../index.php?mess=Your session is over");
    exit();
}
$months = ['January', 'Febuary', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <style type="text/css">
            td{ padding:5px; font-size:10pt}
            input{ padding:10px 15px 10px 15px;}

            .pagination {
                font-family: 'Helvetica Neue',Helvetica,Arial,sans-serif;
                height: 30px;
                font-size: 12px;
                -webkit-font-smoothing: antialiased;
                float: left;

            }

            .pagination a {
                background-color: #1199BB;
                width: 27px;
                height: 27px;
                line-height: 27px;
                border : 1px solid #fff;
                text-decoration: none;
                display: inline-block;
                color: #717171;
                text-align: center;
                color: #717171;
                text-shadow: 1px 1px 0 #fff;
            }

            .pagination a:hover {
                background-color: green;
                color: #fff;
            }


            .pagination ul li {
                float: left;
                display: block;
                width: 30px;
            }
            .pagination ul li a {
                background-repeat: no-repeat;
                width: 27px;
                height: 27px;
                line-height: 27px;
                text-decoration: none;
                display: block;
                text-align: center;
                color: #717171;
                text-shadow: 1px 1px 0 #fff;
            }

        </style>
        <script type="text/javascript" language="javascript">
            $(document).ready(function () {

                $("#errorMsg").html("<blink><span style='color:green'><b>Verifying...</b></span></blink>");
                $.post("pubs/api.php", {opCode: "REPORT", page1: "<?= isset($_GET['page1']) ? $_GET['page1'] : '0' ?>"}, function (response) {
                    if (response != "") {

                        $("#content").html(response);

                    } else {
                        $("#errorMsg").html("<blink><span style='color:green'><b>Error in connection</b></span></blink>");
                    }
                });
                $("#errorMsg").html("");
            });
        </script>
    </head>
    <body>
        <form action="downloaddata.php" enctype="multipart/form-data" method="post" name="form1">
            <table width="90%" cellspacing="0" cellpadding="0">
                <tr>
                    <td colspan="2" align="center"><span style="font-size:14pt; color:#333">Export Report with date</span></td>
                </tr>
                <tr>
                    <td width="11%">From:</td>
                    <td width="89%"><input type="date" name="start_date" id="start_date" placeholder="dd/mm/yyyy" /></td>
                </tr>
                <tr>
                    <td>To:</td>
                    <td><input type="date" name="end_date" id="end_date"  placeholder="dd/mm/yyyy" /></td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                    <td><input type="submit" name="export" id="export" value="Export"></td>
                </tr>
            </table>
        </form>
        <div id="errorMsg"></div>
        <div id="content"></div>

        <div id="errorMsg"></div>
    </body>
</html>